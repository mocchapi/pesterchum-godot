extends _quirkreplacer
class_name QuirkReplacerYourname # set this to the name

const name:String = ""# Printable name here

export var some_parameter := # Add a user-facing parameter
const some_parameter_doc:String = "" # The function of the parameter in layman. also user-facing


func printable():
	var printable_representation:String =
	# A short string that describes the state of this replacer
	# I.E. "add prefix {prefix}".format({'prefix':prefix})
	return printable_representation


func replace(selected_text:String) -> String:
	var out:String = 
	# The actual functionality. selected_text comes straight from the selector
	return out

# Dont forget to add this to the Globals.replacers array!
