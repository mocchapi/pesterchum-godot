extends _IRCcmdParser

func get_format() -> Array:
	# Array of Args that form the parsestring
	return [ Origin.new(), ]

func trigger(irc, args:Array):
	# The action to undertake when this parser is matched
	pass
