extends Reference
class_name VERSIONS

# Not instantiated
# 
# This is here because putting these in any other global class like Globals or Config
# Sends you straight to cyclic dependency hell from which you will never return

const application:String = "alpha_10"
# Version name of pesterchum godot
const theme_format:int = 1
# Version of the theme format, should be increased if any UIComponent is changed/added/removed or the api changed significantly
const config_format:int = 8
# Version of the config format, should be increased if a new item is added or an old one removed
const updated:int = 1718376949
# Timestamp when the current update was roughly

const update_check_url :String = "https://gitlab.com/mocchapi/pesterchum-godot/-/raw/stable/singletons/Versions.gd"
# Update file on stable (major versions)
#const update_check_url :String = "https://gitlab.com/mocchapi/pesterchum-godot/-/raw/main/singletons/Versions.gd"
# Update file on main (will also set off if a release is in development, but not yet released)
