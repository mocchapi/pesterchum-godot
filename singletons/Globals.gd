extends Node
class_name _globals


var splash := """


	 ----- Welcome to pesterchum_godot! -----
		 You are running version {0}

 pesterchum_godot is open source & licensed under GPLv3.

 if you run into a problem, or want to view the code
 head to the gitlab at: https://gitlab.com/mocchapi/pesterchum-godot
	 ----------------------------------------
 Your config directory is:
	{2} 
	 ----------------------------------------
		 {1}


""".format([VERSIONS.application, 'This is a debug editor build' if is_editor_build() else '', OS.get_user_data_dir()])


var main: _pesterchum_main setget ,get_main
var irc: IRCClient setget ,get_irc
var pesterlayer: _PesterchumLayer setget ,get_pesterlayer
var parsetools: _ParseTools setget ,get_parsetools
var usercache: _UserCache setget ,get_usercache
var menus setget ,get_menus
var popups setget ,get_popups

var themes := {}

var icons := {
	friend = preload("res://assets/texture/icons/chum.png"),
	robot = preload("res://assets/texture/icons/robot_small.png"),
	unknown_mood = preload("res://assets/texture/icons/unknownmood.png"),
	troll = preload("res://assets/texture/icons/troll.png"),
	current_profile = preload("res://assets/texture/icons/greencircle.png"),
	canon = preload("res://assets/texture/icons/canon.png")
}

func get_main():
	if not is_instance_valid(main):
		main = $"/root/pesterchum_main"
	return main

func get_irc():
	if not is_instance_valid(irc):
		irc = $"/root/pesterchum_main/IRCClient"
	return irc 

func get_pesterlayer():
	if not is_instance_valid( pesterlayer ):
		pesterlayer = $"/root/pesterchum_main/PesterchumLayer"
	return pesterlayer

func get_usercache():
	if not is_instance_valid( usercache ):
		usercache = $"/root/pesterchum_main/Usercache"
	return usercache

func get_menus():
	if not is_instance_valid( menus ):
		menus = $"/root/pesterchum_main/approot/menus"
	return menus

func get_popups():
	if not is_instance_valid( popups ):
		popups = $"/root/pesterchum_main/approot/popups"
	return popups

func get_parsetools():
	if not is_instance_valid( parsetools ):
		parsetools = $"/root/pesterchum_main/Parsetools"
	return parsetools

func _enter_tree():
	_re_get_global_vars()
	reload_themes()

func _re_get_global_vars():
	main = get_main()
	irc = get_irc()
	pesterlayer = get_pesterlayer()
	usercache = get_usercache()

func reload_themes():
	themes = {}
	var items = EasyDirectory.walk_directory("user://custom/themes", true, false, true) + EasyDirectory.walk_directory("res://themes/resources/", true, false, true)
	print("(Globals) reload_themes: ","Loading themes...")
	for path in items:
		print('   Checking item: ',path)
		var res = load(path)
		if res.get("is_client_theme"):
			print("    Loaded as ",res.theme_name, " by ", res.theme_author)
			themes[res.theme_name] = res
	
	themes['Pesterchum'] = preload("res://themes/resources/pesterchum.tres")
	# Make absolutely sure Pesterchum is always the internal one
	# This because Pesterchum is the default & fallback theme, so letting it get overwritten would be. a bad idea
	# TODO?: maybe make user:// & internal themes seperate steps, so custom themes can never overwrite internal themes?



static func color_contrasts_dark(color)->bool:
	var cval = (float(color.r8)*0.299 + float(color.g8)*0.587 + float(color.b8)*0.114)
	return cval > 180

func _ready():
	print(splash)
	randomize()
	_enter_tree()

func start_connection():
	if irc.is_websocket_server != Config.settings.connection.use_websocket:
		irc.is_websocket_server = Config.settings.connection.use_websocket
	if irc != null:
		irc.nickname = Config.current_profile.chumhandle
		irc.realname = "godot;"+str(VERSIONS.application)
		irc.host = Config.settings.connection.hostname
		irc.port = Config.settings.connection.port
		irc.use_ssl = Config.settings.connection.use_tls
		irc.validate_certificate = Config.settings.connection.validate_certificate
		irc.start_connection()

func hard_reload():
	if irc != null:
		irc.quit()
	if get_tree().change_scene("res://scenes/main/pesterchum_main.tscn") == OK:
		yield(get_tree(), "idle_frame")
		call_deferred("_enter_tree")

func running_on_phone(ignore_force_desktop:=false)->bool:
	if ignore_force_desktop:
		if OS.get_name() == 'HTML5':
			return JavaScript.eval('(screen.orientation.type === "portrait-primary" || screen.orientation.type === "portrait-secondary" )')
		return OS.get_name() in ["Android", "iOS"]
	match Config.settings.flags.force_desktop:
		OVERRIDE.FORCE_TRUE:
			return false
		OVERRIDE.FORCE_FALSE:
			return true
		OVERRIDE.DONT_OVERRIDE, _:
			return OS.get_name() in ["Android", "iOS"]

func is_editor_build() -> bool:
	return OS.has_feature('editor')

func is_release_build() -> bool:
	return not is_editor_build()


func validate_channel(channel_name:String)->bool:
	return len(channel_name) > 0 and channel_name[0] in ['#','&'] and not ' ' in channel_name and not ',' in channel_name and not ':' in channel_name



var quirk_selectors = [QuirkSelectorAll, QuirkSelectorSimple, QuirkSelectorBetween, QuirkSelectorRegex]
var quirk_replacers = [QuirkReplacerSimple, QuirkReplacerPrefix, QuirkReplacerRepeat, QuirkReplacerColor, QuirkReplacerSuffix, QuirkReplacerCase, QuirkReplacerExpression]


var canon_handles = ["gardenGnostic", "gallowsCalibrator", "grimAuxiliatrix", "ghostyTrickster", "ectoBiologist", "gutsyGumshoe", "golgothasTerror", "carcinoGeneticist", "cuttlefishCuller", "caligulasAquarium", "centaursTesticle", "arachnidsGrip", "arsenicCatnip", "apocalypseArisen", "adiosToreador", "turntechGodhead", "terminallyCapricious", "twinArmageddons", "tentacleTherapist", "tipsyGnostalgic", "timaeusTestified"]


enum HINTS {
	ignore,
	override,
	enum_as_string,
	enum_as_int,
	codeblock,
}

enum OVERRIDE {
	FORCE_TRUE,
	FORCE_FALSE,
	DONT_OVERRIDE
}

enum DISPLAYMODES {
	MULTIWINDOW,
	SINGLEWINDOW,
}
enum VKMODE {
	PUSH_UP,
	RESIZE,
	NONE,
}

enum SERVERMODES {
	NORMAL,
	LEGACY,
	IRC
}
