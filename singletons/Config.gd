extends Node

signal loading_failed(message)

# The singleton that helps with saving and loading config resources, NOT the config resource itself
# The resource can be found at "res://singletons/config/config_res.gd"
# You can however access config attributes directly still
# for example:
# 	Config.settings would be the same as Config.c.settings

onready var c := pcConfig.new() 

signal config_loaded()
signal config_saved()

export var save_path := "user://config.tres"
export var load_on_ready := true

var save_timer:Timer

# Possible CLI args as of alpha 4:
# --reset-config: overwrites the config file to the default settings
# --ignore-config: Loads the default config, but doesnt overwrite the config file, best used in tandem with --readonly-config
# --readonly-config: Prevents saving the config file
const override_CLI :Array = []
var cli_arguments := []

var has_changed := false setget set_has_changed

func _ready():
	save_timer = Timer.new()
	add_child(save_timer)
	save_timer.connect("timeout", self, "_on_save_timer_timeout")
	save_timer.one_shot = true
	
	cli_arguments = OS.get_cmdline_args()
	if len(override_CLI) > 0 and Globals.is_editor_build():
		cli_arguments = override_CLI
	ensure_directories()
	if "--reset-config" in cli_arguments:
		printerr("(Config.gd) _ready: ","--reset-config passed, resetting config")
		save()
	elif not ResourceLoader.exists(save_path):
		printerr("(Config.gd) _ready: ","No existing config at ",save_path,'! making one now')
		save()
	elif load_on_ready and not '--ignore-config' in cli_arguments:
		config_load()
#	print("Config is ",JSON.print(c.to_dict(), "	"))

func switch_current_profile(profile:ConfigProfile):
	var idx = c.profiles.find_user(profile)
	if idx >= 0 and idx != c._current_profile_index:
		c._current_profile_index = idx
		Globals.hard_reload()

func check_if_changed() -> bool:
	return has_changed


func ensure_directories():
	var dir = Directory.new()
	for path in ["pesterlogs", "custom/themes", "custom/plugins", "custom/quirk_presets"]:
		path = "user://" + path
		dir.open("user://")
		if not dir.dir_exists(path):
			dir.make_dir_recursive(path)

func _get(property):
	if property in c:
#		print("(Config) _get: on internal config resource: ",property)
		return c.get(property)
	else:
		match property:
			"save_path":
				return save_path
			"load_on_ready":
				return load_on_ready
		return null


func _set(property, value) -> bool:
	print("(Config.gd) _set: ","set ",property," to ",value)
	if property in c:
		print("(Config.gd) _set: ","on config resource")
		c._set(property, value)
		return true
	else:
		print("(Config.gd) _set: ","on self")
		set(property, value)
		return true


func save(path:=save_path):
	print("(Config.gd) save: ","Saving to ",path)
	if '--readonly-config' in cli_arguments:
		printerr("(Config.gd) save: ","Didnt save because '--readonly-config' is active")
		return
	var out = ResourceSaver.save(path, c)

	if out == OK:
		emit_signal("config_saved")
	else:
		printerr("Error occured while saving!! ",out)
	has_changed = false
	return out

func config_load(path:=save_path):
	print("(Config.gd) config_load: ","Loading from ",path)
	if ResourceLoader.exists(path):
		var out = ResourceLoader.load(path, "", true)
		if out == null:
			var err = "Couldnt load for unknown reasons"
			printerr("(Config.gd) config_load: ",err)
			emit_signal("loading_failed", err)
			return false
		elif out.format_version != VERSIONS.config_format:
			var err = "Target config has format version {0}, which mismatches the expected version of {1}".format([out.format_version, VERSIONS.config_format])
			printerr("(Config.gd) config_load: ",err)
			emit_signal("loading_failed", err)
			return false
		elif not out is pcConfig:
			var err = "File corrupted or not a config"
			printerr("(Config.gd) config_load: ",err)
			emit_signal("loading_failed", err)
			return false
		if out.profiles.size() == 0:
			printerr("(Config.gd) config_load: ","what. how does this have 0 profiles")
			printerr("            ^^^^^^^^^^^   Im adding a new one. this is fine. this is fine.")
			breakpoint
			out.profiles.add_user(ConfigProfile.new())
		print("(Config.gd) config_load: ","Config succesfully loaded!")
		c = out
		has_changed = false
		emit_signal("config_loaded")
		return true
	else:
		var err = "No such file: "+str(path)
		printerr("(Config.gd) config_load: ",err)
		emit_signal("loading_failed", err)
		return false


func _notification(what):
	if what in [MainLoop.NOTIFICATION_WM_FOCUS_OUT, MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST, MainLoop.NOTIFICATION_APP_PAUSED]:
		if check_if_changed():
			save()
	elif what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		print("(Config.gd) _notification: ","caught quit request")
		for user in Config.chumroll:
			user._set("mood",'offline')
		save()
		if is_instance_valid(Globals.irc):
			Globals.irc.quit("Quit: godot "+str(VERSIONS.application)+" <3")
		call_deferred("__quit")

func __quit():
	print("(Config.gd) __quit: ","goodbye <3")
	get_tree().quit() # default behavior


func _on_save_timer_timeout():
	save()


func set_has_changed(new:bool):
	if new:
		save_timer.start(3)
	has_changed = new
