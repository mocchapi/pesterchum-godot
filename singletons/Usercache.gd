extends Node
class_name _UserCache

var user_cache := {}

var memo_cache := {} # TODO: remove this in favor of conversation_cache

var conversation_cache := {}

signal conversation_opened(conversation)
signal conversation_closed(conversation)


signal conversation_created(conversation)
signal user_created(user)

func _ready():
	user_cache[Config.c.current_profile.chumhandle] = Config.c.current_profile

	for list in [Config.chumroll, Config.blocklist]:
		for user in list:
			user._set("mood","offline")
			ingest_user(user)
	
	for robot_handle in Config.robots:
		get_user(robot_handle)
		get_conversation(robot_handle).allow_auto_open = false

		

func ingest_user(user:ConfigUser)->void:
	# Manually adds an existing user to the cache
	user_cache[user.chumhandle] = user
	user.connect("mood_value_changed", self, "_on_user_mood_changed", [user])
	user.connect("color_value_changed", self, "_on_user_color_changed", [user])

	emit_signal("user_created", user)

func _on_user_mood_changed(new, old, user):
	Globals.pesterlayer.emit_signal("user_mood_changed", new, old, user)

func _on_user_color_changed(new, old, user):
	Globals.pesterlayer.emit_signal("user_color_changed", new, old, user)

func ingest_conversation(conversation:ConfigConversation):
	# Manually adds an existing conversation to the cache
	conversation_cache[conversation.title] = conversation
	_conversation_gearup(conversation)
	emit_signal("conversation_created", conversation)

func get_conversation(title:String)->ConfigConversation:
	var do_print = title != Config.settings.server.legacy_mood_channel
	if do_print:
		print("(Usercache) get_conversation: grabbing "+title)
	if has_conversation(title):
		return conversation_cache[title]
	else:
		if do_print: print("(Usercache) get_conversation: didnt exist; making now")
		var new:ConfigConversation
		if Globals.validate_channel(title):
			new = MemoConversation.new()
		else:
			new = PrivmsgConversation.new(get_user(title))
		new.title = title
		ingest_conversation(new)
		return new

func _conversation_gearup(conversation:ConfigConversation):
	conversation.connect("opened", self, "_on_conversation_opened", [conversation])
	conversation.connect("closed", self, "_on_conversation_closed", [conversation])
	conversation.connect("focus_requested", Globals.pesterlayer, "emit_signal", ["focus_conversation", conversation])

func _on_conversation_closed(conversation:ConfigConversation):
	emit_signal("conversation_closed", conversation)

func _on_conversation_opened(conversation:ConfigConversation):
	print("(usercache) _on_conversation_opened: caught open ",conversation.title)
	emit_signal("conversation_opened", conversation)

func has_conversation(title:String):
	return conversation_cache.has(title)


func has_user(nickname)->bool:
	if nickname == "" or nickname == Config.current_profile.chumhandle:
		return true
	return user_cache.has(nickname)

func get_user(nickname:String) -> ConfigUser:
	if nickname == "" or nickname == Config.current_profile.chumhandle:
		return Config.current_profile
	if has_user(nickname):
		return user_cache[nickname]
	else:
		var new = ConfigUser.new(nickname)
		new.submit_change = false
		new.is_robot = nickname in Config.robots
		ingest_user(new)
		return new

func get_all_conversations()->Array:
	return conversation_cache.values()

func get_open_conversations()->Array:
	var out := []
	for convo in conversation_cache.values():
		if convo.is_open:
			out.append(convo)
	return out


func _on_IRCClient_on_join(location)->void:
	var conv:MemoConversation = get_conversation(location)
	conv.lock_open_state = ((location == Config.settings.server.legacy_mood_channel) and Config.settings.server.should_obey_legacy_moods()) or conv.lock_open_state
	conv.is_participating = true
	conv.participants.add_user(Config.current_profile)
	conv.open()
	if conv.is_open:
		conv.request_focus()

func _on_IRCClient_on_user_join(user, location):
	if (location == Config.settings.server.legacy_mood_channel) and Config.settings.server.should_obey_legacy_moods():
		return
	user = Globals.irc.split_client_identifier(user)[0]
	var memo = get_conversation(location)
	memo.participants.add_user(get_user(user))

func _on_IRCClient_on_channel_user_list_updated(channel, userlist):
	if channel == Config.settings.server.legacy_mood_channel and Config.settings.server.should_obey_legacy_moods():
		return
	var memo = get_conversation(channel)
	var out := []

	for item in userlist:
		if item[0] in ['~','+','%','&','@']:
			item = item.substr(1,-1)
		if memo.participants.default_metadata:
			out.append( [get_user(item), memo.participants.default_metadata.duplicate(true)] )
		else:
			out.append( [get_user(item), {}] )
	memo.participants.replace_users(out)


func _on_IRCClient_on_part(location, reason):
	pass # Replace with function body.
	var memo:MemoConversation = get_conversation(location)
	memo.is_participating = false
	memo.emit_signal("left")


func _on_IRCClient_on_user_part(user, location, reason):
	# User left a channel, so remove them from the cached MemoConversation
	if location == Config.settings.server.legacy_mood_channel and Config.settings.server.should_obey_legacy_moods():
		return
	user = Globals.irc.split_client_identifier(user)[0]
	printerr("(Usercache) _on_IRCClient_on_user_part: ","user "+user+" leave ",location, '(',reason,')')
	var memo = get_conversation(location)
	var user_obj = get_user(user)
	if memo.participants.contains_user(user_obj):
		memo.participants.remove_user(user_obj)
	else:
		printerr("(Usercache) _on_IRCClient_on_user_part: ",user," was not in this memo: ",memo.participants.get_users())

func _on_IRCClient_on_user_quit(user, reason):
	print("(Usercache) _on_IRCClient_on_user_quit: ","user ",user,' QUIT with reason: ', reason)
	if not has_user(user):
		return
	get_user(user)._set("mood", "offline")
	for key in memo_cache:
		_on_IRCClient_on_user_part(user, key, reason)


func _on_IRCClient_on_nick_changed(new_nick):
	_on_IRCClient_on_user_nick_changed(Config.current_profile.chumhandle, new_nick)


func _on_IRCClient_on_user_nick_changed(old_nick, new_nick):
	# TODO: maybe remove this? a user changing nick should maybe not get the history changed
	# IE a canon handle switches to a personal handle -> all canon history is rewritten to be to personal handle
	
#	var usr = get_user(old_nick)
#	usr._set('chumhandle', new_nick)
#	user_cache.erase(old_nick)
#	user_cache[new_nick] = usr

	# Yeah thats. this would also change the chumroll. bad! just handle it like a <use went offline> <diff user went online>
	var old_usr = get_user(old_nick)
	old_usr._set("mood", "offline")
	
	var new_usr = get_user(new_nick)



func _on_IRCClient_on_draft_metadata_received(location, key, value):
	# TODO: maybe move this to Pesterlayer
#	printerr(location, '  ', key,' -> ',value)
	if Globals.validate_channel(location):
		pass
	else:
		var user := get_user(location)
		match key:
			'mood':
				var mood_name = Globals.pesterlayer.idx_to_mood(int(value))
				print("UserCache (_on_IRCClient_on_draft_metadata_received): Setting MOOD for ", user.chumhandle, " to ",mood_name, " (",value,")")
				if mood_name:
					user._set("mood", mood_name)
			'color':
				var color = Color(value)
				print("UserCache (_on_IRCClient_on_draft_metadata_received): Setting COLOR for ", user.chumhandle, " to ",color.to_html(false))
				user._set("color", color)
