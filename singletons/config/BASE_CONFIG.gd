extends Resource
class_name _baseconfig, "res://assets/texture/icons/chat.png"


signal value_changed(key, value, old_value)
var options := {} setget ,get_options

var submit_change :bool = true


# Base class for most of our custom resources
# All _baseconfig resources can be used with settingspage.tscn

# To make something saveable, make it an export variable
# To add a short description for what the value determines,
#  add a string constant with the same name as the variable with "_doc" added to the end.
# These docstrings are user-facing, so make sure therye not overly technical
# Variable names are ALSO user-facing, so dont make them random gibberish.
# Variable names starting with an underscore (_) do not ever get shown to the user
#
# Example:
# 	export(bool) var enable_epicness := true
# 	const enable_epicness_doc := "Determines if the client is epic or cringe."



func _init():
	_re_index()



func get_documentation(key:String) -> String:
	var out = get(key+"_doc")
	if out is String:
		return out
	return ""

var include_values_in_str:=true
func _to_string():
	var out = "config:["
	var opts = get_options()
	for item in opts:
		if include_values_in_str:
			out += item+'='+ str(get(item)).substr(0, 15)+';'
		else:
			out += item+';'#+'='+ str(get(item))+';'
	out += '>]'
	return out

func get_options() -> Dictionary:
	if len(options) == 0:
		_re_index()
	return options


# TODO: this is used nowhere and seems extremely janky. Remove?
# vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
func _r_check(value):
	# Recursively maps arrays, dictionaries, and Configs
	# Used to turn Configs into dictionaries
	#
	var out = value

	match typeof(value):
		TYPE_ARRAY: 
			out = []
			for val in value:
				out.append(_r_check(val))
		TYPE_DICTIONARY:
			out = {}
			for key in value:
				out[key] = _r_check(value[key])
		TYPE_OBJECT:
			if out.get_class() == get_class():
				out = value.to_dict(true)
	return out

func to_dict(recursive:=true):
	var out := {}
	for item in options:
		var out2 = get(item)
		if recursive:
			out2 = _r_check(out2)
		out[item] = out2
	return out
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

func get_class():
	return "_baseconfig"

func _re_index():
	# Rebuilds the options dictionary, which contains all the exported 
	options = {}
	for item in get_property_list():
		if item.get('usage') == 8199: # check if item is exported. this is literally undocumented
			var new = {'name':item['name'], 'documentation':get_documentation(item['name'])}
			options[item['name']] = new
			if not has_user_signal(item['name']+"_value_changed"):
				add_user_signal(item['name']+"_value_changed")
			if not has_user_signal(item['name']+"_value_changed+"):
				add_user_signal(item['name']+"_value_changed+")


func _set(property, value) -> bool:
	# This is supposed to trigger when any of the variables are changed, like a universal setter
	# But in practice it does not seem to do that. not sure why. So where it matters, call _set on all configs explicitly
	# This will dispatch the right signals (x_value_changed & value_changed) for responsive GUIs
	# TODO figure that out ^^
	if not property in self:
		printerr("(_baseconfig) _set: ","what the shit. variable {var} ({value}) doesnt exist.".format({'var':property, 'value': value}))
		return false
	if value != get(property):
		var old_value = get(property)
		set(property, value)
		if property in options:
			if Config != null and submit_change:
				Config.has_changed = true

			emit_signal("value_changed", property, value, old_value)
			emit_signal(property+"_value_changed", value, old_value)
			emit_signal(property+"_value_changed+", value)
		return true
	return false
