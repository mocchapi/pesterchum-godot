extends Resource
class_name SelectResult

export var start_index:int
export var text :String
var end_index:int

func _init(new_start_index:int, new_text:String):
	start_index = new_start_index
	text = new_text
	end_index = start_index+len(text)

func _to_string()->String:
	return '<SR:'+text+':['+str(start_index)+'::'+str(end_index)+']>'
