extends _baseconfig
class_name _quirkreplacer, "res://assets/texture/icons/puzzle_right.png"


# Add the following to all inheriting
# Due to gdscript disallowing inherited variables to be overwritten before init
# const name := ""
# const description := ""


func replace(selected_text:String) -> String:
	return selected_text

func printable():
	var name_get = get("name")
	if name_get == null:
		return "?"
	return name_get
