extends _quirkreplacer
class_name QuirkReplacerPrefix

const name = "Prefix"

export var prefix := ""
const prefix_doc := "What gets added before the selected text."

func replace(selected_text:String) -> String:
	return prefix + selected_text

func printable():
	return 'add prefix "'+prefix+'"'
