extends _quirkreplacer
class_name QuirkReplacerSimple

const name = "Simple"

export var replace_with := ""
const replace_with_doc := "What to replace the selected text with."

func replace(selected_text:String) -> String:
	return replace_with


func printable():
	return 'replace with "'+replace_with+'"'
