extends _quirkreplacer
class_name QuirkReplacerCase

const name = "Case"

enum MODES {
	uppercase,
	lowercase,
	camelcase,
	inverted_case,
}

export(MODES) var case_mode := MODES.uppercase
const case_mode_doc := """Casing type to replace with. Examples using "Foo bar":
	"Uppercase": FOO BAR
	"Lowercase": foo bar
	"Camcelcase": Foo Bar
	"Inverted case": fOO BAR
	"""

func replace(selected_text:String) -> String:
	match case_mode:
		MODES.uppercase:
			return selected_text.to_upper()
		MODES.lowercase:
			return selected_text.to_lower()
		MODES.camelcase:
			var out = []
			for word in selected_text.split(' '):
				if len(word) >0:
					word[0] = word[0].to_upper()
				out.append(word)
			return ' '.join(out)
		MODES.inverted_case,_:
			var out = ""
			for chr in selected_text:
				if chr == chr.to_upper():
					out += chr.to_lower()
				else:
					out += chr.to_upper()
			return out

func printable():
	return "change case to "+MODES.keys()[case_mode].replace('_',' ')
