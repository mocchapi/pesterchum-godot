extends _quirkreplacer
class_name QuirkReplacerRepeat # Set this to the name

const name:String = "Repeat" # Printable name here

export var repeat_count := 1 # Add a user-facing parameter
const repeat_count_doc:String = "Amount of times to repeat the selected text" # The function of the parameter in layman. also user-facing


func printable():
	var printable_representation:String = "repeat "+str(abs(repeat_count))+" times"
	# A short string that describes the state of this replacer
	# I.E. "add prefix {prefix}".format({'prefix':prefix})
	return printable_representation


func replace(selected_text:String) -> String:
	var out:String = selected_text.repeat(abs(repeat_count)+1)
	# The actual functionality. selected_text comes straight from the selector
	return out

# Dont forget to add this to the Globals.replacers array!
