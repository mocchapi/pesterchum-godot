extends _quirkreplacer
class_name QuirkReplacerSuffix

const name = "Suffix"

export var suffix := ""
const suffix_doc := "What gets added after the selected text."

func replace(selected_text:String) -> String:
	return selected_text + suffix


func printable():
	return 'add suffix "'+suffix+'"'
