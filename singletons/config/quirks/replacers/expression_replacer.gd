extends _quirkreplacer
class_name QuirkReplacerExpression


const name:String = "Simplified code"# Printable name here

export var expression_code := """selected_text.replace(' ','_')"""# Add a user-facing parameter
const expression_code_doc:String = 'The Expression code. For more info check "https://docs.godotengine.org/en/stable/classes/class_expression.html"' # The function of the parameter in layman. also user-facing

export var keep_variables := false
const keep_variables_doc := "If enabled, variables will persist between messages and selections."

var prev_env := expression_environment.new()

class expression_environment:
	extends Reference
	# Provides a way to store and read values for expressions if set as base_instance
	var variables := {}
	var result:String
	func line_output(line_number):
		return variables.get('line_'+str(line_number))
	func if_else(condition, if_true, if_false):
		if condition:
			return if_true
		else:
			return if_false
	func store(key:String, value):
		variables[key] = value
		return value
	func read(key:String):
		return variables.get(key)
	func set_result(value:String):
		result = value
		return value
	func _to_string():
		return str(variables)

func printable():
	var printable_representation:String = 'run script "'+expression_code.substr(0, 20)+ ('...' if len(expression_code) > 20 else '')+'"'
	# A short string that describes the state of this replacer
	return printable_representation


func replace(selected_text:String) -> String:
	var tmp = []
	var x := 0
	for item in expression_code.split('\n'):
		x += 1
		if item.strip_edges() == '':
			continue
		tmp.append('store("{0}", {1})'.format(["line_"+str(x), item]))
	var real_code = '['+', '.join(tmp)+'][-1]'
	# we fake newline support by turning the whole thing into an array and taking the last item as output
	# you can use store() and read() to emulate variables
	# All line output is stored as well, readable with line_output(line_number)
	
	var out:String = selected_text
	var expr = Expression.new()
	var err = expr.parse(real_code, ['selected_text'])
	if err != OK:
		printerr("(QuirkReplacerExpression) replace: ","Expression quirk parse error: ",expr.get_error_text())
		return selected_text + ' (expression syntax error {0}: {1})'.format([err, expr.get_error_text()])
	
	var env
	if keep_variables:
		env = prev_env
	else:
		env = expression_environment.new()
	env.store("selected_text",selected_text)
	out = str(expr.execute([selected_text], env))
	if env.result != null:
		out = env.result
	if expr.has_execute_failed():
		printerr("(QuirkReplacerExpression) replace: ","Expression quirk execute error: ",expr.get_error_text())
		printerr("    env:", env)
		return selected_text + ' (expression run error: {1})'.format([err, expr.get_error_text()])
	return out

