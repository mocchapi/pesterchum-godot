extends _quirkreplacer
class_name QuirkReplacerColor

const name = "Color"


export var color := Color.black
const color_doc := "What color to change the selected text to."

func replace(selected_text:String) -> String:
	return "<c=#{color}>{selected_text}</c>".format({
												'color':color.to_html(false),
												'selected_text':selected_text
												})

func printable():
	return "colorize to #"+color.to_html(false)
