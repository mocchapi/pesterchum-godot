extends _baseconfig
class_name Quirk, "res://assets/texture/icons/puzzle_combined.png"



# selectors set apart pieces of the sent message
# these pieces of the message then get handed to the replacer
# which modifies them in some fun and quirky way
# these modified pieces are then woven back into the original message

export var enabled := true
const enabled_doc := "Disabled quirks get skipped."
export var randomness := 0.0
const randomness_doc := "Determines how often this quirk gets run. Setting this higher means that it will get skipped more often. 0 means it's always triggered, 1 means never, 0.5 means 50% of the time."



export(Resource) var selector = QuirkSelectorSimple.new()
export(Resource) var replacer = QuirkReplacerSimple.new()

func _to_string():
	return '[Quirk:'+printable()+']'

func printable():
	return selector.printable() +' → '+ replacer.printable()

func _init(clone_from=null):
	if clone_from != null:
		print("(Quirk) _init: cloning from: ",clone_from, ' (',clone_from.get_class(),')')
		selector = clone_from.selector.duplicate()
		replacer = clone_from.replacer.duplicate()

func roll_dice() -> bool:
	# returns True if the quirk should be applied,
	# False if it should be skipped
	return rand_range(0.0, 1.0) >= randomness

func _sort_by_index(a, b):
	return a.start_index > b.start_index

func apply(text:String) -> String:
	# Apply the quirk here
	var out = text
	var selector_results = selector.select(text)
	selector_results.sort_custom(self, "_sort_by_index")
	for item in selector_results:
		var replace_result = replacer.replace(item.text)
		var A = out.substr(0, item.start_index)
		var B = out.substr(item.end_index, -1)
		out = A+replace_result+B
	return out
