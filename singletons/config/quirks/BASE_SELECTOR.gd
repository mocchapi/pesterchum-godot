extends _baseconfig
class_name _quirkselector, "res://assets/texture/icons/puzzle_left.png"

# Add the following to all inheriting
# Due to gdscript disallowing inherited variables to be overwritten before init
# const name := ""
# const description := ""

func select(message:String) -> Array:
	return [SelectResult.new(0, message)]


func printable():
	return "?"
