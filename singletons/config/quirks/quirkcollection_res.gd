extends _baseconfig
class_name QuirkCollection

signal quirk_added(quirk,index)
signal quirk_removed(quirk,index)
signal quirk_moved(quirk,index)

export var name := "Collection"
const name_doc := "Name of this collection"

export(Array, Resource) var quirks := []

func _init(clone_from=null):
	if clone_from != null:
		name = clone_from.name
		for item in clone_from.quirks:
			quirks.append(Quirk.new(item))

func apply(text:String):
	var out = text
	for item in quirks:
		if not item is Quirk:
			printerr("(QuirkCollection) apply: item ",item," is not a quirk")
			printerr("    ",item.get_property_list())
		elif item.enabled and item.roll_dice():
			out = item.apply(out)
		else:
			pass
	return out

func add_quirk(quirk:Quirk, at_index=-1):
	if Config != null:
		Config.has_changed = true

	if not quirk in quirks:
		print("(QuirkCollection) add_quirk: ","new quirk: ",quirk.printable())
		if at_index == -1:
			quirks.append(quirk)
		else:
			quirks.insert(at_index, quirk)
		emit_signal("quirk_added", quirk, at_index)

func remove_quirk(quirk:Quirk):
	remove_index(get_index(quirk))

func remove_index(idx:int):
	if Config != null:
		Config.has_changed = true

	if idx != -1:
		var quirk = quirks[idx]
		quirks.remove(idx)
		emit_signal("quirk_removed", quirk, idx)

func get_index(quirk:Quirk):
	return quirks.find(quirk)


func move_quirk(quirk:Quirk, to_index:int, swap:=false):
	if not to_index in range(quirks.size()):
		printerr("(QuirkCollection) move_quirk: ","out of bounds: ",to_index, " vs max of ",quirks.size())
		return
	if Config != null:
		Config.has_changed = true

	var current_idx = get_index(quirk)
	if current_idx != -1 and to_index != current_idx:
		if not swap:
			quirks.remove(current_idx)
			quirks.insert(to_index, quirk)
			emit_signal("quirk_moved", quirk, to_index)
		else:
			var A = quirks[current_idx]
			var B = quirks[to_index]
			
			quirks[current_idx] = B
			quirks[to_index] = A
			emit_signal("quirk_moved", A, to_index)
			emit_signal("quirk_moved", B, current_idx)

var i := 0
func _iter_init(arg):
	i = 0
	return i < quirks.size()

func _iter_next(arg):
	i += 1
	return i < quirks.size()

func _iter_get(arg):
	return quirks[i]

func get_quirk(idx:int):
	return quirks[idx]

func size() -> int:
	return quirks.size()
