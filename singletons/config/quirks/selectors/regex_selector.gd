extends _quirkselector
class_name QuirkSelectorRegex
# Selects with regex

const name = "Regex"

export var regex_matcher := "" setget set_regex_matcher
const regex_matcher_doc := "The regular expression function to select with. For more info check out https://regexr.com/"
var regex = RegEx.new()

func set_regex_matcher(matcher:String):
	regex_matcher = matcher
	regex.compile(regex_matcher)


func select(message:String) -> Array:
	var matches = regex.search_all(message)
	var out := []
	for item in matches:
		out.append(SelectResult.new(item.get_start(), item.get_string()))
	return out


func printable():
	return "regex "+regex_matcher
