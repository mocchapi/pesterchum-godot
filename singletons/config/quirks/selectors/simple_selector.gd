extends _quirkselector
class_name QuirkSelectorSimple
# Selects a substring

const name = "Simple"

export var text_to_select := ""
const text_to_select_doc := ""

export var ignore_case := false
const ignore_case_doc := "If enabled, upper and lower case version will both match."

func select(message:String) -> Array:
	var x := 0
	var out := []
	var matchMsg = message
	var matchWith = text_to_select
	if ignore_case:
		matchMsg = matchMsg.to_lower()
		matchWith = matchWith.to_lower()
	
	while true:
		var idx = matchMsg.find(matchWith, x)
		if idx == -1:
			break
		else:
			out.append(SelectResult.new(idx, message.substr(idx, len(matchWith)) ))
			x = idx+1
	return out


func printable():
	return '"'+text_to_select+'"'
