extends _quirkselector
class_name QuirkSelectorAll
# Selects entire message

const name = "Whole message"

func select(message:String) -> Array:
	return [SelectResult.new(0, message)]


func printable():
	return "entire message"
