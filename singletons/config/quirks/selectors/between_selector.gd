extends _quirkselector
class_name QuirkSelectorBetween
# Selects a substring

const name = "Between"

export var starting_text := "["
const text_to_select_doc := "Starting point. Everything in the message after this until the ending text gets selected"

export var ending_text := "]"
const ending_text_doc := ""

export var ignore_case := false
const ignore_case_doc := "If enabled, upper and lower case version will both match."

export var include_start_and_end := true
const include_start_and_end_doc := "If enabled, includes the start and end text in the selection."

func select(message:String) -> Array:
	var x := 0
	var out := []

	var seeking_start := true
	var last_start = 0
	for chr in message:
		if seeking_start:
			var matchwith = message.substr(x, len(starting_text))
			matchwith = matchwith.to_lower() if ignore_case else matchwith
			if matchwith == (starting_text.to_lower() if ignore_case else starting_text):
				seeking_start = false
				last_start = x
		else:
			var matchwith = message.substr(x, len(ending_text))
			matchwith = matchwith.to_lower() if ignore_case else matchwith
			if matchwith == (ending_text.to_lower() if ignore_case else ending_text):
				seeking_start = true
				var start
				var end
				if include_start_and_end:
					start = last_start
					end = x+len(ending_text)-start
				else:
					start = last_start+len(starting_text)
					end = x-start
				out.append(SelectResult.new(start, message.substr(start, end)))
		x += 1
	return out


func printable():
	return 'text between "{0}" and "{1}"'.format([starting_text, ending_text])
