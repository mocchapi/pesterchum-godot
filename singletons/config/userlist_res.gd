extends _baseconfig
class_name ConfigUserlist

# Serialisable list of users. Useful for chumroll and blocklist
# TODO: check whats up with all these debug prints


signal user_removed(user)
signal user_added(user)
signal list_updated(user, added_else_removed)
signal list_replaced(previous_list)

export(Array, Resource) var array:Array # [ [ConfigUser, Dictionary (for metadata)] ]
const array_doc := "List of users & their metadata."

var just_removed_metadata
export var default_metadata:Dictionary

func _init(firstfills:Array = array):

	# v\/v Leaving this here for when i inevitably have to figure out why this is class is So Weird
#	if len(firstfills) > 0 or len(array) > 0:
#		print("(ConfigUserlist) _init: "," overwriting array ",array,' with firstfills ',firstfills)

	if firstfills != null and len(firstfills) > 0:
		array = firstfills
		_validate_array()
	else:
		call_deferred("_validate_array")

func _validate_array():
	if len(array) > 0:
		if typeof(array[0]) != TYPE_ARRAY:
			# In case of [user, user]
			# instead of [[user, metadata], [user, metadata]]
			printerr("(ConfigUserlist) _validate_array: array is of [user] instead of [[user, metadata]], fixing now")
			printerr("                    ^^^^^^^^^^^^^^^^  was: ",array)
			var cpy = array
			array = []
			for item in cpy:
				if default_metadata == null:
					array.append([item, {}])
				else:
					array.append([item, default_metadata.duplicate(true)])
			printerr("                    ^^^^^^^^^^^^^^^^  now: ",array)


func contains_user(user:ConfigUser)->bool:
	return find_user(user) != -1

func contains_handle(handle:String)->bool:
	for item in array:
		if item[0].chumhandle == handle:

			return true
	return false

func get_user_metadata(user:ConfigUser):
	var idx := find_user(user)
	if idx == -1:
		printerr("(ConfigUserlist) get_user_metadata: ",'user "'+user.chumhandle+'" not in this list')
		return null
	return array[idx][1]

func set_user_metadata(user:ConfigUser, values:Dictionary)->void:
	var idx := find_user(user)
	if idx == -1:
		printerr("(ConfigUserlist) set_user_metadata: ",'user "'+user.chumhandle+'" not in this list')
		return
	array[idx][1] = values

func add_user(user:ConfigUser)->void:
	print("(ConfigUserlist) add_user: ","add ",user.chumhandle)
	if contains_user(user):
		return
	if user.mood == 'offline' and not user is ConfigProfile:
		user.request_mood()
	if default_metadata == null:
		array.append([user, {}])
	else:
		array.append([user, default_metadata.duplicate(true)])
	print("(ConfigUserlist) add_user: ","current users: ",array)
	emit_signal("list_updated", user, true)
	emit_signal("user_added", user)

func remove_user(user:ConfigUser)->void:
	var idx = find_user(user)
	if idx == -1:
		printerr("(ConfigUserlist) remove_user: ",'user "'+user.chumhandle+'" not in this list')
		return
	just_removed_metadata = array[idx][1]
	array.remove(idx)
	emit_signal("list_updated", user, false)
	emit_signal("user_removed", user)
	just_removed_metadata = null

func get_just_removed_metadata()->Dictionary:
	# Returns the metadata of the user that was just removed
	# Only accessible inside signal connections to "user_removed" and "list_updated"
	# otherwise null
	return just_removed_metadata

func clear(emit_signals:=true)->void:
	if emit_signals:
		print("(ConfigUserlist) clear: removing all users")
		for item in array:
			remove_user(item[0])
	else:
		printerr("(ConfigUserlist) clear: cleared list, no signals emitted")
		array = []

func replace_users(new_users:Array):
	# Replaces the internal user list with something new and exciting
	# DOES NOT EMIT THE NORMAL SIGNALS! this can desync things!
	# Only emits "list_replaced" with a copy of the old array
	printerr("(ConfigUserlist) replace_users: replacing internal user list with ",new_users)
	printerr("                 ^^^^^^^^^^^^^  this emits ONLY `list_replaced`!")
	var old = array
	array = new_users
	_validate_array()
	emit_signal("list_replaced", old)

func find_user(user:ConfigUser)->int:
	var x:=0
	for item in array:
		if item[0].chumhandle == user.chumhandle:
			# TODO: find a way to make sure only two instances of a user can exist
			# IE, when loading from disk, oftem item[0] != user = false
			# but item[0].chumhandle == user.chumdhandle = true
			# This is disk loading shenanigans
			
			return x
#		print(item[0].chumhandle ,' != ', user.chumhandle)
		x += 1
	return -1

func get_users()->Array:
	var out = []
	for item in array:
		out.append(item[0])
	return out

func get_handles()->Array:
	var out := []
	for item in array:
		out.append(item[0].chumhandle)
	return out

var i := 0
func _iter_init(arg):
	i = 0
	return i < array.size()

func _iter_next(arg):
	i += 1
	return i < array.size()

func _iter_get(arg):
	return array[i][0]

func get_index(idx:int):
	return array[idx][0]

func size() -> int:
	return array.size()
