extends _baseconfig
class_name ConfigSettings

# Container for all global/client settings

export(Resource) var connection = ConfigSettingsConnection.new()
const connection_doc := "Options about how to connect to the server."

export(Resource) var server = ConfigSettingsServer.new()
const server_doc := "Options for compatibility with different servers."

export(Resource) var interface = ConfigSettingsInterface.new()
const interface_doc := "Various options about the user interface."

export(Resource) var sound = ConfigSettingsSound.new()
const sound_doc := "Options for the various sounds."

export(Resource) var flags = ConfigSettingsFlags.new()
const flags_doc := "Experimental and WIP options."
