extends ConfigUser
class_name ConfigProfile

export var theme:="Pesterchum"
const theme_doc := "Theme to use for this profile. (Requires a reload)"

export var chums_only := false
const chums_only_doc := "If enabled, ignores all incoming pester attempts from users that arent on your chumroll."

export var random_encounters := false
const random_encounters_doc := "If enabled, allows random users to pester you through the 'random encounter' button."

export(Array, Resource) var autojoin_memos:=["#pesterchum_godot"]
const autojoin_memos_doc := "Memos to join on startup."

export var nickserv_password := ""
const nickserv_password_doc := "Password to authenticate to nickServ with. Leave blank to disable."

export(Array, Resource) var quirks = QuirkCollection.new()
const quirks_doc := "Text quirks."

