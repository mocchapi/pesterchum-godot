extends _baseconfig
class_name ConfigSettingsServer


export var nickserv_handle := "NickServ"
const nickserv_handle_doc := "Handle of the NickServ service. Note that profile passwords are sent to this user, so make absolutely sure that you set this to the correct handle."

export var chanserv_handle := "ChanServ"
const chanserv_handle_doc := "Handle for the ChanServ service."

export var calsprite_handle := "calSprite"
const calsprite_handle_doc := "Handle for the calSprite bot. Leave empty to disable."

export var randomencounter_handle := "randomEncounter"
const randomencounter_handle_doc := "Handle for the random encounter bot. Leave empty to disable."



export(_globals.SERVERMODES) var server_mode := _globals.SERVERMODES.NORMAL
const server_mode_doc := "Determines how the app behaves.\nIn legacy mode, old systems for mood & color exchanges are used. Use this if the server does not support metadata protocol\nIn irc mode, all pesterchum functionality is disabled. Use this on unrelated IRC servers"

export var obey_legacy_moods := true
const obey_legacy_moods_doc := "Uses the legacy mood channel to listen for & answer mood status requests from older/outdated pesterchum clients. Does not require legacy mode to be enabled"

export var legacy_mood_channel:="#pesterchum"
const legacy_mood_channel_doc := "Channel where legacy mood updates are exchanged"


func is_irc_mode()->bool:
	return server_mode == _globals.SERVERMODES.IRC

func is_legacy_mode()->bool:
	return server_mode == _globals.SERVERMODES.LEGACY

func is_normal_mode()->bool:
	return server_mode == _globals.SERVERMODES.NORMAL


func should_obey_legacy_moods()->bool:
	return is_legacy_mode() or (obey_legacy_moods and not is_irc_mode())
