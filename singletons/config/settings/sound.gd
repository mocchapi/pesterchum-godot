extends _baseconfig
class_name ConfigSettingsSound

export var focused_conversation_sounds := true
const focused_conversation_sounds_doc := "Play a sound everytime a message is sent in the conversation you are directly looking at."

export var unfocused_conversation_sounds := true
const unfocused_conversation_sounds_doc := "Play a sound everytime a message is sent in a conversation you are not directly looking at."

export var mention_sounds := true
const mention_sounds_doc := "Play a sound when someone sends a message containing your chumhandle."

export var new_privmsg_sounds := true
const new_privmsg_sounds_doc := "Play a sound when someone starts a new direct conversation with you."
