extends _baseconfig
class_name ConfigSettingsInterface

export var show_timestamps_in_pesters := true
const show_timestamps_in_pesters_doc := "If enabled, displays the time a message was sent in [HH:MM] format in one-on-one chats."

export var show_timestamps_in_memos := true
const show_timestamps_in_memos_doc := "If enabled, displays the time a message was sent in [HH:MM] format in memo chats."

export var relative_memo_timestamps := false
const relative_memo_timestamps_doc := "Changes a user's displayed time in memos according to the 'future' or 'past' time relative to now. Requires 'show timestamps in memos' to be enabled."

export var use_hiDPI :bool= false
const use_hiDPI_doc := "Renders at a much higher resolution, but may be too small for some screens. Possibly not supported on linux."

export var show_chum_color_in_lists := true
const show_chum_color_in_lists_doc := "If enabled, sets the color of a chum's name on chumroll and memo lists as their text color."

export var contrast_chum_list_background := false
const contrast_chum_list_background_doc := "If enabled, changes the background color of a chum's name on chumroll and memo lists to a lighter gray if their text color is hard to see on a black background. Chum list colors must be enabled."

export var bbcode_enabled := true
const bbcode_enabled_doc := "Determines if text colors and emotes are displayed in chat."
