extends _baseconfig
class_name ConfigSettingsConnection

export var hostname := "irc.pesterchum.xyz"
const hostname_doc := "URL or IP adress of the server."

export var port :int = 6697 if OS.get_name() != "HTML5" else 8443
const port_doc := "Port of the server."

export var use_tls := true
const use_tls_doc := "Connect over secure TLS. Disabling this means your chats are not encrypted."


export var use_websocket :bool= false setget ,get_use_websocket
const use_websocket_doc := "If enabled, will use the websocket protocol instead of standard TCP."

export var validate_certificate := true
const validate_certificate_doc := "Validate server's SSL certificate. Recommended to leave enabled."

func get_use_websocket()->bool:
	# Forces browser to always connect over websocket
	# Because JS/webasm does not support normal sockets
	if OS.get_name() ==  "HTML5":
		return true
	else:
		return use_websocket
