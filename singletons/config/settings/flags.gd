extends _baseconfig
class_name ConfigSettingsFlags

export(_globals.OVERRIDE) var force_desktop:int = _globals.OVERRIDE.DONT_OVERRIDE
const force_desktop_doc := "Forces the theme to be in desktop or phone mode. Also frees the 'display mode' lock. Note that this may result in an unusable interface."

export(_globals.DISPLAYMODES) var display_mode:int = _globals.DISPLAYMODES.SINGLEWINDOW if Globals.running_on_phone(true) else _globals.DISPLAYMODES.MULTIWINDOW
const display_mode_doc := "Sets the way windows are organized. This is always locked to 'single window' on phones."

export(_globals.VKMODE) var virtual_keyboard_mode:int = _globals.VKMODE.RESIZE
const virtual_keyboard_mode_doc := "Determines the way the app behaves on phones when the keyboard is visible."

export(float) var virtual_keyboard_offset:float = 10.0
const virtual_keyboard_offset_doc := "Extra pixels to add/subtract to the height of the text bar when a virtual keyboard is open. Increase this if your keyboard is covering the text youre typing."

export(bool) var hide_host_messages := true
const hide_host_messages_doc := "If enabled, (numerical) messages from the host wont be displayed in a chat window. (not working atm)"

export(bool) var check_for_updates_on_start :bool = true setget ,get_check_for_updates_on_start
const check_for_updates_on_start_doc:String = "Will notify you if a new version of pesterchum godot is available when you open the app."

func get_check_for_updates_on_start()->bool:
	if OS.get_name() ==  "HTML5":
		return false
	else:
		return check_for_updates_on_start
