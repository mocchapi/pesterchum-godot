extends _baseconfig
class_name pcConfig


# Version of the config file this was saved as
# If this mismatches, the config loader will refuse to load to avoid errors (in theory)
# Add 1 for every release with breaking changes
export var format_version:int = VERSIONS.config_format


export(Array, Resource) var profiles = ConfigUserlist.new([[ConfigProfile.new(), {}]])
const profiles_doc := "Array of profile resources."

export var _current_profile_index := 0
const current_profile_index_doc := "Index of the selected profile."

export(Resource) var blocklist = ConfigUserlist.new()
const blocklist_doc := "Array of users that are blocked."

export(Resource) var chumroll = ConfigUserlist.new()
const chumroll_doc := "Array of users that are on the chumroll."

export(Resource) var settings = ConfigSettings.new()
const settings_doc := "Settings collection"

var current_profile:ConfigProfile setget ,get_current_profile
const current_profile_doc := "Reference to current profile."

var robots := [settings.connection.hostname, settings.server.randomencounter_handle, settings.server.nickserv_handle, settings.server.chanserv_handle, settings.server.calsprite_handle]

func get_current_profile() -> ConfigProfile:
	if profiles.size() > 0:
		if not _current_profile_index in range(profiles.size()):
			_current_profile_index = 0
		return profiles.get_index(_current_profile_index)
	return null

func _set(key, value):
	if key in self:
		return ._set(key, value)
	else:
		print("(pcConfig) _set: ","no such key: ",key)
