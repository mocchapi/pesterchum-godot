extends _baseconfig
class_name ConfigMemo

# -- deprecated in favor of MemoConversation --

# TODO: (done)
# upgrade memos to make use of ConfigConversation instead

# Info related to memos. Similar to userlist, but users are not serialised & saved
# Also contains ConfigMemoUsers instead of ConfigUsers, for timeframe reasons
# altho now that i think of it, maybe thats better to keep track of in here?

signal user_removed(user)
signal user_added(user)
signal list_updated(user, added_else_removed)

export var name :String
const name_doc := "#name of the memo"

export var description := ""
const description_doc := "Description of the memo"

var users:={}
# Dict of ConfigMemoUsers. not saved

func update_userlist(configusers:Array):
	var to_delete := []
	for item in users:
		if not item in configusers:
			to_delete.append(item)
	
	for item in to_delete:
		remove_user(users[item])

	for item in configusers:
		add_user(item)

func get_user_dispatch(user) -> String:
	var frame = get_user_current_frame(user)
	var out := "PESTERCHUM:TIME>"
	if frame['timeframe'] == 0:
		out += "i"
		return out
	elif frame['timeframe'] > 0:
		out += 'F'
	else:
		out += 'P'
	out += str(frame['hours']).pad_zeros(2)+":"+str(frame['minutes']).pad_zeros(2)
	return out

static func unix_to_hour_minute(timeframe:int):
	var hours_minutes = Time.get_datetime_dict_from_unix_time(abs(timeframe)*60)
	var hours = hours_minutes['hour'] + (hours_minutes['day']-1)*24 + (hours_minutes['month']-1)*31*24 # close enough, hour slider only goes up to 1000
	var minutes = hours_minutes['minute']
	return {'hour': hours, 'minute':minutes, 'timezone':Time.get_time_zone_from_system()['bias'], 'pos':1.0 if timeframe >= 0 else -1.0}

func set_user_timeframe(user, timeframe:int) -> bool:
	if user == "":
		user = Config.current_profile.chumhandle
	var frame = get_user(user, true)
	var new = frame['current'] != timeframe
	frame['current'] = timeframe
	print("(ConfigMemo) set_user_timeframe: ","set timeframe to ",timeframe," for ",user)
	if not frame['frames'].has(timeframe):
		print("(ConfigMemo) set_user_timeframe: ","new timeframe registered")
		var frame_out := {'hours':0, 'minutes':0, 'shorthandle':'C'+frame['user'].initials, 'timeframe':timeframe, 'timestring':"", 'fullhandle':""}
		var handle = frame['user'].initials
		var timestamp = get_user_timestamp(user, false)
		var hours_minutes = unix_to_hour_minute(timeframe)
		var hours = hours_minutes['hour']
		var minutes = hours_minutes['minute']
		var reltime := ""
		if hours > 0:
			reltime = str(hours)+" HOUR"
			if hours > 1:
				reltime +="S"
		if minutes >0:
			if len(reltime) > 0:
				reltime +=" AND "
			reltime += str(minutes)
			reltime += " MINUTES"
			if minutes >1:
				reltime+="S"
		
		if timeframe > 0:
			handle = 'F'+handle
			if frame['future_index'] > 1:
				handle += str(frame['future_index'])
			frame['future_index'] += 1
			
			frame_out['fullhandle'] = "FUTURE "+user
			frame_out['timestring'] = reltime + " FROM NOW"
		else:
			handle = 'P'+handle
			if frame['past_index'] > 1:
				handle += str(frame['past_index'])
			frame['past_index'] += 1
			
			frame_out['fullhandle'] = "PAST "+user
			frame_out['timestring'] = reltime + " AGO"
		
		frame_out['hours'] = hours_minutes['hour']
		frame_out['minutes'] = hours_minutes['minute']
		frame_out['shorthandle'] = handle
		frame['frames'][timeframe] = frame_out
	return new

func get_user_timestring(user)->String:
	return get_user_current_frame(user)['timestring']

func get_user_current_frame(user):
	var frame = get_user(user)
	return frame['frames'][frame['current']]

func get_user_timestamp(user, add_brackets := true, relative = Config.settings.interface.relative_memo_timestamps):
	if relative:
		return Globals.pesterlayer.get_timestamp(add_brackets, get_user_timeframe(user)*60)
	else:
		return Globals.pesterlayer.get_timestamp( add_brackets)

func get_user_shorthandle(user):
	return get_user_current_frame(user)['shorthandle']

func get_user_timeframe(user) -> int:
	var frame = get_user(user, true)
	return frame['current']

func add_user(user:ConfigUser):
	if not has_user(user.chumhandle):
		if user.mood == 'offline':
			if not user.request_mood():
				user.call_deferred("request_mood")
		users[user.chumhandle] = {'user':user, 'permission':'', 'frames':{0:{'shorthandle':'C'+user.initials, 'timeframe':0, 'timestring':"RIGHT NOW", 'fullhandle':"CURRENT "+user.chumhandle}}, 'current':0, 'future_index':1, 'past_index':1}
		emit_signal("user_added", user)
		emit_signal("list_updated", user, true)
		return user

func get_user_special_begin(user):
	var userobj = get_user(user)['user']
	var beans := {'color':userobj.color.to_html(false)}
	beans.merge(get_user_current_frame(user))
	return "-- {fullhandle} [color=#{color}][{shorthandle}][/color] {timestring} responded to memo. --".format(beans)

func get_user(nickname, add_if_nonexistant:=true):
	if nickname is ConfigUser:
		nickname = nickname.chumhandle
	if has_user(nickname):
		return users[nickname]
	elif add_if_nonexistant:
		var usr = Globals.usercache.get_user(nickname)
		add_user(usr)
		return users[usr.chumhandle]

func has_user(nickname:String)->bool:
	return users.has(nickname)


func remove_user(user) -> bool:
	if user in users:
		users.erase(user)
		emit_signal("user_removed", user)
		emit_signal("list_updated", user, false)
		return true
	return false


var i := 0
func _iter_init(arg):
	i = 0
	return i < users.size()

func _iter_next(arg):
	i += 1
	return i < users.size()

func _iter_get(arg):
	return users[users.keys()[i]]['user']

func get_index(i:int):
	return users[users.keys()[i]]['user']

func size() -> int:
	return users.size()

func request_list():
	pass
