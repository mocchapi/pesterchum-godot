extends ConfigConversation
class_name MemoConversation

signal joined() # I joined this memo
signal left()   # I left this memo

signal timeframe_selected(user, timeframe)
signal timeframe_added(user, timeframe)

var is_participating:bool = false
# Is the current user inside this memo (join/part)
# Readonly!! changing this is bad news!

var participants:ConfigUserlist
# All users who are in this memo
# not exported because they do not need to be serialised and saved

func _init():
	._init()
	if participants == null:
		participants = ConfigUserlist.new()
		participants.default_metadata = {'current_timeframe':0, 'future_index':0, 'past_index':0, 'permissions':'', 'timeframes':{ 0:{'index':0, 'prefix':'CURRENT', 'when':'RIGHT NOW', 'has_spoken':false} }}
		participants.connect("user_added", self, "_on_user_joined")
		participants.connect("user_removed", self, "_on_user_left")
	
	participants.submit_change = false


func is_legacy_mood_channel():
	return self.get_location() == Config.settings.server.legacy_mood_channel


func parse_message(sender:ConfigUser, content:String, message_type:String):
	
	match message_type:
		"timeframe":
			var hour:int = 0
			var minute:int = 0
			var time_string:String = content.split('>')[1]
			if time_string == 'i':
				pass
			elif time_string.begins_with('F'):
				var split = time_string.substr(1).split(':')
				hour = abs(int(split[0]))
				minute = abs(int(split[1]))
			elif time_string.begins_with('P'):
				var split = time_string.substr(1).split(':')
				hour = -abs(int(split[0]))
				minute = -abs(int(split[1]))
			_set_user_timeframe(sender, hour*60 + minute)

		"message":
			var meta = participants.get_user_metadata(sender)
			if meta == null:
				printerr("memoconversation_res parse_message: metadata is null!")
				printerr(sender, ' :: ', content, ' :: ', message_type)
				return

			var current_timeframe = meta['timeframes'][meta['current_timeframe']]
			
			var split = Globals.parsetools.split_memo_initials(content)
			var start_color = split[0]
			start_color = start_color.replace(sender.initials, '')
			start_color = Globals.parsetools.insert_colors(start_color)
			# "<c=31,34,54>TT" becomes "[color=31,34,54]"
			
			if current_timeframe['has_spoken'] == false:
				_ingest_anymsg(sender, "-- {prefix} {chumhandle} {relative_time} {when} [{initials}] responded to memo. --".format({
								'prefix': current_timeframe['prefix'],
								'when': current_timeframe['when'],
								'chumhandle': sender.chumhandle,
								'relative_time': Globals.parsetools.format_full_reltime(get_user_reltime(sender)),
								'initials': sender.color_text(get_user_time_initals(sender)),
							}))
				current_timeframe['has_spoken'] = true
			
			var timestamp:String = ""
			if Config.settings.interface.show_timestamps_in_memos:
				var add :float = 0
				if Config.settings.interface.relative_memo_timestamps:
					add += meta['current_timeframe']*60
				timestamp = Globals.pesterlayer.get_timestamp(true, add)
			
			var cleaned_message = Globals.parsetools.clean_user_message( split[1] )
			var out = Globals.parsetools.translate_user_message( cleaned_message )
			out = "{0} {1}{2}: {3}".format([timestamp, start_color, get_user_time_initals(sender), out])
			_ingest_anymsg(sender, out)
			return out
		_:
			return .parse_message(sender, content, message_type)



func format_pchum_me(sender:ConfigUser, content:String):
	content = Globals.parsetools.split_memo_initials(content)[1]
	if content.ends_with('</c>'):
		content = content.substr(0, len(content)-4)
	var cleaned = content
	if content.to_lower().begins_with('/me'):
		cleaned = cleaned.substr(3)
	elif content.to_upper().begins_with('PESTERCHUM:ME'):
		cleaned = cleaned.substr(13)
	
	var suffix = ""
	var body = ""

	if len(cleaned) > 0 and cleaned.substr(0,1) != " ":
		# To handle `/me's cat is gaming`
		var split = cleaned.split(" ")
		suffix = split[0]
		split.remove(0)
		body = ' '.join(split)
	else:
		# To handle `/me has no cat`
		body = cleaned
	
	var prefix = ""
	var meta = participants.get_user_metadata(sender)
	if meta == null:
		printerr("(memoconversation_res) format_pchum_me: get_user_metadata() output is null!")
		printerr(sender, " :: ", content)
		prefix = "?"
	else:
		prefix = meta['timeframes'][ meta['current_timeframe'] ]['prefix']
	
	return "-- {prefix} {chumhandle}{suffix} [{initials}] {body} --".format({
		'prefix': prefix,
		'chumhandle':sender.chumhandle,
		'suffix':suffix,
		'initials': sender.color_text( get_user_time_initals(sender) + suffix ),
		'body': body,
	})



func set_own_timeframe(timestamp:int):
	if Config.settings.server.is_irc_mode():
		printerr("(MemoConversation) set_own_timeframe: irc mode is on, timeframe ignored")
	var timestring = _form_timestring(timestamp)
	
	print("(MemoConversation) set_own_timeframe: Setting to ",timestring,' (',timestamp,')')
	sendraw(timestring)

func _form_timestring(timestamp:int):
	var prefix = ""
	if timestamp == 0:
		return "PESTERCHUM:TIME>i"
	elif timestamp > 0:
		prefix = 'F'
	else:
		prefix = "P"
	
	var hours:int = floor(abs(timestamp)/60)
	var minutes:int = abs(timestamp) - hours*60
	var timestring = "{0}{1}:{2}".format([prefix, str(hours).pad_zeros(2), str(minutes).pad_zeros(2)])
	timestring = "PESTERCHUM:TIME>"+timestring
	return timestring


func get_user_reltime(user:ConfigUser)->Array:
	var meta = participants.get_user_metadata(user)
	if meta == null:
		printerr("get_user_time_reltime: meta is null!")
		printerr(user)
		return [0,0]
	var current_timeframe = meta['current_timeframe']
	return Globals.parsetools.timestamp_to_H_M(abs(current_timeframe))

func get_user_time_initals(user:ConfigUser):
	var meta = participants.get_user_metadata(user)
	if meta == null:
		printerr("get_user_time_initials: meta is null!")
		printerr(user)
		return "?"
	var current_timeframe = meta['timeframes'][meta['current_timeframe']]
	var out = current_timeframe['prefix'].substr(0,1)
	out += user.initials
	if current_timeframe['index'] != 0:
		out += str(current_timeframe['index']+1)
		# PTT -> PTT2
		# (+1 because indexes work like current -> past -> past 2)
		#                                          ^^^^ implicitly, the first past is past 1 (or future)
	return out

func _set_user_timeframe(user:ConfigUser, time:int):
	print("(MemoConversation) _set_user_timeframe: in ",get_location(),' set ',user.chumhandle,' time -> ',time)
	var meta = participants.get_user_metadata(user)
	if meta == null:
		printerr("_set_user_timeframe: meta is null!")
		printerr(user, " :: ", time)
		return
	meta['current_timeframe'] = time
	if meta['timeframes'].has(time):
		print("                   ^^^^^^^^^^^^^^^^^^^  Already exists :3")
		emit_signal("timeframe_selected", user, time)
	else:
		var index: int = 0
		var prefix: String
		var when: String

		if time == 0:
			index = 0
			prefix = 'CURRENT'
			when = 'RIGHT NOW'
		elif time < 0:
			index = meta['past_index']
			meta['past_index'] += 1
			prefix = 'PAST'
			when = 'AGO'
		elif time > 0:
			index = meta['future_index']
			meta['future_index'] += 1
			prefix = 'FUTURE'
			when = 'FROM NOW'
		
		print("                   ^^^^^^^^^^^^^^^^^^^  created as ",prefix,' ',index)
		meta['timeframes'][time] = { 'index': index, 'prefix': prefix, 'when': when, 'has_spoken':false }
		emit_signal("timeframe_added",user,time)
	emit_signal("timeframe_selected", user, time)

func leave(also_close:=true):
	Globals.irc.leave_channel(title)
	is_participating = false
	_ingest_sent('{0} ceased responding to memo'.format([Config.current_profile.get_bbcode_name()]), false)
	if also_close and is_open:
		close()
	emit_signal("left")

func join():
	Globals.irc.join_channel(title)

func get_location()->String:
	return title

func _put_reconnect():
	_ingest_sent("\n\n-- You reconnected --\n\n", false)

func _on_user_joined(user:ConfigUser):
	var meta = participants.get_user_metadata(Config.current_profile)
	if meta == null:
		printerr("_on_user_joined: meta is null!")
		printerr(user)
		return
	var timeframe = meta['current_timeframe']
	if timeframe == 0:
		return
	sendraw(
		_form_timestring(
			timeframe
		))
	# Send my timeframes in order so that time indexes like FTT2 or FTT5 are in the right order to new users
	# TODO: consider this. i blocked it off for now since the normal client doesnt seem to do this either
	# There are flood concerns to keep in mind. if someone joins and everyone send their entire timeframe history
	# it could get very spammy
#	print("(MemoConversation) _on_user_joined: Sending my timeframes to new user")
#	var meta = participants.get_user_metadate(Config.current_profile)
#	var timeframes = [] # [[index, timeframe], [index2,timeframe]]
#	for timeframe in meta['timeframes']:
#		timeframes.append([meta['timeframes'][timeframe]['index'] +(len(meta['timeframes']) if timeframe < 0 else 0), timeframe])
#
#	timeframes.sort_custom(self, "_custom_sort")
#
#	timeframes.append([0, ])
#
#	participants.set_user_metadata(user, {'current_timeframe':0, 'future_index':0, 'past_index':0, 'permissions':'', 'timeframes':{ 0:{'index':0, 'prefix':'CURRENT', 'when':'RIGHT NOW', 'has_spoken':false} }})

#func _custom_sort(a,b):
#	return a[0] > b[0]
#	pass

func _on_user_left(user:ConfigUser):
	print(participants.get_just_removed_metadata())
	if participants.get_just_removed_metadata()['timeframes'][participants.get_just_removed_metadata()['current_timeframe']].get('has_spoken', false):
		_ingest_receive(user, '{0} ceased responding to memo'.format([user.get_bbcode_name()]), false)
