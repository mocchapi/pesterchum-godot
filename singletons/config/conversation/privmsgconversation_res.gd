extends ConfigConversation
class_name PrivmsgConversation

export var participant:Resource #ConfigUser

var has_ceased := true
# True when PESTERCHUM:CEASE is last message
# should be et to false whenever a new message it sent or received

func _init(new_participant:ConfigUser=null):
	._init()
	if new_participant != null:
		participant = new_participant
		force_no_sent_quirks = participant.is_robot

func get_location()->String:
	return participant.chumhandle

func send(text:String, no_quirks:=false):
	if has_ceased and not participant.is_robot:
		.send("PESTERCHUM:BEGIN", true)
		has_ceased = false
	.send(text, no_quirks)

func _opened():
	if has_ceased and not participant.is_robot:
		.send("PESTERCHUM:BEGIN", true)
		has_ceased = false

func _closed():
	if not has_ceased and not participant.is_robot:
		.send("PESTERCHUM:CEASE", true)
		has_ceased = true

func parse_message(sender:ConfigUser, content:String, message_type:String):

	if sender.is_blocked():
		allow_auto_open = false
		send("PESTERCHUM:BLOCKED", true)
		return

	var im_sender:bool = sender == Config.c.current_profile
	
	var victim
	if im_sender:
		victim = participant
	else:
		victim = Config.c.current_profile

	var out
	var do_open := true
	has_ceased = false
	match message_type:
		"pchum_begin":
			out = '-- {0} began pestering {1} --'.format([ sender.get_bbcode_name(), victim.get_bbcode_name() ])
		"pchum_cease":
			out = '-- {0} ceased pestering {1} --'.format([ sender.get_bbcode_name(), victim.get_bbcode_name() ])
			has_ceased = true
			do_open = false
		"pchum_unblock":
			out = '-- {0} unblocked {1} --'.format([ sender.get_bbcode_name(), victim.get_bbcode_name() ])
			if im_sender:
				do_open = false
		"pchum_block":
			out = '-- {0} blocked {1} --'.format([ sender.get_bbcode_name(), victim.get_bbcode_name() ])
			if im_sender:
				do_open = false
		"pchum_blocked":
			out = '-- {0} did not receive message from {1} --'.format([ sender.get_bbcode_name(), victim.get_bbcode_name() ])
			if im_sender:
				do_open = false
		"pchum_idle":
			out = '-- {0} is idle --'.format([ sender.get_bbcode_name() ])
			
		
		_:
			if not im_sender and Config.current_profile.chums_only and not Config.chumroll.contains_user(participant):
				_ingest_receive(sender, content, false)
				sendraw("PESTERCHUM:IDLE")
				return out

			if participant.chumhandle == Config.settings.server.nickserv_handle and "identify" in content.to_lower():
				var prev = content
				content = content.replace(Config.current_profile.nickserv_password, '<c=#ce0a14>[PASSWORD REDACTED]</c>')

			return .parse_message(sender, content, message_type)
	
	# todo: make this a better error
#	printerr(out)
	_ingest_anymsg(sender, out, do_open)
	return out
