extends _baseconfig
class_name ConfigConversation


# Seperates conversations from the GUI
# the `is_open` value indicates if a conversation should be visible to the user
# It does *not* mean that the conversation is ongoing or has ended
# I.E. #pesterchum is (usualy) NOT open, but activity still happens
# 


signal sent(text) # appended text was sent by current user
signal received(sender, text) # appended text was sent by another user
signal special(text) # appended text was "special", I.E. a system message ## TODO: do we need this?
# "X changed mood", "X ceased responding", etc can all normal received()s, so. what for

signal new_message(sender, text) # Combination of signal sent(), received() & special()

signal focus_requested() # Conversation should immediatly be shown to the user
# This is a "request" because the chat tabs and whatever else needs to implement this,
# And its not really crucial

signal opened() # Conversation was opened [show/create tab]
signal closed() # Conversation was closed [hide/destroy tab]

export var title:String = ""
const title_doc:String = "Determines the name of the conversation, usually either the username or memo name"

export var is_open:bool = false setget set_is_open
const is_open_doc:String = "Determines if this conversation should be shown to the user in the GUI"
# The reason for this is that for stuff like a blocked user trying to message me, the client
# can automatically answer with a BLOCK without summoning a new tab. among other stuff
# Also, this allows for preserving message history between opening & closing tabs. Also maybe in the future for reading & writing from disk
# Because the usercache node can be kept between theme changes, these objects can also be used to re-build the tabs in other themes

export(Array, String) var chat_log:Array
const chat_log_doc:String = "Complete chat history in [[message_bbcode, sender or null, unix_timestamp]] format."
# Note that sometimes sender_handle is null
# chat_log[-1] is always the most recent message (but the log can be empty, so this might error)

export var unread_messages:int = 0
const unread_messages_doc:String = "Amount of messages that were received while unfocused"
# Because focus isnt tracked in this object, a chat_tabs or similar node should reset this to 0 when it sees fit

export var force_no_sent_quirks:bool = false
const force_no_sent_quirks_doc:String = "Special-cases this conversation to disable quirkify calls to send()"

export var allow_auto_open := true
const prevent_auto_open_doc:String = "If disabled, will not open itself when a message is received, only when the user opens this via GUI"

export var lock_open_state:=false
const lock_open_state_doc:String = "If set to true, will refuse to change the open/close state. Helpful for mandatory or background-only conversations, or blocked users."


func _init():
	pass

func open():
	set_is_open(true)


func close():
	set_is_open(false)




func append_log(text:String, sender:ConfigUser=null):
	# Append the text to the history
	if chat_log == null:
		chat_log = []

	chat_log.append([text, sender, Time.get_unix_time_from_system()])
	emit_signal("new_message", sender, text)


func _ingest_receive(sender:ConfigUser, text:String, open_if_closed:=allow_auto_open)->void:
	# Called when the client receives a message in this conversation
	# Called by Pesterlayer when another users sends a message
	# (so dont call this manually)
	if open_if_closed and is_open == false and not lock_open_state:
		open()
	append_log(text, sender)
	unread_messages += 1
	emit_signal("received", sender, text)

func _ingest_sent(text:String, open_if_closed:=allow_auto_open)->void:
	# Append text & emit the "sent" signal
	# Also called by pesterlayer
	# (so dont call this manually)
	if open_if_closed and is_open == false and not lock_open_state:
		open()
	append_log(text, Config.c.current_profile)
	unread_messages = 0
	emit_signal("sent", text)

func _ingest_special(text:String, open_if_closed:=allow_auto_open):
	if open_if_closed and is_open == false and not lock_open_state:
		open()
	append_log(text, null)
	emit_signal("special", text)

func _ingest_anymsg(sender:ConfigUser, text:String, open_if_closed:=allow_auto_open)->void:
	# shortcut
	if sender == Config.c.current_profile:
		_ingest_sent(text, open_if_closed)
	else:
		_ingest_receive(sender, text, open_if_closed)

func sendraw(text:String):
	Globals.pesterlayer.send_message(
		get_location(),
		text,
		false,
		false
		)

func send(text:String, no_quirks:=false):
	# Send actual real life message to this conversation
	# This is pretty much the path thill take:
	# -> call send here
	# -> thissll call pesterlayer
	# -> pesterlayer will apply its formatting magic
	# -> then itll and it to the IRC node
	# -> the IRC node will eventually emit its own signal that the server ACK'd the privmsg
	# -> pesterlayer will catch that & call the _on_sent() function on this object
	# -> this object will then emit its own `sent` signal
	print("(ConfigConversation) send: ", "Sending '"+text+"' to ",get_location())
	
	var enable_quirks:bool = not (force_no_sent_quirks or no_quirks or text.begins_with('/me') or (text.begins_with('((') and text.ends_with('))')))
	
	Globals.pesterlayer.send_message(
		get_location(),
		text,
		true,
		enable_quirks
		)

func get_log_as_bbcode()->String:
	var out := ""
	for message in chat_log:
		out += message[0]
		out += '\n'
	return out

func set_is_open(state:bool)->void:
	if state != is_open:
		if lock_open_state:
			printerr("(ConfigConversation) set_is_open: ","["+title+"] ","Refused to change is_open to ",state," because lock_open_state is enabled")
			return
		is_open = state
		print("(ConfigConversation) set_is_open: is_open set to ",state)
		if state:
			_opened()
			emit_signal("opened")
			request_focus()
		else:
			_closed()
			emit_signal("closed")
	else:
		printerr("(ConfigConversation) set_is_open: ","["+title+"] ","Did not change is_open to ",state," because it was already that value")

func request_focus():
	if is_open:
		emit_signal("focus_requested")
	else:
		printerr("(ConfigConversation) request_focus: must be open to request focus")

func get_location()->String:
	printerr("(ConfigConversation) get_location: Not implemented! overwrite this function with a callable that returns the send location")
	return ""



func parse_message(sender:ConfigUser, content:String, message_type:String):
	var is_me = sender == Config.current_profile

	match message_type:
		'me':
			var out =  format_pchum_me( sender, content )
			_ingest_anymsg(sender, out)
			return out

		'message', _:
			var cleaned_message = Globals.parsetools.clean_user_message( content )
			var out = Globals.parsetools.translate_user_message( cleaned_message )
			
			var timestamp:String = ""
			if Config.settings.interface.show_timestamps_in_pesters:
				timestamp = Globals.pesterlayer.get_timestamp(true)
			
			out = "{0} {1}: {2}".format([timestamp, sender.initials, out])
			out = sender.color_text(out)
			_ingest_anymsg(sender, out)
			return out



func format_pchum_me(sender:ConfigUser, content:String):
	var cleaned = content
	if content.to_lower().begins_with('/me'):
		cleaned = cleaned.substr(3)
	elif content.to_upper().begins_with('PESTERCHUM:ME'):
		cleaned = cleaned.substr(13)
	
	var suffix = ""
	var body = ""

	if len(cleaned) > 0 and cleaned.substr(0,1) != " ":
		# To handle `/me's cat is gaming`
		var split = cleaned.split(" ")
		suffix = split[0]
		split.remove(0)
		body = ' '.join(split)
	else:
		# To handle `/me has no cat`
		body = cleaned
	
	return "-- {chumhandle}{suffix} [{initials}] {body} --".format({
		'chumhandle':sender.chumhandle,
		'suffix':suffix,
		'initials': sender.color_text(sender.initials + suffix.to_upper()),
		'body': body,
	})


# Ease-of-use functions for the subclasses
func _opened():
	pass

func _closed():
	pass

func _sent(text):
	pass

func _received(sender, text):
	pass
