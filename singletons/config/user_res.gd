extends _baseconfig
class_name ConfigUser

export var chumhandle:String="pesterchumGodot"+str(randi()%999).pad_zeros(3) + str(OS.get_unix_time()).substr(5)
const chumhandle_doc := "The user's chumhandle."

export(Color, RGB) var color:=Color.black
const color_doc := "The user's text & name color"

export(String) var mood:="offline"
const mood_doc := "The mood of this user"

export var is_robot:bool = false
const is_robot_doc:String = "Indicates if this user should be treated as a robot/service, which may change how conversations are handled"

var bbcode_name:String setget ,get_bbcode_name
var bbcode_mood:String setget ,get_bbcode_mood
var mood_image:StreamTexture setget ,get_mood_image
var initials:String setget ,get_initials

func color_contrasts_dark()->bool:
	return Globals.color_contrasts_dark(color)

func _init(new_chumhandle:=chumhandle, new_color:=color, new_mood := mood):
	chumhandle = new_chumhandle
	mood = new_mood
	color = new_color

func request_mood(force_if_metadata:bool=true)->bool:
	if Globals.pesterlayer != null:
		return Globals.pesterlayer.request_mood(chumhandle, force_if_metadata)
	else:
		printerr("(ConfigUser) request_mood: ","Pesterlayer is null; cant request mood for ",chumhandle)
		return false

func get_bbcode_name()->String:
	return '{0} [{1}]'.format([chumhandle, color_text( get_initials() )])

func get_mood_image()->StreamTexture:
	if is_robot:
		return Globals.icons['robot']
	if Globals.pesterlayer == null:
		printerr("(ConfigUser) get_mood_image: ","Pesterlayer is null, cant get image for ",mood)
		return Globals.icons['unknown_mood']
	elif Globals.pesterlayer.moods.has(mood):
		return Globals.pesterlayer.moods.get(mood)['image']
	else:
		printerr("(ConfigUser) get_mood_image: ","no such mood: ",mood)
		return Globals.icons['unknown_mood']

func get_bbcode_mood()->String:
	return "[img]{0}[/img]".format([get_mood_image().get_path()])

func color_text(text:String):
	return "[color=#{color}]{text}[/color]".format({
		'color': color.to_html(false),
		'text': text,
		})

func is_blocked()->bool:
	return Globals.pesterlayer.is_blocked(self)

func get_conversation():
	return Globals.usercache.get_conversation(self.chumhandle)

func is_me()->bool:
	return chumhandle == Config.current_profile.chumhandle

func get_initials()->String:
	if initials != '':
		return initials
	if len(chumhandle) == 0:
		return '??'

	var capital = chumhandle[0].to_upper()

	for i in range(1,len(chumhandle)-1):
		var item = chumhandle[i]
		if item.to_upper() == item and item in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
			capital = item
			break
	initials = chumhandle[0].to_upper() + capital
	return initials

func _to_string():
	return "ConfigUser:[{0};{1};#{2}]".format([chumhandle, mood, color.to_html(false)])
