
# Pesterchum Godot ![icon](./assets/texture/icons/godot_pals.png)

Pesterchum IRC client made with the [Godot engine](https://godotengine.org) 3.5, targeting desktop, android, and the web. Inspired by the [QT Pesterchum client](https://github.com/Dpeta/pesterchum-alt-servers).  
This project intends to replace [Chumdroid](https://www.pesterchum.xyz/) (an old android client), as well as provide a universal experience across webbrowsers, phones, & desktops.  
![Screenshot of the pesterchum theme](./assets/texture/misc/readme_image.png) 

## What's pesterchum??
From the original [Pesterchum](https://github.com/Dpeta/pesterchum-alt-servers) client repository: 
> Pesterchum is an instant messaging client copying the look and feel of clients from Andrew Hussie's webcomic Homestuck.

## How is this different? 
The biggest difference between Pesterchum Godot & [normal Pesterchum](https://github.com/Dpeta/pesterchum-alt-servers) are:  

- Support for android & web  
- Adaptive window scaling
- A lot more powerful (but more complicated) theming options <sup>(Theme support is currently WIP)</sup>

Also this is just a fun little side project :P  

## Getting started
You can download the latest stable builds at [the releases page](https://gitlab.com/mocchapi/pesterchum-godot/-/releases). Just pick the newest one & download the version for your platform!  
These builds are also available on [itch.io](https://mocchapi.itch.io/pesterchum-godot), if you prefer using it & its auto updating launcher
On itch.io you will also find a hosted version of te web build, which you can use without downloading anything (the browser version has been reported to work on IOS, so iphone users are not entirely left out)  

If you run into any problems, you can open an issue here on gitlab, or contact me (mocha) in the **__#development__ channel** of the [original pesterchum's discord server](https://discord.gg/eKbP6pvUmZ).  
_Please don't put issues you have with Pesterchum Godot in any other channel, since those are meant for the original pesterchum._


## So what's the state of affairs
Pesterchum Godot is coming along nicely. things are still missing, but the majority of features for normal use are here.  
Version 1.0 is on the horizon!  
For a more specific & up-to-date feature list, check the [TODO.md file](TODO.md) 

#### These things work:

- Moods, Chumhandles, & colored text
- Memos with time controls
- Random encounters
- Metadata protocol support!
- Multiple profiles
- Highly customizable automatic text quirks
- Compact mobile view or expanded desktop view
- Web browser, android, & desktop versions
- Different themes (WIP) <sup>currently, pesterchum & trollian</sup>
- Global user & memo list
- Custom servers
- Emotes <sup>(i.e. :brocool: becomes ![brocool emote](./assets/texture/emotes/brocool.png))</sup>
- Connecting over Websocket, TCP, & TLS
- Blocking & unblocking users
- Chumrolls
- IRC mode to make it more friendly to normal servers

#### Things that are currently missing:

- Saving pesterlogs to disk
- Animated emotes
- Message sounds
- Many bug fixes

#### Stuff that is really unlikely to happen:

- Background processing for android
- Publishing to android playstore
- Push notifications

#### Things that will never happen:

- IOS versions.  
	Apple makes it incredibly difficult (and expensive) to develop software for their platforms, so unless someone has the know-how & the monetary resources to get it working and put on the IOS App store, this will not happen.  
	You can however use the [web build](https://mocchapi.itch.io/pesterchum-godot) on IOS & add it to your homescreen, which *should* work.

## Attribution

#### Fonts

- [Courier prime](https://quoteunquoteapps.com/courierprime/) is licensed under the [SIL open font license](https://scripts.sil.org/OFL)
- Also we use Noto emoji and GNU freefont! Check the license files in assets/font/source
- GNU unifont
- TYPOSTUCK, by [Speherh](https://fontstruct.com/fontstructors/1247932/speherh) is also used, which is licensed under the [Creative Commons 3.0 Attribution](https://creativecommons.org/licenses/by/3.0/) license.
- W95FA by FontsArena is licensed under the [SIL open font license](https://scripts.sil.org/OFL)

### Textures/images/art

- Most [emotes](assets/texture/emotes) and [mood images](assets/texture/moods) are sourced from [the pesterchum desktop client](https://github.com/Dpeta/pesterchum-alt-servers)  
- Ones that arent from that repository i made myself

### Misc

This software and its "Pesterchum" and "Trollian" themes are inspired by the [Homestuck web comic](https://homestuck.com) by Andrew Hussie & other [artists](https://www.homestuck.com/credits/art) and [musicians](https://www.homestuck.com/credits/sound)  

proper in-app attribution is coming pls dont sue <3
