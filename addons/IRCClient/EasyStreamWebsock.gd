extends EasyStream

class_name EasyStreamWebsock

# Abstracts websocket connections into a drop-in replacement for EasyStream

export var buffer_packets_instead := true
# ignores buffer_unicode_to and emits new_buffer_item for every packet
export var buffer_packets_unicode_compatiblity := true
# put_data will have buffer_unicode_to stripped from the end
# incoming data will have buffer_unicode_to added to the end

var stream: WebSocketClient setget ,get_stream
var status: int = STATUS.DISCONNECTED


var debug_prints:bool = false

func connect_to_host(host:String, port:int, use_ssl:=true, validate_cert=true, cert_hostname:="", blocking=true) -> bool:
	
	disconnect_from_host()
	
	_set_status(STATUS.CONNECTING)
	
	if debug_prints:
		print_debug("connecting to ",host,":",port, (" with ") if use_ssl else (" without "), "SSL. Verifying cert is  ",validate_cert)
		print_debug("with args: ", port,"; ",use_ssl,'; ',validate_cert,'; ', cert_hostname, '; ', blocking)
	
	stream = WebSocketClient.new()
	stream.verify_ssl = validate_cert
	
	stream.connect("connection_closed", self, "_on_stream_closed")
	stream.connect("connection_error", self, "_on_stream_error")
	stream.connect("connection_established", self, "_on_stream_connected")
	stream.connect("data_received", self, "_on_stream_data")

	var real_host = ""
	if use_ssl:
		real_host += 'wss://'
	else:
		real_host += 'ws://'
	real_host += host
	real_host += ':'+str(port)
	
	if debug_prints:
		print_debug("real_host: ",real_host)
	var err = stream.connect_to_url(real_host)
	if err != OK:
		printerr("(EasyStreamWebsock) connection error: ",err)
		emit_signal("on_error")
		emit_signal("connection_error", err)
		emit_signal("on_disconnected")
		return false
	return true

func _on_stream_connected(proto=''):
	_set_status(STATUS.CONNECTED)
	if clear_buffer_on_connect:
		_buffer = ''

func _on_stream_closed(was_clean = false):
	if not was_clean:
		_set_status(STATUS.ERROR)
	_set_status(STATUS.DISCONNECTED)

func _on_stream_error():
	_on_stream_closed(false)


func _on_stream_data():
	var data = stream.get_peer(1).get_packet().get_string_from_utf8()
	
	if debug_prints:
		print_debug(" DATA GET: ",data)
	
	if buffer_packets_instead:
		if buffer_packets_unicode_compatiblity:
			data += buffer_unicode_to
		emit_signal("new_buffer_item", data)
		return
	for chr in data:
		_buffer += chr
		if _buffer.ends_with(buffer_unicode_to):
			emit_signal("new_buffer_item", _buffer)
			_buffer = ""

func adopt_stream(new_stream:StreamPeer):
	printerr("(EasyStreamWebsock) adopt_stream() is not implemented in websockets yet")
	breakpoint

func _set_status(new_status:int):
	if new_status != status:
		status = new_status
		if debug_prints:
			print_debug("Status changed to ",status, ": ", STATUS.keys()[status])
		emit_signal("status_changed", status)
		emit_signal(translation_table_signal[new_status])

func _process(delta):
	if enable_buffer:
		poll()

func put_utf8_string(text:String) -> bool:
	if is_connected_to_host():
		stream.get_peer(1).put_packet(text.to_utf8())
		return true
	return false


func poll():
	if is_connected_to_host():
		stream.poll()

func disconnect_from_host():
	if is_connected_to_host(true):
		_set_status(STATUS.DISCONNECTED)
		return stream.disconnect_from_host()

func is_connected_to_host(include_connecting:=true) -> bool:
	if stream != null:
		if include_connecting:
			return (status == STATUS.CONNECTING) or (status == STATUS.CONNECTED)
		else:
			return status == STATUS.CONNECTED
	return false

func get_status() -> int:
	# For compatibility, emulated by signals
	if stream == null:
		return STATUS.DISCONNECTED
	return status

func get_stream():
	return stream
