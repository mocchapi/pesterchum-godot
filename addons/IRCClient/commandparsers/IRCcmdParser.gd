extends Reference
class_name _IRCcmdParser

var format := []

enum MODE {
	ALL, # ALL in array are true
	NONE, # ALL in array are false
	ANY, # Some in array are true
	ONE # One in array is true
}

class BaseArg:
	var type := "Base"
	
	func confirm(text) -> bool:
		return true

	func __verify_mode(mode:int, results:=[]) -> bool:
		var totals = len(results)
		var positives := 0
		for item in results:
			if item:
				positives += 1
		match mode:
			MODE.ALL:
				return positives == totals
			MODE.NONE:
				return positives == 0
			MODE.ANY:
				return positives != 0
			MODE.ONE:
				return positives == 1
		return false
	
	func _to_string():
		return "["+str(type)+"]"

class Literal:
	extends BaseArg
	# Direct comparison, accepts if the given text is the exact same 
	var name :String
	var case_sensitive :bool
	func _init(new_name:String, new_case_sensitive:=false):
		type = "Literal"
		name = new_name
		case_sensitive = new_case_sensitive
	
	func confirm(text) -> bool:
		return (text == name) or (not case_sensitive and text.to_lower() == name.to_lower())


class LiteralAny:
	extends BaseArg
	# Match one of the given literals
	var names := []
	
	func _init(new_names:Array):
		type = "Literal"
		names = new_names
	
	func confirm(text) -> bool:
		return text in names


class Match:
	extends BaseArg
	var starts_with := []
	var ends_with := []
	var contains := []
	var mode : int
	func _init(new_mode:int ,new_starts_with:=[],new_ends_with:=[],new_contains:=[]):
		type = "Match"
		mode = new_mode
		starts_with = new_starts_with
		ends_with = new_ends_with
		contains = new_contains
	
	func confirm(text) -> bool:
		var out := []
		
		if len(starts_with) > 0:
			out.append(false)
		for item in starts_with:
			if text.starts_with(item):
				out[0] = true
				break

		if len(ends_with) > 0:
			out.append(false)
		for item in ends_with:
			if text.ends_with(item):
				out[1] = true
				break

		if len(contains) > 0:
			out.append(false)
		for item in contains:
			if item in text:
				out[2] = true
				break
		return __verify_mode(mode, out)

class Combine:
	extends BaseArg
	# Combines several args into one, mode is MODE, which determines the success condition
	var args := []
	var mode: int
	func _init(mode:int, new_args:Array):
		type = "Combine"
		args = new_args
	
	func confirm(text:String) -> bool:
		var out := []
		for item in args:
			out.append(item.confirm(text))
		return __verify_mode(mode, out)

class Anything:
	extends BaseArg
	# Literally anything. Always matches
	func _init():
		type = "Any"
	
	func confirm(text) -> bool:
		return true

class Location:
	extends BaseArg
	# #Channel or User
	var allow_channels:bool
	var allow_users:bool
	func _init(new_allow_channels:=true, new_allow_users:=true):
		type = "Location"
		allow_users = new_allow_users
		allow_channels = new_allow_channels
	
	func confirm(text:String) -> bool:
		var is_channel = len(text) > 0 and text[0] in ['#','&'] and not ' ' in text and not ',' in text and not ':' in text

		return (allow_channels and is_channel) or (allow_users and not is_channel)

class Origin:
	extends BaseArg
	# Usually the first argument
	func _init():
		type = "Origin"
	
	func confirm(text:String) -> bool:
		return text.begins_with(':')

class Number:
	extends BaseArg
	# Number
	func _init():
		type = "Number"
	
	func confirm(text:String) -> bool:
		if text.count('.') > 1 or text.count('-') > 1:
			return false
		for chr in text:
			if not chr in ['-','1','2','3','4','5','6','7','8','9','0','.']:
				return false
		return true


func _init():
	format = get_format()

### the actual parser code v v v

func __match_format(msg:Array, format:Array) -> bool:
	if len(format) == len(msg):
		# iterate over all its args
		for i in len(format):
			# check if text matches for every arg
			if not format[i].confirm(msg[i]):
				# Quit if one doesnt
				return false
		return true
	else:
		return false

func matches(msg:Array) -> bool:
	# Verifies if this parser can be used for a given message
	# Iterates over all formats from get_format() and checks them
	# if one of them matches, return true
	# if none do, return false
	# You may overwrite __match_format() if you need more flexibility than the system provides, I.E. for indeterminable argument count messages
	var formats = get_format()
	if len(formats) == 0:
		return false
	if not formats[0] is Array:
		formats = [formats]
	
	for format in formats:
		if __match_format(msg, format):
			return true
	return false

func get_format() -> Array:
	# Array of Args that form the parsestring
	# Can return a 2D array if more than one format can be applied
	return [ Literal.new("PING"), Anything.new() ]

func trigger(irc, args:Array):
	# The action to undertake when this parser is matched
	irc._sendraw("PONG "+args[1])
