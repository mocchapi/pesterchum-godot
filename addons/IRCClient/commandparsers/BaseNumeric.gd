extends _IRCcmdParser

export var print_on_fallback := false

var channel_list := []
var temp_channel_users := {}


const errors := {
	# 'number' ['ERR_IRCERRORNAME', 'lowercase plaintext human-readable version', disconnect now (bool)]
	'432': ['ERR_ERRONEUSNICKNAME', 'given nickname is invalid', true],
	'433': ['ERR_NICKNAMEINUSE', 'nickname is already in use', false],
	'421': ['ERR_UNKNOWNCOMMAND', 'unknown command', false]
}


func trigger_761(irc, args:Array): #RPL_KEYVALUE (from metadata CAP
	if irc.cap_requests_results.get('draft/metadata', false):
		irc.emit_signal("on_draft_metadata_received", args[2], args[4], args[6])


func trigger_001(irc, args:Array): #RPL_WELCOME
	irc._in_cap = false
	irc.emit_signal("on_connected")


func trigger_433(irc, args:Array): # ERR_NICKNAMEINUSE
	irc.emit_signal("on_disconnect", "Nickname is taken.")

func trigger_432(irc, args:Array): # ERR_ERRONEUSNICKNAME
	irc.emit_signal("on_error", 432, "Given nickname is invalid")

func trigger_375(irc, args:Array): #REPL_MOTDSTART
	irc.motd = args[-1]

func trigger_372(irc, args:Array): #REPL_MOTD
	irc.motd += args[-1]

func trigger_376(irc, args:Array): #REPL_ENDOFMOTD
	irc.motd += args[-1]
	irc.emit_signal("on_motd_updated", irc.motd)

func trigger_303(irc, args:Array): #RPL_ISON
	pass # ah fuck this is a pain to parse

func trigger_331(irc, args:Array): #REPL_NOTOPIC
	trigger_332(irc, args)

func trigger_332(irc, args:Array): #REPL_TOPIC
	irc.emit_signal("on_channel_topic_updated", args[-2], args[-1])


# list of channels
func trigger_321(irc, args:Array): #REPL_LISTSTART (?)
	channel_list = []

func trigger_322(irc, args:Array): #REPL_LISTITEM (?)
	channel_list.append([args[3], int(args[4]), args[5]])

func trigger_323(irc, args:Array): # REPL_LISTEND (?)
	irc.emit_signal("on_channel_list_updated",channel_list)
	channel_list = []


# list of users in x channel:
func trigger_353(irc, args:Array): #RPL_NAMREPLY
	temp_channel_users[args[4]] = temp_channel_users.get(args[4], []) + Array(args[-1].split(' ', false))

func trigger_366(irc, args:Array): #RPL_ENDOFNAMES
	var out = temp_channel_users.get(args[3], [])
	temp_channel_users.erase(args[3])
	irc.emit_signal("on_channel_user_list_updated", args[3], out)

func trigger_fallback(irc, args:Array):
	if print_on_fallback:
		printerr("Fallback for numerical ",args)






func matches(msg:Array) -> bool:
	if len(msg) < 2:
		return false
	return (__match_format(msg.slice(0,1), get_format()[0]) or __match_format(msg.slice(0,2), get_format()[1]))

func get_format() -> Array:
	# Array of Args that form the parsestring
	return [ [Origin.new(), Number.new() ], [Origin.new(), Number.new(), Location.new()]]

func trigger(irc, args:Array):
	# The action to undertake when this parser is matched
	args[0] = irc.strip_origin_colon(args[0])
	irc.emit_signal("on_numeric_reply", args[0], args[1], args[2], args.slice(3,-1))
	if has_method("trigger_"+args[1]):
		callv("trigger_"+args[1], [irc, args])
	else:
		trigger_fallback(irc, args)
		irc.emit_signal("on_unhandled_numeric_reply", args[0], args[1], args[2], args.slice(3,-1))
