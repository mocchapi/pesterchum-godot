extends _IRCcmdParser

func get_format() -> Array:
	# Array of Args that form the parsestring
	#                           METADATA               PERSON             KEY       VISIBILITY (*)        VALUE
	return [ Origin.new(), Literal.new("METADATA"), Location.new(), Anything.new(), Anything.new(), Anything.new() ]

func trigger(irc, args:Array):
	irc.emit_signal("on_draft_metadata_received", args[2], args[3], args[5])
	# The action to undertake when this parser is matched
	pass
