extends _IRCcmdParser

func get_format() -> Array:
	# Array of Args that form the parsestring
	return [ Origin.new(), Literal.new("NICK"), Anything.new()]

func trigger(irc, args:Array):
	# The action to undertake when this parser is matched
	if irc.is_me(args[0]):
		irc.nickname = args[-1]
		irc.emit_signal("on_nick_changed", args[-1])
	else:
		irc.emit_signal("on_user_nick_changed", args[0], args[-1])
