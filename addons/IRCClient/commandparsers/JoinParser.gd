extends _IRCcmdParser

func get_format() -> Array:
	return [ Origin.new(), Literal.new("JOIN"), Location.new(true, false) ]

func trigger(irc, args:Array):
	args[0] = irc.strip_origin_colon(args[0])
	if irc.is_me(args[0]):
		irc.emit_signal("on_join", args[-1])
	else:
		irc.emit_signal("on_user_join", args[0], args[-1])
