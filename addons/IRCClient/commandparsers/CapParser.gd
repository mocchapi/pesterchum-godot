extends _IRCcmdParser

var _l_cap = Literal.new("CAP")
var _l_ast = Anything.new()
var _orig = Origin.new()
var subcommands:Dictionary 

var tmp_server_caps :Array = []

func get_format() -> Array:
	subcommands = {
	'LS item': [ _orig, _l_cap,  _l_ast, Literal.new("LS"), Literal.new('*'), Anything.new()],
	'LS end': [ _orig, _l_cap,  _l_ast, Literal.new("LS"), Anything.new()],
	
	'LIST item': [ _orig, _l_cap,  _l_ast, Literal.new("LIST"), Literal.new('*'), Anything.new()], # CAP * LS * :cap1, cap2
	'LIST end': [ _orig, _l_cap,  _l_ast, Literal.new("LIST"), Anything.new()], # CAP * LS :cap1, cap2
	
	'ACK': [ _orig, _l_cap,  _l_ast, Literal.new("ACK"), Anything.new()],
	'NAK': [ _orig, _l_cap,  _l_ast, Literal.new("NAK"), Anything.new()],
	}
	return subcommands.values()

func trigger(irc, args:Array):
	var cmd:String = args[3]
	print("TRIGGER ",cmd)
	
	match cmd.to_upper():
		'ACK':
			irc._receive_cap_acks(args[4].split(' ', false))
		'NAK':
			irc._receive_cap_naks(args[4].split(' ', false))

		'LS','LIST':
			if args[4] == '*':
				tmp_server_caps += Array(args[5].split(' '))
			else:
				irc._receive_cap_list(tmp_server_caps + Array(args[4].split(' ')))
				tmp_server_caps = []
		_:
			printerr("irc: (CapParser) No parser for ",args)
