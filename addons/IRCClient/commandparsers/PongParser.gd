extends _IRCcmdParser

func get_format() -> Array:
	# Array of Args that form the parsestring
	return [ [ Origin.new(), Literal.new("PONG"), Anything.new() ], [ Origin.new(), Literal.new("PONG"), Anything.new(), Anything.new() ] ]

func trigger(irc, args:Array):
	# The action to undertake when this parser is matched
	irc.emit_signal("on_ping_answered", args[-1])
