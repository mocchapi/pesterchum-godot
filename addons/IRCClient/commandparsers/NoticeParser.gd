extends _IRCcmdParser

func get_format() -> Array:
	# Array of Args that form the parsestring
	# Can return a 2D array if more than one format can be applied
	return [ Origin.new(), Literal.new("NOTICE"), Location.new(), Anything.new() ]

func trigger(irc, args:Array):
	# The action to undertake when this parser is matched
	if irc.is_me(args[2]):
		args[2] = irc.split_client_identifier(args[0].substr(1))[0]
	irc.emit_signal("on_notice_received", args[2], args[0], args[3])
