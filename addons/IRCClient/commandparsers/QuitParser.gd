extends _IRCcmdParser

func get_format() -> Array:
	# Array of Args that form the parsestring
	return [ Origin.new(), Literal.new("QUIT"), Anything.new()]

func trigger(irc, args:Array):
	# The action to undertake when this parser is matched
	args[0] = irc.strip_origin_colon(args[0])
	if irc.is_me(args[0]):
		irc.emit_signal("on_quit", args[-1])
	else:
		irc.emit_signal("on_user_quit", args[0], args[-1])

