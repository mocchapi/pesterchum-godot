extends _IRCcmdParser

func get_format() -> Array:
	# Array of Args that form the parsestring
	return [[ Origin.new(), Literal.new("PART"), Location.new() ], [ Origin.new(), Literal.new("PART"), Location.new(), Anything.new() ]]

func trigger(irc, args:Array):
	# The action to undertake when this parser is matched
	args[0] = irc.strip_origin_colon(args[0])
	var reason := ""
	if len(args) > 3:
		reason = args[3]
	if irc.is_me(args[0]):
		printerr("on_part")
		irc.emit_signal("on_part", args[2], reason)
	else:
		printerr("on_user_part")
		irc.emit_signal("on_user_part", args[0], args[2], reason)
