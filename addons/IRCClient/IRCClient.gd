extends Node
class_name IRCClient, "res://addons/IRCClient/icon.png"

export var connect_on_ready := true
# Connect immediatly when the scene is loaded

export var host := "irc.pesterchum.xyz"
# URL / IP adress of irc server

export var port := 6697
# Server port to use. Default is 6697 for SSL/TLS and 6667 for TCP

export var nickname := "telecastTadpole"
# Nick to use

export var server_password := ""
# Server password to connect with. Leave empty if none is set
# Note that this is NOT nickServ or SASL, only the *server-wide* password

export var use_ssl := true
# Use SSL (TLS) ecnryption. Required on many servers. Disabling will result in plaintext messages sent over the network

export var validate_certificate := true
# use_ssl required. validates the server's SSL certificate

export var is_websocket_server:bool = false setget set_websocket_server
# Connects over websocket instead of TCP

export var realname := "*"
# Realname to register with. Leave empty to use nickname

export var ping_interval := 60
# Amount of time between sending PINGs
# Set to 0 to disable

export var capability_negotiation := false
# Enable IRCv3 CAP negotiation

export(Array, String) var cap_requests := []
# Capabilities to request from the server


export var parsers:Array = []
# irc command parser instances. must extend _IRCcmdParser

export var print_rx := false
# Prints all received data to console
export var print_tx := false
# Prints all sent data to console
export var print_on_parse_failure := true
# Prints if there is no parser for a received 

var motd := ""
var channels := {} # not currently tracked
var users := {} # not currently tracked


# Note that all signal names ending in "_updated" just means that the data they hold got re-queried to the server
# It does not indicate if the data actually changed. These are mostly used for some function returns, such as is_user_away()

signal rawreceived(MESSAGE)
# Emits on any received data
signal rawsent(MESSAGE)
# Emits every time this client sents something to the server
# rawsent + rawreceived combined offer everything goin in and out, helpful for debugging
signal fallback(MESSAGE)
# Emits if a suitable parser could not be located for the received data

signal on_message_received(location, sender, content)
# Emits when you receive a message in DMs or a channel
signal on_message_sent(location, content)
# Emits when the client (you) sends a message to DMs or a channel
signal on_notice_received(location, sender, notice)
# Emits when you receive a NOTICEx
signal on_notice_sent(location, content)
# Emits when the client (you) sends a notice to DMs or a channel
signal on_join(location)
# Emits when you join a channel
signal on_part(location, reason)
# Emits when you leave a channel
signal on_connecting()
# Emits when you are attempting to connect
# followed by either on_connect or on_disconnect
signal on_connected()
# Emits when you successfully authenticated with a server
signal on_disconnect(reason)
# Emits when the connection with the server is severed for any reason (QUIT, kicked, connection lost, invalid login, etc)
signal on_quit(reason)
# Emits when you manually disconnect from the server
signal on_nick_changed(new_nick)
# Emits when your nickname changed

signal on_user_join(user, location)
# Emits when another user joins a channel you are in
signal on_user_part(user, location, reason)
# Emits when another user leaves a channel you are in
signal on_user_quit(user, reason)
# Emits when another user disconnects from the server manually
signal on_user_nick_changed(user, new_nick)
# Emits when another user's nickname changes
#signal on_user_away_updated(user, online)
# Emits when the state of user onlineness is *re-checked*
signal on_user_channellist_updated(user, channel_list)
# Received when the list of channels a user is in is *re-checked*
signal on_channel_user_list_updated(channel, userlist)
# Received when the list of users in a channel is *re-checked*
signal on_channel_topic_updated(channel, topic)
# Received when a channel's topic is *re-checked*
signal on_channel_list_updated(channel_list)
# Emits when the list all visible channels on the server is *re-checked*

signal on_cap_negotiation_started()
signal on_cap_server_list_received(server_capabilities)
signal on_cap_negotiation_ended(capability_requests_results)

signal on_motd_updated(new_motd)
signal on_numeric_reply(origin, number, location, args)
signal on_unhandled_numeric_reply(origin, number, location, args)
signal on_ping_answered(ping_payload) # on PONG returnal, ping payload should be same as sent to verify
signal on_error(number, error_name, error_text)



signal on_draft_metadata_received(location, key, value)


var server_capabilities:Array = []
# CAP LS results
var cap_requests_results:Dictionary = {}
# Key: value, where key is the name of a capability listed in cap_requests
# and value a boolean showing if it was enabled
# IE { "draft/metadata": false, "sasl": true }


var _in_cap := false
var connection: EasyStream
var _time_since_ping := 0.0

#var is_connected:bool = false

# Connection functions:

func start():
	# Alias
	return start_connection()

func start_connection():
	print_debug("Connecting to ",host,":",port," as ",nickname)
	var ok = connection.connect_to_host(host, port, use_ssl, validate_certificate)
	print_debug("connection OK: ",ok)


func quit(quit_message:="Quit"):
	# polite disconnect with QUIT :)
	print_debug("polite manual disconnect: ",quit_message)
	_sendraw("QUIT :"+quit_message)
	connection.disconnect_from_host()
	emit_signal("on_quit", quit_message)


func close_connection():
	# rough disconnect without QUIT
	print_debug("impolite manual disconnect, consider using `quit()` instead.")
	connection.disconnect_from_host()
	emit_signal("on_quit", "Manual disconnect.")




# IRC functions

func send_message(location:String, contents:String):
	# Don't send an empty message, because the chum will not receive it
	if contents == "":
		printerr("(irc) Cannot send empty message")
		return false
	_sendraw("PRIVMSG "+location+" :"+contents)
	emit_signal("on_message_sent", location, contents)
	return true

func send_notice(location:String, contents:String):
	_sendraw("NOTICE "+location+" :"+contents)
	emit_signal("on_notice_sent", location, contents)
	return true

func join_channel(channel:String, password:=""):
	# channel must start with # or other valid channel characters such as &
	var out = "JOIN "+channel
	if password != "":
		out += " "+password
	_sendraw(out)

func leave_channel(channel:String):
	# channel must start with # or other valid channel characters such as &
	var out = "PART "+channel
	_sendraw(out)

func request_channel_topic(channel:String) -> void:
	if not channel.begins_with('#'):
		channel = '#'+channel
	_sendraw("TOPIC "+channel)

func request_channel_list()->void:
	_sendraw("LIST")

func request_channel_user_list(channel:String)->void:
	_sendraw("NAMES "+channel)

func is_me(origin:String) -> bool:
	return split_client_identifier(strip_origin_colon(origin))[0] == split_client_identifier(get_me())[0]

func get_me()->String:
	## TODO: replace with what welcome message says is my appointed nick/host/whatever
	return nickname

func set_nickname(new_nickname:String)->void:
	_sendraw("NICK "+new_nickname)









## useful helpers

static func split_client_identifier(identifier:String)->Array:
	# Extracts the nickname, username & host from a :clientidentifier
	# doing split_client_identifier(someid)[0] ensures you always get only the nickname
	identifier = strip_origin_colon(identifier)
	var left = [identifier]
	if '!' in left[-1]:
		# nick ! user
		left = left[-1].split('!')
	if '@' in left[-1]:
		# nick/user @ host
		left += Array(left).pop_back().split('@')
	return left

static func strip_origin_colon(origin:String)->String:
	# Just strips the : of an origin
	if origin.begins_with(':'):
		return origin.substr(1)
	return origin
	














### u probably dont need to pay attentin to this


func _ready():
	_load_parsers()
	if connection == null:
		set_websocket_server(is_websocket_server)

func _reinit():
	add_child(connection)
	connection.buffer_unicode_to = "\r\n"
	connection.enable_buffer = true
	connection.clear_buffer_on_connect = true
	
	connection.connect("new_buffer_item", self, "_on_stream_buffer_item")
	connection.connect("on_connecting", self, "_on_stream_connecting")
	connection.connect("on_connected", self, "_on_stream_connected")
	connection.connect("on_disconnected", self, "_on_stream_disconnected")
	connection.connect("connection_error", self, "_on_stream_connection_error", [false])
	connection.connect("handshake_error", self, "_on_stream_connection_error", [true])
	
	if connect_on_ready:
		start_connection()


func _process(delta):
	if connection == null:
		return
	if connection.is_connected_to_host():
		
		if ping_interval >= 1:
			_time_since_ping += delta
			if _time_since_ping >= ping_interval:
				_time_since_ping = 0.0
				_sendraw("PING :"+str(OS.get_unix_time()))


func _load_parsers(directory:String = "res://addons/IRCClient/commandparsers"):
	if not directory.ends_with('/'):
		directory += '/'
	print("COLLECTING PARSERS")
	var dir = Directory.new()
	var files := []
	if dir.open(directory) == OK:
		dir.list_dir_begin(true)
		var file_name = dir.get_next()
		while file_name != "":
			print("  checking "+file_name,'...')
			if not dir.current_is_dir() and (file_name.ends_with('.gd') or file_name.ends_with('.gdc')):
				if file_name[-1] == 'c':
					file_name = file_name.substr(0,len(file_name)-1)
				print("    OK! ",file_name)
				files.append(file_name)
			file_name = dir.get_next()
	
	print("LOADING PARSERS")
	for file_name in files:
		var parser_path = directory + file_name
		var loaded_parser = load(parser_path).new()
		if loaded_parser == null:
			print("Error loading ",file_name)
		else:
			parsers.append(loaded_parser)
	print("DONE LOADING PARSERS")



func _sendraw(text:String, add_endline:=true):
	if connection:
		if add_endline:
			text += "\r\n"
		if print_tx:
			print("irc TX: ",'"',text,'"')
		emit_signal("rawsent",text)
		connection.put_utf8_string(text)

func _login_handshake():
	_in_cap = false
	if capability_negotiation:
		_in_cap = true
		cap_requests_results.clear()
		server_capabilities.clear()
		_sendraw("CAP LS 302")
	if server_password != "":
		_sendraw("PASS "+server_password)
	set_nickname(nickname)
	var real = realname if realname != "" else nickname
	_sendraw("USER "+nickname+" 0 * "+ real)


func _receive_cap_list(caps:Array):
	if _in_cap:
		server_capabilities = caps
		emit_signal("on_cap_server_list_received", caps)
		if cap_requests.empty():
			_in_cap = false
			_sendraw("CAP END")
			emit_signal("on_cap_negotiation_ended", cap_requests_results)
		else:
			_sendraw("CAP REQ :"+' '.join(cap_requests))

func _receive_cap_acks(acks:Array):
	for item in acks:
		cap_requests_results[item] = true
	if len(cap_requests) == len(cap_requests_results):
		_in_cap = false
		_sendraw("CAP END")
		emit_signal("on_cap_negotiation_ended", cap_requests_results)

func _receive_cap_naks(naks:Array):
	for item in naks:
		cap_requests_results[item] = false
	if len(cap_requests) == len(cap_requests_results):
		_in_cap = false
		_sendraw("CAP END")
		emit_signal("on_cap_negotiation_ended", cap_requests_results)

func parse_message(text:String) -> bool:
	var msg := split_message(text)
	for parser in parsers:
		var format = parser.format
		if parser.matches(msg):
			parser.trigger(self, msg)
			return true
	if print_on_parse_failure:
		printerr("irc: No parser for ",msg)
	emit_signal("fallback", msg)
	return false


func split_message(text:String, strip_newline:=true) -> Array:
	# Splits 
	var out := []
	var buffer = ""
	
	for i in len(text):
		var chr = text[i]
		
		if chr == " ":
			out.append(buffer)
			buffer = ""
			continue
		

		if chr == ':' and i != 0:
			out.append(text.substr(i+1, len(text)))
			buffer = ""
			break

		buffer += chr
	buffer = buffer.strip_edges()
	if len(buffer) > 0:
		out.append(buffer)
	
	if strip_newline and out[-1].ends_with("\r\n"):
		out[-1] = out[-1].substr(0,len(out[-1])-2)
		if len(out[-1]) == 0:
			out.pop_back()
	return out





## EasyStream functions

func _on_stream_buffer_item(item:String):
	# Message received -> parsing is next
	if print_rx:
		print('irc RX: "',item,'"')
	emit_signal("rawreceived",item)
	parse_message(item)

func _on_stream_connected():
	# Stream connects
	_login_handshake()

func _on_stream_disconnected():
	# Stream disconnected, probably by the server 
	printerr("IRC: disconnected")
	print(connection.get_status())
	emit_signal("on_disconnect", "Connection ended.")

func _on_stream_connection_error(error:int, is_handshake_error:bool):
	# this is no good
	printerr("ERROR ", "[Handshake]" if is_handshake_error else "[Connect]", " ",error)
	pass

func _on_stream_connecting():
	emit_signal("on_connecting")


func set_websocket_server(new_state:bool):
	is_websocket_server = new_state
	if connection != null:
		remove_child(connection)
		connection.disconnect_from_host()
		connection.queue_free()
	if is_websocket_server:
		print("IRCClient will connect over websocket")
		connection = EasyStreamWebsock.new()
	else:
		print("IRCClient will connect over TCP/IP")
		connection = EasyStreamTCPSSL.new()
	_reinit()
