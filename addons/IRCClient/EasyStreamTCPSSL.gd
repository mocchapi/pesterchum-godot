extends EasyStream
class_name EasyStreamTCPSSL

# Abstracts TCP and SSL connections into one object

var _last_status :int= STATUS.DISCONNECTED
var stream:StreamPeer
const translation_table_ssl := [STATUS.DISCONNECTED, STATUS.CONNECTING, STATUS.CONNECTED, STATUS.ERROR, STATUS.ERROR]
const translation_table_tcp := [STATUS.DISCONNECTED, STATUS.CONNECTING, STATUS.CONNECTED, STATUS.ERROR]


func connect_to_host(host:String, port:int, use_ssl:=true, validate_cert=true, cert_hostname:="", blocking=true) -> bool:
	disconnect_from_host()
	_last_status = STATUS.DISCONNECTED
	
	emit_signal("on_connecting")
	
	printerr("Go connection to ",host)
	print_debug("connecting to ",host,":",port, (" with ") if use_ssl else (" without "), "SSL. Verifying cert is  ",validate_cert)
	print("args: ", port,"; ",use_ssl,'; ',validate_cert,'; ', cert_hostname, '; ', blocking)
	
	var new_stream = StreamPeerTCP.new()
	var err = new_stream.connect_to_host(host, port)
	if err != OK:
		printerr("EasyStream TCP error: ",err)
		emit_signal("on_error")
		emit_signal("connection_error", err)
		emit_signal("on_disconnected")
		return false
	stream = new_stream
	
	if use_ssl:
		var ssl_stream = StreamPeerSSL.new()
		ssl_stream.blocking_handshake = blocking
		if validate_cert:
			print_debug("Validate certificate & connct")
			if cert_hostname == "":
				cert_hostname = host
			err = ssl_stream.connect_to_stream(stream, validate_cert, cert_hostname)
		else:
			print_debug("Dont validate & connct")
			err = ssl_stream.connect_to_stream(stream)
		if err != OK:
			print_debug("An error occured: ",err)
			emit_signal("on_error")
			emit_signal("handshake_error", err)
			emit_signal("on_disconnected")
			return false
		stream = ssl_stream
	
	return true



func adopt_stream(new_stream):
	if not new_stream is StreamPeerTCP and not new_stream is StreamPeerSSL:
		printerr("Must be StreamPeerTCP or StreamPeerSSL")
		return
	disconnect_from_host()
	stream = new_stream


func _process(delta):
	var status = get_status()
	if status != _last_status:
		print_debug("Status changed to ",status, ": ", STATUS.keys()[status])
		emit_signal("status_changed", status)
		if status != STATUS.ERROR:
			emit_signal(translation_table_signal[status])
		if status == STATUS.CONNECTED and clear_buffer_on_connect:
			_buffer = ""
		_last_status = status

	match status:
		STATUS.CONNECTED:
			if enable_buffer:
				poll()
				var bytecount = stream.get_available_bytes()
				if bytecount > 0:
					var new_str = String(stream.get_utf8_string(bytecount))
					for chr in new_str:
						_buffer += chr
						if _buffer.ends_with(buffer_unicode_to):
							emit_signal("new_buffer_item", _buffer)
							_buffer = ""
		_:
			pass

func put_utf8_string(text:String) -> bool:
	if is_connected_to_host():
		stream.put_data(text.to_utf8())
		return true
	return false


func poll():
	if is_ssl_enabled() and is_connected_to_host():
		stream.poll()
	else:
		printerr("DIDNT POLL: ",is_ssl_enabled()," ", is_connected_to_host(), _last_status, " ",stream)

func disconnect_from_host():
	if is_connected_to_host(true):
		print_debug("DC from host")
		if is_ssl_enabled():
			return stream.disconnect_from_stream()
		return stream.disconnect_from_host()

func is_connected_to_host(include_connecting:=true) -> bool:
	var stat = get_status()
	if include_connecting:
		return stat == STATUS.CONNECTED or stat == STATUS.CONNECTING
	return stat == STATUS.CONNECTED

func get_status() -> int:
	if stream == null:
		return STATUS.DISCONNECTED
	return _translate_status(stream.get_status())

func is_ssl_enabled() -> bool:
	return stream is StreamPeerSSL




func get_stream() -> StreamPeer:
	return stream

func _translate_status(status:int) -> int:
	if stream == null:
		return STATUS.DISCONNECTED
	elif stream is StreamPeerSSL:
		return translation_table_ssl[status]
	elif stream is StreamPeerTCP:
		return translation_table_tcp[status]
	return STATUS.DISCONNECTED
