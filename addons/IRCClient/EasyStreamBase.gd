extends Node
class_name EasyStream

# Abstracts TCP, SSL, and WebSocket connections into a single interface

export var buffer_unicode_to := "\r\n"
export var enable_buffer := true
export var clear_buffer_on_connect := true


enum STATUS {
	DISCONNECTED,
	CONNECTING,
	CONNECTED,
	ERROR,
}

var _buffer := ""
const translation_table_signal := ["on_disconnected", "on_connecting", "on_connected", "on_error"]

signal new_buffer_item(item)
signal status_changed(new_status)
signal on_disconnected()
signal on_connecting()
signal on_connected()
signal on_error()

signal connection_error(error_enum) 
signal handshake_error(error_enum)


func connect_to_host(host:String, port:int, use_ssl:=true, validate_cert=true, cert_hostname:="", blocking=true) -> bool:
	return false

func adopt_stream(new_stream):
	pass

func poll():
	pass

func disconnect_from_host():
	pass
	
func put_utf8_string(text:String) -> bool:
	return false

func is_connected_to_host(include_connecting:=true) -> bool:
	return false

func get_status() -> int:
	return STATUS.DISCONNECTED


func get_stream():
	return null
