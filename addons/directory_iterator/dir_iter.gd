extends Reference
class_name EasyDirectory

var path:String
var list_files := true
var list_directories := true
var path_in_filename := true
var items := []

func _init(path:String, list_files:=list_files, list_directories:=list_directories, path_in_filename:=path_in_filename):
	path = path
	list_files = list_files
	list_directories = list_directories
	path_in_filename = path_in_filename
	items = walk_directory(path, list_files, list_directories, path_in_filename)

static func walk_directory(path, list_files:=true, list_directories:=true, path_in_filename:=true):
	var out := []
	if not path.ends_with('/'):
		path += '/'
	var dir = Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin(true)
		var file_name = dir.get_next()
		while file_name != "":
			if path_in_filename:
				file_name = path + file_name
			if dir.current_is_dir():
				if list_directories:
					out.append(file_name)
			elif list_files:
				out.append(file_name)
			file_name = dir.get_next()
	return out
