extends Button
class_name WidgetHideablebutton
export var hide_on_multiwindow := true
export var hide_on_singlewindow := false
export var hide_on_phones := false
export var hide_on_desktop := false

func _ready():
	add_to_group("widgets_hideablebutton")
	add_to_group("widgets")
	if hide_on_phones and Globals.running_on_phone():
		hide()
	if hide_on_desktop and !Globals.running_on_phone():
		hide()
	if hide_on_multiwindow and Config.c.settings.flags.display_mode == Globals.DISPLAYMODES.MULTIWINDOW:
		hide()
	if hide_on_singlewindow and Config.c.settings.flags.display_mode == Globals.DISPLAYMODES.SINGLEWINDOW:
		hide()
