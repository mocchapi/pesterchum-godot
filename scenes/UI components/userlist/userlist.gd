extends ItemList
class_name WidgetUserlist
var list:ConfigUserlist

export(Array, String) var context_actions = ["Pester", "Add chum", "Block", "Report"]
onready var menu = $menu

signal action_pressed(action_name)

var touch_duration:float = 0
var touch_down:bool = false
var touch_position = null

func _ready():
	add_to_group("widgets_userlist")
	add_to_group("widgets")
	connect("item_rmb_selected", self, "_on_item_rmb_selected")
	connect("item_activated", self, "_on_item_activated")
	connect("nothing_selected", self, "unselect_all")
	menu.connect("id_pressed", self, "_on_menu_id_pressed")
	if list != null:
		watch_list(list)
	else:
		yield(get_tree(),"idle_frame")
		if list == null:
			printerr("(WidgetUserList) _ready: frame 1, not watching a list")
	
	menu.add_separator("userName", 0)
	for action in context_actions:
		menu.add_item(action)


func _process(delta):
	if not touch_down:
		set_process(false)
	touch_duration += delta
	if touch_duration >= 0.75:
		touch_down = false
		var selected = get_selected_items()
		if len(selected) == 0:
			selected = null
		else:
			selected = selected[0]
		summon_popup(selected, touch_position, true)


func watch_list(new_list:ConfigUserlist):
	if new_list != null:
		new_list.include_values_in_str = true
		print("(WidgetUserlist) watch_list: ",new_list)
		new_list.include_values_in_str = false
		if list:
			list.disconnect("user_added", self, "_on_user_added")
			list.disconnect("user_removed", self, "_on_user_removed")
			list.disconnect("list_replaced", self, "_on_list_replaced")
		new_list.connect("user_added", self, "_on_user_added")
		new_list.connect("user_removed", self, "_on_user_removed")
		new_list.connect("list_replaced", self, "_on_list_replaced")
		list = new_list
		clear()
		for user in new_list:
			_on_user_added(user)
	else:
		printerr("(WidgetUserlist) watch_list: No such list: ",new_list)

func get_selected_user()->ConfigUser:
	var idx = get_selected_items()
#	print('(WidgetUserlist) get_selected_user: ',idx)
	if len(idx) > 0:
		var out = list.get_index(idx[0])
		return out
	return null


func find_index(username:String) -> int:
	for i in get_item_count():
		if get_item_text(i) == username:
			return i
	return -1 

func has_user(user:ConfigUser) -> bool:
	return find_index(user.chumhandle) != -1

func _on_user_added(user:ConfigUser):
	if has_user(user):
		printerr("(WidgetUserlist) _on_user_added: ","Already has user ",user)
		return
	print("(WidgetUserlist) _on_user_added: ","add user to UI list ",name,": ",user)
	add_item(user.chumhandle, 
			user.mood_image if user != Config.current_profile else Globals.icons['current_profile'])
	if Config.settings.interface.show_chum_color_in_lists:
		set_item_custom_fg_color(get_item_count()-1, user.color)
		if Config.settings.interface.contrast_chum_list_background and not user.color_contrasts_dark():
			set_item_custom_bg_color(get_item_count()-1, Color.lightgray)
	user.connect("mood_value_changed", self, "_on_mood_value_changed", [user])
	user.connect("chumhandle_value_changed", self, "_on_user_name_changed", [user])
	user.connect("color_value_changed", self, "_on_user_color_value_changed", [user])

	if user.mood == "offline" and user != Config.current_profile:
		user.request_mood()

func _on_list_replaced(old_list):
	for item in old_list:
		_on_user_removed(item[0])
	for item in list:
		_on_user_added(item)

func _on_user_removed(user:ConfigUser):
	var idx = find_index(user.chumhandle)
	if idx != -1:
		remove_item(idx)
		user.disconnect("chumhandle_value_changed",self,"_on_user_name_changed")
		user.disconnect("mood_value_changed", self, "_on_mood_value_changed")
		user.disconnect("color_value_changed", self, "_on_user_color_value_changed")


func _on_user_color_value_changed(new_color:Color, old_color, user:ConfigUser):
	if Config.settings.interface.show_chum_color_in_lists:
		var i = find_index(user.chumhandle)
		if i == -1:
			return
		set_item_custom_fg_color(i, user.color)
		if Config.settings.interface.contrast_chum_list_background and not user.color_contrasts_dark():
			set_item_custom_bg_color(i, Color.lightgray)


func _on_mood_value_changed(mood:String, prev_mood:String, user:ConfigUser):
	var i = find_index(user.chumhandle)
	if i != -1:
		set_item_icon(i, user.mood_image)

func _on_user_name_changed(newhandle:String, old_handle:String, user:ConfigUser):
	var i = find_index(old_handle)
	if i != -1:
		set_item_text(i, newhandle)

func _on_memo_user_added(user):
	_on_user_added(user.user)
	
func _on_memo_user_removed(user):
	_on_user_removed(user.user)


func _gui_input(event):
	if event is InputEventScreenTouch:
		touch_down = event.pressed
		if touch_down:
			touch_position = event.position + rect_global_position + Vector2(0, 30)
			touch_duration = 0
		set_process(touch_down)

func _on_item_activated(index:int):
	print("Activated!")
	if Globals.running_on_phone():
		summon_popup()
	else:
		get_selected_user().get_conversation().open()

func summon_popup(index=null, position=null, subtract_menu_size:=false):
	if index == null:
		if position == null:
			var selected = get_selected_items()
			if len(selected) > 0:
				index = selected[0]
			else:
				return
		else:
			index = get_item_at_position(position)
			select(index)

	menu.set_item_text(0, get_item_text(index))
	menu.set_as_minsize()

	if position == null:
		position = get_global_rect().get_center() - menu.rect_size/2
	elif subtract_menu_size:
		position -= menu.rect_size/2
	_ensure_menu_position(position)
	menu.set_as_minsize()
	menu.popup()

func _on_item_rmb_selected(index:int, click_position:Vector2):
	menu.set_item_text(0, get_item_text(index))
	menu.set_as_minsize()
	summon_popup(index, click_position + rect_global_position)

func _ensure_menu_position(newpos):
	var menu_rect = menu.get_rect()
	var global_rect = Rect2(Vector2.ZERO, get_tree().root.size)
	if not global_rect.encloses(menu_rect):
		newpos.x = min( menu_rect.end.x, global_rect.end.x) - menu_rect.size.x
		newpos.y = min( menu_rect.end.y, global_rect.end.y) - menu_rect.size.y
	menu.rect_global_position = newpos

func _on_menu_id_pressed(id:int):
	var action = menu.get_item_text(id)
	_on_action_pressed(action)
	emit_signal("action_pressed",action)

func _on_action_pressed(action:String):
	var user := get_selected_user()
	if user == null:
		printerr("(WidgetUserlist) _on_action_pressed: ","user == null!")
		return
	match action.to_lower():
		"pester":
			user.get_conversation().open()
		"add chum":
			if not Config.c.chumroll.contains_user(user):
				if Config.c.blocklist.contains_user(user):
					Config.c.blocklist.remove_user(user)
				Config.c.chumroll.add_user(user)
		"block":
			if not Config.c.blocklist.contains_user(user):
				if Config.c.chumroll.contains_user(user):
					Config.c.chumroll.remove_user(user)
				Config.c.blocklist.add_user(user)
		"unblock":
			if Config.c.blocklist.contains_user(user):
				Config.c.blocklist.remove_user(user)
			else:
				printerr("(WidgetUserlist) _on_action_pressed: cant unblock ",user,': not blocked! ')
				printerr("                 ^^^^^^^^^^^^^^^^^^: ", Config.c.blocklist.get_handles())
		"remove chum":
			if Config.c.chumroll.contains_user(user):
				Config.c.chumroll.remove_user(user)
		"report":
			Globals.main.notimplnotif()
		_:
			printerr("(WidgetUserlist) _on_action_pressed: ",'Unknown action: ',action)
