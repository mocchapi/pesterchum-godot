extends WidgetHideablebutton
class_name WidgetTogglechatbutton

func _ready():
	add_to_group("widgets_togglechatbutton")
	hint_tooltip = "Switch to chats/main screen"

func _pressed():
	Globals.main.toggle_chatwindow_open()
