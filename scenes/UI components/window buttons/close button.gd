extends WidgetHideablebutton
class_name WidgetClosewindowbutton


func _ready():
	add_to_group("widgets_closewindowbutton")
	hint_tooltip = "Close Pesterchum"

func _pressed():
	get_tree().notification(MainLoop.NOTIFICATION_WM_QUIT_REQUEST)
