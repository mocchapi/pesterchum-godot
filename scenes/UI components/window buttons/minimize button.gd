extends WidgetHideablebutton
class_name WidgetMinimizebutton

func _ready():
	add_to_group("widgets_minimizebutton")
	hint_tooltip = "Minimize Pesterchum"

func _pressed():
	OS.set_window_minimized(true)
