extends VBoxContainer

onready var chatlog = $"%chatlog"
onready var button_emotes = $"%button_emotes"
onready var button_send = $"%button_send"
onready var input_text = $"%send"

onready var audio_mention:AudioStreamPlayer = $audio_mention
onready var audio_focused:AudioStreamPlayer = $audio_focused
onready var audio_unfocused:AudioStreamPlayer = $audio_unfocused

var conversation:ConfigConversation setget set_conversation

func set_conversation(new:ConfigConversation):
	conversation = new
	conversation.connect("new_message", self, "_on_new_message")
	if chatlog:
#		print("(chatwindow.gd) set_conversation: ",new.get_log_as_bbcode(),' ~~ ',new.chat_log)
		chatlog.bbcode_text = new.get_log_as_bbcode()
		if len(chatlog.bbcode_text) > 2:
			pass
			# TODO: figure out why this is too fast
#			chatlog.append_bbcode('\n-- HISTORY END --\n\n')

func _on_new_message(sender:ConfigUser, text:String):
	# TODO: replace with regex
	if sender != Config.current_profile:
		var is_mention:bool = Config.current_profile.chumhandle.to_lower() in text.to_lower() or \
								" " + Config.current_profile.initials + " " in text
		if is_mention:
			if Config.settings.sound.mention_sounds:
				audio_mention.play()
		elif is_visible_in_tree():
			if Config.settings.sound.focused_conversation_sounds:
				audio_focused.play()
		else:
			if Config.settings.sound.unfocused_conversation_sounds:
				audio_unfocused.play()
	
	if not text.ends_with('\n'):
		text += '\n'
	chatlog.append_bbcode(text)

func _ready():
	add_to_group("widgets_chatwindow")
	add_to_group("widgets")
	input_text.connect("text_entered",self,'_on_input_text_entered')
	button_send.connect("pressed",self,'_on_button_send_pressed')
	set_conversation(conversation)
	connect("visibility_changed",self,"_on_vis_changed")

func _on_button_send_pressed():
	_on_input_text_entered(input_text.text)

func _on_input_text_entered(text:String):
	conversation.send(text)
	input_text.text = ""

func _on_button_emotes_pressed():
	var pop = Globals.main.popups.getpop("emoji")
	pop.popup()
	var result = yield(pop, "closed")
	if result[0]:
		input_text.text += ":"+result[1]+":"

func _on_vis_changed():
	pass
