extends RichTextLabel

func _ready():
	self.connect("meta_clicked", self, "_on_meta_clicked")

func _on_meta_clicked(meta):
	var split = meta.split(':')
	var identifiier = split[0]
	split.remove(0)
	var content = ':'.join(split)
	print("(chatlog.gd) _on_meta_clicked: ",identifiier,' with value ',content)
	match identifiier:
		'url':
			OS.shell_open(content)
		'memo':
			Globals.usercache.get_conversation(content.strip_edges()).join()
