extends "res://scenes/UI components/chatwindows/chatwindow.gd"


onready var userlist = $"%userlist"
onready var option_timeframes = $"%option_timeframes"

func _ready():
	add_to_group("widgets_memowindow")
	userlist.watch_list(conversation.participants)
	conversation.connect("timeframe_added", self, "_on_timeframe_added")
	conversation.connect("timeframe_selected", self, "_on_timeframe_selected")
	option_timeframes.clear()
	var meta = conversation.participants.get_user_metadata(Config.current_profile)
	if meta == null:
		printerr("_ready: meta is null!")
		printerr(Config.current_profile)
		return
	_on_timeframe_added(Config.current_profile, 0)
	
	if Config.settings.flags.display_mode == Globals.DISPLAYMODES.MULTIWINDOW:
		var controls = $"%controls"
		$VSplitContainer/top_bar.remove_child(controls)
		$VSplitContainer/top_bar.hide()
		$VSplitContainer/HSplitContainer/side_bar.add_child(controls)
		$VSplitContainer/HSplitContainer/side_bar.show()

func _on_timeframe_added(user:ConfigUser, timeframe:int):
	if user.chumhandle == Config.current_profile.chumhandle:
		option_timeframes.clear()
		var meta = conversation.participants.get_user_metadata(Config.current_profile)
		if meta == null:
			printerr("_on_option_timeframes_item_selected: meta is null!")
			printerr(timeframe, ' :: ', user)
			return
		var keys = meta['timeframes'].keys()

		keys.sort()
		keys.invert()
		for item in keys:
			var reltime:String
			if item != 0:
				reltime = Globals.parsetools.format_full_reltime(Globals.parsetools.timestamp_to_H_M(item))
				if item < 0:
					reltime += " AGO"
				else:
					reltime += " FROM NOW"
			else:
				reltime = "RIGHT NOW"
			option_timeframes.add_item(reltime, item)

func _on_timeframe_selected(user:ConfigUser, timeframe:int):
	if user.chumhandle == Config.current_profile.chumhandle:
		var idx = option_timeframes.get_item_index(timeframe)
		if idx >= 0:
			option_timeframes.selected = idx

func _on_btn_new_timeframe_pressed():
	var pop = Globals.popups.getpop("add_timeframe_popup")
	pop.popup([conversation.get_location()])
	var out = yield(pop, "closed")
	
	if out[0]:
		print("(memowindow.gd) _on_btn_new_timeframe_pressed:"," Add timeframe ",out[1])
		conversation.set_own_timeframe(out[1])


func _on_option_timeframes_item_selected(index):
	var timeframe:int = option_timeframes.get_item_id(index)
	var meta = conversation.participants.get_user_metadata(Config.current_profile)
	if meta == null:
		printerr("_on_option_timeframes_item_selected: meta is null!")
		printerr(timeframe, ' :: ', Config.current_profile)
		return
	if meta['current_timeframe'] != timeframe:
		conversation.set_own_timeframe(timeframe)
