extends "res://scenes/UI components/chatwindow/chatwindow.gd"

onready var userlist := $"%userlist"
export var channel_name := ""

func _ready():
	add_to_group("widgets_memowindow")
	userlist.watch_list(Globals.usercache.get_memo(channel_name))

func _on_time_slider_value_changed(value):
	var h_m = ConfigMemo.unix_to_hour_minute(value)
	$"%label_time".text = "Relative time: " + str(h_m['hour']*h_m['pos']).pad_zeros(2) + ":" + str(h_m['minute']).pad_zeros(2)

func _on_button_set_time_pressed():
	var value = $"%time_slider".value
	Globals.pesterlayer.dispatch_timeframe(channel_name, value)


func _on_button_reset_time_pressed():
	$"%time_slider".value = 0
