extends "res://scenes/UI components/chatwindows/chatwindow.gd"

onready var botnotice = $"%botnotice"


func _ready():
	add_to_group("widgets_privmsgwindow")
	botnotice.visible = conversation.participant.is_robot
	if Config.settings.sound.new_privmsg_sounds:
		$audio_begin.play()
