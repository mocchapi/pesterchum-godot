extends VBoxContainer

onready var bar := $"%send"
onready var chatlog = $"%chatlog"

func _ready():
	add_to_group("widgets_chatwindow")

func _on_button_emotes_pressed():
	var pop = Globals.main.popups.getpop("emoji")
	pop.popup()
	var result = yield(pop, "closed")
	if result[0]:
		bar.text += ":"+result[1]+":"


func _on_button_send_pressed():
	bar.emit_signal("text_entered", bar.text)
