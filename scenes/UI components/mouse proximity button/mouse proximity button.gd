extends WidgetHideablebutton
class_name WidgetMouseproximitybutton

export var color_fallback_on_phones :Color = Color.white

export var modulate_gradient :Gradient= Gradient.new()
export(float) var far_distance :float= 150.0
export(float) var near_distance :float= 25.0

func _ready():
	add_to_group("widgets_mouseproximitybutton")
	add_to_group("widgets")

func _input(event):
	if visible and event is InputEventMouseMotion:
		_update_prox(event.position)

func _update_prox(position:Vector2):

	if Globals.running_on_phone(true):
		modulate = color_fallback_on_phones
		return
	var distance = clamp(position.distance_to(rect_global_position), near_distance, far_distance) - near_distance
	var gradient_position = 1 - distance/(far_distance-near_distance)
	modulate = modulate_gradient.interpolate(gradient_position)
