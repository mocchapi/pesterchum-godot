extends Control

signal pressed()
signal closed()

onready var _text := $"%text"
onready var _close := $"%close"
#onready var _suffix := get_node_or_null("tab/tab/tab/tab/suffix")
#onready var _prefix := get_node_or_null("tab/tab/tab/tab/prefix")

export var icon :Texture setget set_icon
export var text := "" setget set_text
var show_close_button := true setget set_show_close_button
export var color := Color.white setget set_color

func _ready():
	set_text(text)
	set_show_close_button(show_close_button)
	set_icon(icon)
	set_color(color)

func set_icon(newicon:Texture):
	$"%icon".texture = newicon
	icon = newicon

func set_color(newcolor:Color):
	color = newcolor
	$"%text".add_color_override("default_color", color)

func _gui_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and not event.pressed:
		emit_signal("pressed")

func set_text(new_text:String):
	if _text != null:
		_text.bbcode_text = '[center]' + new_text + '[/center]'
	text = new_text

func set_show_close_button(value:bool):
	show_close_button = value
	if _close != null:
		_close.visible = value


func _on_close_pressed():
	emit_signal("closed")
