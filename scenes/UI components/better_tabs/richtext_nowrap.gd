extends RichTextLabel
class_name RichTextLabelNowrap

export var wrapping := false setget set_wrapping
onready var font:Font = get_font("normal_font")
export var extra_characters_counted := 3
export var real_min_size:=Vector2.ZERO

var prev := 0

func _process(delta):
	var l = len(text)+ceil(extra_characters_counted)
	if l != prev:
		_recalc()
		prev = l

func set_wrapping(state:bool):
	wrapping = state
	_recalc()

func _recalc():
	if wrapping:
		rect_min_size = real_min_size
	else:
		rect_min_size = font.get_string_size(text+"_".repeat(extra_characters_counted))
