extends VBoxContainer
class_name BetterTabs


# Like normal tab controls, but better


var idx := 0

signal tab_pressed(tab_name)
signal tab_focused(tab_name)
signal tab_added(tab_name)
signal tab_closed(tab_name)

export var tabs_override : NodePath
export var content_override : NodePath
export var tab_scene : PackedScene = preload("res://scenes/UI components/better_tabs/tab.tscn")
export var label_override : NodePath

var tabs	# Container for the clickable buttons that corrospond to a tab page 
var content # Container where the tab pages are children to
var label	# Any node that has a `.text` property. Shows the current tab page's name
onready var dummy := $content/dummy # A background thats shown when there are no tabs

export(String) var focused
export var label_format := ":: {0} ::"
export var default_show_close_button := true # What newly added tabs will have their close button's visibility set to
export var tabs_visible := true setget set_tabs_visible,get_tabs_visible
var lookup := {} # Keeps track of all the tabs. maps name to node

func set_tabs_visible(newval:bool):
	if tabs:
		tabs.visible = newval
	else:
		$tabs.visible = newval

func get_tabs_visible() -> bool:
	if tabs:
		return tabs.visible
	else:
		return $tabs.visible


func _ready():
	if tabs_override:
		$tabs.hide()
		tabs = get_node(tabs_override)
	else:
		tabs = $tabs/tabs/tabs
	if content_override:
		$content.hide()
		content = get_node(content_override)
	else:
		content = $content/content
	
	if label_override:
		$label/Label.hide()
		label = get_node(label_override)
	else:
		label = $label/Label
	
	_re_index()

func has_tab(tab_name):
	return lookup.has(tab_name)

func _re_index():
	# Purges all tabs & then make new ones for all tab pages
	print("(BetterTabs) _re_index:")
	lookup = {}
	_hide_all()
	for item in tabs.get_children():
		tabs.remove_child(item)
		item.queue_free()
	for item in content.get_children():
		add_tab(item)

func _hide_all():
	for item in lookup.values():
		item[1].hide()

func _make_tab(object:CanvasItem, show_close_button:bool, tab_name:String):
	var newtab = tab_scene.instance()
	newtab.text = tab_name
	newtab.show_close_button = show_close_button
	newtab.connect("closed", self, "close_tab", [tab_name])
	newtab.connect("pressed", self, "focus_tab", [tab_name])
	newtab.connect("pressed", self, "_scrollto_tab", [newtab])
	return [tab_name, object, newtab, idx]

func _scrollto_tab(tab_node):
	if tabs.has_method("ensure_control_visible"):
		tabs.ensure_control_visible(tab_node)

func add_tab(object:CanvasItem, show_close_button=null, tab_name := ""):
	# Creates a Tab & Tab page for the given `object` node
	if tab_name == "":
		tab_name = object.name
	if lookup.has(tab_name) and lookup[tab_name][1] != object:
		printerr("(BetterTabs) add_tab: Tabname ",tab_name," is already in use by object ",lookup.get(tab_name)[1])
		return
	if show_close_button == null:
		show_close_button = default_show_close_button
	# Reparent tab to be a child of this.
	# TODO?: maybe only do this when a child has no parent
	var object_parent = object.get_parent()
	if object_parent and object_parent != content:
		printerr("(BetterTabs) add_tab: While creating new tab ",tab_name,"; object ",object," already has a parent (",object.get_path(),')')
		printerr("             ^^^^^^^: moving to my path: ",get_path())
		object.get_parent().remove_child(object) 
	if object_parent != content:
		content.add_child(object)
		
	var out = _make_tab(object, show_close_button, tab_name)
	lookup[tab_name] = out
	object.hide()
	tabs.add_child(out[2])
	idx += 1
	emit_signal("tab_added", tab_name)
	if focused == "":
		focus_tab(tab_name)
	_check_dummy()
	print("(BetterTabs) add_tab: added tab ",out[2]," (" +tab_name+ ") to ",tabs.get_path())
	print("(BetterTabs) ^^^^^^^: Tabs has these kids: ",tabs.get_children())

	return out

func close_tab(tab_name:String):
	print("(BetterTabs) close_tab: ",tab_name)
	if lookup.has(tab_name):
		
		var tab = lookup[tab_name]
		if tab[1].get_parent() == content:
			content.remove_child(tab[1])
		if tab[2].get_parent() == tabs:
			tabs.remove_child(tab[2])
		tab[1].queue_free()
		tab[2].queue_free()
		emit_signal("tab_closed", tab_name)
		lookup.erase(tab_name)
		if tab_name == focused:
			if len(lookup) >= 1:
				focus_tab(lookup.keys()[0], true)
			else:
				focus_tab("", true)
				set_label("")
	else:
		printerr("(BetterTabs) close_tab: Can't close tab ",tab_name," because it doesn't exist")
	_check_dummy()

func _check_dummy():
	if content_override:
		dummy.hide()
	else:
		dummy.visible = focused == "" or len(lookup) == 0
		content.visible = not dummy.visible

func focus_tab(tab_name:String, no_signals:bool=false):
	if tab_name == "" or !lookup.has(tab_name):
		return
	if lookup.has(focused):
		lookup[focused][1].hide()
	lookup[tab_name][1].show()
	
	if not no_signals:
		emit_signal("tab_pressed",tab_name)
		if focused != tab_name:
			emit_signal("tab_focused", tab_name)
	
	focused = tab_name
	if label != null:
		set_label(label_format.format([tab_name]))

func set_tab_icon(tab_name:String, texture:Texture):
	var t = get_tab(tab_name)
	if t != null:
		t.icon = texture

func set_label(label_text:String):
	label.bbcode_text = "[center]"+label_text+"[/center]"

func get_tab(tab_name:String):
	return lookup[tab_name][2] if lookup.has(tab_name) else null

func get_content(tab_name:String):
	return lookup[tab_name][1] if lookup.has(tab_name) else null

func set_tab_color(tab_name:String, color:Color):
	var t = get_tab(tab_name)
	if t != null:
		t.color = color
