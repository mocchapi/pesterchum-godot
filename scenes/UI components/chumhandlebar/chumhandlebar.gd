extends HBoxContainer
class_name WidgetChumhandlebar
tool
# Sets & reads the chumhandle for the current profile
# Also allows setting color & showing current mood

export var handle_editable := true setget set_handle_editable
export var color_editable := true setget set_color_editable
export var show_mood := true setget set_show_mood
export var show_color := true setget set_show_color

onready var text_chumhandle := $"%text_chumhandle"
onready var icon_mymood := $"%icon_mymood"
onready var clr_color := $"%clr_color"


func _on_clr_popup_vis_changed():
	if Globals.running_on_phone() or true:
		clr_color.get_popup().rect_scale = Vector2(0.8, 1)

func _ready():
	add_to_group("widgets_chumhandlebar")
	add_to_group("widgets")
	var c_profile :ConfigProfile = Config.c.current_profile
	text_chumhandle.text = c_profile.chumhandle
	clr_color.color = c_profile.color


	icon_mymood.icon = c_profile.get_mood_image()
	icon_mymood.get_popup().connect("index_pressed", self, "_on_mood_index_pressed")
	for moodname in Globals.pesterlayer.moods:
		icon_mymood.get_popup().add_icon_item(Globals.pesterlayer.moods[moodname]['image'], moodname.capitalize())
	
	c_profile.connect("chumhandle_value_changed", self, "_on_profile_chumhandle_changed")
	c_profile.connect("mood_value_changed", self, "_on_profile_mood_changed")
	c_profile.connect("color_value_changed", self, "_on_profile_color_changed")

func _on_profile_color_changed(new_color, old_color):
	clr_color.color = new_color

func _on_profile_chumhandle_changed(new_handle, old_handle):
	text_chumhandle.text = new_handle

func _on_profile_mood_changed(new_mood, old_mood):
	icon_mymood.icon = Config.current_profile.get_mood_image()


func _on_clr_color_color_changed(new_color):
	$debounce.start()

func _on_text_chumhandle_text_entered(new_handle:String):
	if Globals.pesterlayer.is_valid_chumhandle(new_handle):
		Globals.irc.set_nickname(new_handle)
		Config.current_profile._set('chumhandle',new_handle)
	else:
		text_chumhandle.text = Config.current_profile.chumhandle
		Globals.main.simplenotif("Your handle must be at least 2 characters long and contain exactly 1 capital, which may not be the first character.","Invalid chumhandle!")

func set_handle_editable(state:bool):
	handle_editable = state
	$"%text_chumhandle".editable = state
	
func set_color_editable(state:bool):
	color_editable = state
	$"%clr_color".disabled = !state

func set_show_mood(state:bool):
	show_mood = state
	$"%icon_mymood".visible = state

func set_show_color(state:bool):
	show_color = state
	$PanelContainer.visible = state


func _on_debounce_timeout():
	Globals.pesterlayer.set_color(clr_color.color)


func _on_mood_index_pressed(index:int):
	var mood = icon_mymood.get_popup().get_item_text(index).to_lower()
	Globals.pesterlayer.set_mood(mood)
