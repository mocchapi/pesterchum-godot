extends Button
class_name WidgetMoodbutton
tool

export var add_icon := true
# Will add the icon when launched. Its not shown in editor because the icons arent loaded
export var set_text := true
export(String, "chummy", "rancorous", "offline", "pleasant", "distraught", "pranky", "smooth", "ecstatic",
			"relaxed", "discontent", "devious", "sleek", "detestful", "mirthful", "manipulative",
			"vigorous", "perky", "acceptant", "protective", "mystified", "amazed", "insolent", "bemused"
				) var mood := "chummy" setget set_mood


func _ready():
	add_to_group("widgets_moodbutton")
	add_to_group("widgets")
	set_mood(mood)

func set_mood(newmood:String):
	mood = newmood
	if add_icon and Globals.get("pesterlayer") and Globals.pesterlayer.moods.has(mood):
		icon = Globals.pesterlayer.moods[mood]['image']
	if set_text:
		text = mood.to_upper()

func _pressed():
	if Globals.pesterlayer:
		Globals.pesterlayer.set_mood(mood)
