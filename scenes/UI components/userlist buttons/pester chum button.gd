extends "res://scenes/UI components/userlist buttons/baseuserlistbutton.gd"
class_name WidgetPesterchumbutton

func _ready():
	deselect_after_action = true
	allow_empty_selection = true
	add_to_group("widgets_pesterchumbutton")
	disable_if_none_selected = true

func do_action(handles:=[]):
	if len(handles) == 0:
		
		return
	for handle in handles:
		var convo = Globals.usercache.get_conversation(handle)
		convo.open()
		convo.request_focus()
