extends Button

export var linked_userlist:NodePath 
var deselect_after_action := true
var allow_empty_selection := false
var disable_if_none_selected := false

var userlist_only := false

func _ready():
	add_to_group("widgets_userlistbutton")
	add_to_group("widgets")
	if get_list() != null:
		get_list().connect("nothing_selected", self, "_list_not_selected")
		get_list().connect("item_selected", self, "_list_has_selected")
	call_deferred("_list_not_selected")

func _list_has_selected(whatever=null):
	if disable_if_none_selected:
		self.disabled = false

func _list_not_selected():
	if disable_if_none_selected:
		self.disabled = true

func get_list():
	var out = get_node_or_null(linked_userlist)
	if out == null:
		printerr("(baseuserlistbutton.gd) get_list: ","linked userlist ",linked_userlist," is null")
	if userlist_only:
		return out as WidgetUserlist
	return out

func _pressed():
	var list = get_list()
	if list == null:
		_pre_action([])
		return
	if list is WidgetUserlist:
		var out = list.get_selected_user()
		if out:
			out = [out.chumhandle]
		else:
			out = []
		_pre_action(out)
	else:
		var out = []
		for item in list.get_selected_items():
			out.append(list.get_item_text(item))
		_pre_action(out)

func _pre_action(handles:=[]):
	if !handles.empty():
		do_action(handles)
		_post_action()
	elif allow_empty_selection:
		do_action(handles)
		_post_action()

func _post_action():
	if deselect_after_action:
		var list = get_list()
		if list != null:
			list.unselect_all()

func do_action(handles:=[]):
	pass
