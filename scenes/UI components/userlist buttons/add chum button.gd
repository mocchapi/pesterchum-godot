extends "res://scenes/UI components/userlist buttons/baseuserlistbutton.gd"
class_name WidgetAddchumbutton

func _ready():
	allow_empty_selection = true
	
	add_to_group("widgets_addchumbutton")

func do_action(handles:=[]):
	var list = get_list()
	if list != null:
		list.unselect_all()
	var handle = ""
	if !handles.empty():
		handle = handles[-1]
		if list is WidgetUserlist and Config.chumroll.contains_user(Globals.usercache.get_user(handle)):
				return
	Globals.popups.getpop("addchum_popup").popup([handle])
