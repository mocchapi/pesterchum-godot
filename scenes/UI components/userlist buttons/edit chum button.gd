extends "res://scenes/UI components/userlist buttons/baseuserlistbutton.gd"
class_name WidgetEditchumbutton


func _ready():
	userlist_only = true
	deselect_after_action = false
	disable_if_none_selected = true
	focus_mode = Control.FOCUS_NONE
	add_to_group("widgets_editchumbutton")


func do_action(handles:=[]):
	get_list().summon_popup()
