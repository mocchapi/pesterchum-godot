extends "res://scenes/UI components/userlist buttons/baseuserlistbutton.gd"
class_name WidgetUnblockchumbutton


func _ready():
	deselect_after_action = true
	allow_empty_selection = false
	add_to_group("widgets_unblockchumbutton")
	disable_if_none_selected = true

func do_action(handles:=[]):
	get_list()._on_action_pressed("unblock")
