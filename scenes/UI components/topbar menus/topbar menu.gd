extends MenuButton
class_name WidgetTopbarMenu
tool

export(String, "client", "profile", "help") var target_menu  = "client" setget set_target_menu
export var override_title := true
export var override_theme_stuff := true
export var lowercase_names := false

signal menu_picked(menu, name)
signal menu_picked_id(menu, idx)
signal menu_picked_index(menu, idx)

const menus = {
	client = ["OPTIONS","MEMOS", "PESTERLOG","RANDOM ENCOUNTER", "USERLIST", "BLOCKLIST", "IDLE","RELOAD"],
	profile = ["PREFERENCES","QUIRKS","MANAGE"],
	help = ["HELP","RULES","CALSPRITE", "CHANSERV", "NICKSERV", "ABOUT"],
}

func set_target_menu(idx:String):
	if menus.has(idx):
		target_menu = idx
		
		if override_title:
			text = idx.capitalize() if lowercase_names else idx.to_upper()
		
		var pop = get_popup()
		pop.clear()
		for item in menus[idx]:
			pop.add_item(item if not lowercase_names else item.capitalize())

func _ready():
	switch_on_hover = true
	set_target_menu(target_menu)
	if override_theme_stuff:
		mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
		theme_type_variation = "ButtonSmall"
		get_popup().connect("index_pressed", self, "on_index_pressed")
	add_to_group("widgets_topbarmenu")
	add_to_group("widgets")


func on_index_pressed(idx:int):
	var menuname = menus[target_menu][idx]
	emit_signal("menu_picked", target_menu, menuname)
	emit_signal("menu_picked_id", target_menu, get_popup().get_item_id(idx))
	emit_signal("menu_picked_index", target_menu, idx)
	Globals.menus.handle_or_show(target_menu, menuname)


