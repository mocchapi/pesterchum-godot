extends BetterTabs

# chat tabs. again. for the third time. this time using the ConfigConversation system
# (which shifts a bunch of logic onto the individual windows)

const privw := preload("res://scenes/UI components/chatwindows/privmsgwindow/privmsgwindow.tscn")
const memow := preload("res://scenes/UI components/chatwindows/memowindow/memowindow.tscn")

func _ready():
	add_to_group("widgets_chattabs")
	add_to_group("widgets")
#	connect("tab_focused", self, "_on_tab_focus")
#	connect("tab_added", self, "_on_tab_added")
	connect("tab_closed", self, "_on_tab_closed")

	Globals.usercache.connect("conversation_opened",self,"_on_conversation_opened")
	Globals.usercache.connect("conversation_closed",self,"_on_conversation_closed")
	Globals.pesterlayer.connect("focus_conversation",self,"_on_focus_requested")
	
	print("(chattabs_3) _ready: ","ingesting existing conversations...")
	for conversation in Globals.usercache.get_open_conversations():
		ensure_tab(conversation)
		print("(chattabs_3) _ready: ","^^^ added ",conversation)

func ensure_tab(conversation:ConfigConversation)->bool:
	if has_tab(conversation.get_location()):
		print("(chattabs_3) ensure_tab: ",conversation,' already exists')
		return false
	
	var new
	if conversation is PrivmsgConversation:
		new = privw.instance()
	elif conversation is MemoConversation:
		new = memow.instance()
	if new != null:
		new.conversation = conversation
		new.name = conversation.get_location()
		print("(chattabs_3) ensure_tab: ",conversation,' tab created as ',new)
		var tab = add_tab(new, true, conversation.get_location())
		if conversation is PrivmsgConversation:
			conversation.participant.connect("color_value_changed+", tab[2], "set_color")
			tab[2].set_color(conversation.participant.color)
			conversation.participant.connect("mood_value_changed+", self,  "_on_user_mood_changed", [conversation.participant, tab[2]])
			_on_user_mood_changed(conversation.participant.mood, conversation.participant, tab[2])
	return true

func _on_user_mood_changed(mood, user, tab):
	print("_on_user_mood_changed", mood, user, tab)
	if is_instance_valid(tab):
		tab.set_icon(user.get_mood_image())

func _on_tab_closed(tab_name:String):
	print("(chattab_3.gd) _on_tab_closed: Closing ",tab_name,' if it is still open')
	var convo = Globals.usercache.get_conversation(tab_name)
	if convo is MemoConversation and convo.is_participating:
		print("               ^^^^^^^^^^^^^^  Was participating. bye")
		convo.leave(true)
	elif convo.is_open and not convo.lock_open_state:
		print("               ^^^^^^^^^^^^^^  Was open. not anymore")
		convo.close()

func _on_focus_requested(conversation:ConfigConversation):
	if has_tab(conversation.get_location()):
		Globals.main.set_chatwindow_open(true)
		focus_tab(conversation.get_location())

func _on_conversation_closed(conversation:ConfigConversation):
	if has_tab(conversation.get_location()):
		close_tab(conversation.get_location())

func _on_conversation_opened(conversation:ConfigConversation):
	ensure_tab(conversation)
