extends ColorPickerButton
class_name WidgetScaledcolorpickerbutton

export var scale_to:Vector2 = Vector2(0.8, 1.0)
export var scale_on_singlewindow:bool = true
export var scale_on_multiwindow:bool = false

func _ready():
	add_to_group("widgets_scaledcolorpickerbutton")
	add_to_group("widgets")
	get_popup().connect("visibility_changed", self, "_on_clr_popup_vis_changed")

func _on_clr_popup_vis_changed():
	var DM = Config.c.settings.flags.display_mode
	if (DM == Globals.DISPLAYMODES.SINGLEWINDOW and scale_on_singlewindow) or (DM == Globals.DISPLAYMODES.MULTIWINDOW and scale_on_multiwindow):
		get_popup().rect_scale = scale_to
