extends PanelContainer

onready var spin_hours = $"%spin_hours"
onready var spin_minutes = $"%spin_minutes"
onready var slider_times = $"%slider_times"
onready var btn_set = $"%btn_set"

var listen_to_signals = true

func on_popup(extra_args):
	$"%lbl_memo_name".text = extra_args[0]
	spin_hours.value = 0
	spin_minutes.value = 0
	btn_set.disabled = true

func _on_btn_set_pressed():
	var out = spin_hours.value*60 + spin_minutes.value
	if $"%option_when".selected == 0:
		out *= -1
	$"../..".accept([out])


func _on_btn_cancel_pressed():
	$"../..".dismiss()


func _on_slider_times_value_changed(value):
	if listen_to_signals:
		listen_to_signals = false
		spin_hours.value = floor(value/60)
		spin_minutes.value = value - floor(value/60)*60
		check_button()
		listen_to_signals = true

func check_button():
	btn_set.disabled = (spin_hours.value + spin_minutes.value) == 0

func _on_spin_value_changed(value):
	if listen_to_signals:
		listen_to_signals = false
		slider_times.value = (spin_hours.value * 60 + spin_minutes.value)
		check_button()
		listen_to_signals = true
