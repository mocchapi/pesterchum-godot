extends PanelContainer
onready var reason = $PanelContainer/reason


func on_popup(args:Array):
	if len(args) > 0:
		reason.text = ''.join(args)
	else:
		reason.text = "Unknown cause"

func _on_loginButton_pressed():
	$"../..".dismiss()
	Globals.main.popups.getpop("login_popup").popup()


func _on_reconnectButton_pressed():
	$"../..".accept()
	Globals.start_connection()
