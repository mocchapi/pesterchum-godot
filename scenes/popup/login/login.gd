extends PanelContainer

onready var p = $"../.."

func _ready():
	_on_PanelContainer_visibility_changed()

func _on_connectbutton_pressed():
	p.accept()
	Globals.start_connection()


func _on_text_chumhandle_text_changed(new_text):
	var valid_handle = Globals.parsetools.is_valid_chumhandle(new_text)
	$VBoxContainer/connectbutton.disabled = not valid_handle
	$VBoxContainer/handle_rules.visible = not valid_handle


func _on_btn_edit_profile_pressed():
	Globals.menus.handle_or_show("profile",'manage')
	p.dismiss()


func _on_btn_edit_connection_pressed():
	Globals.menus.handle_or_show("client",'options')
	p.dismiss()



func _on_PanelContainer_visibility_changed():
	$"%text_chumhandle".text = Config.current_profile.chumhandle
	$"%text_server".text = Config.settings.connection.hostname
	_on_text_chumhandle_text_changed(Config.current_profile.chumhandle)
