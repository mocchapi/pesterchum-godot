extends PanelContainer

onready var p = $"../.."

onready var list := $"%ItemList"

var presets := []

func _ready():
	re_load()
	re_make_list()

func re_make_list():
	list.clear()
	for item in presets:
		list.add_item('{0}: "{1}"'.format([item.name, item.apply("Foo bar.")]), preload("res://assets/texture/icons/troll.png"))

func re_load():
	presets = []
	var items = EasyDirectory.walk_directory("res://assets/resources/quirk_presets/homestuck/", true, false)
	items = items + EasyDirectory.walk_directory("user://custom/quirk_presets", true, false)
	for preset in items:
		var loaded = ResourceLoader.load(preset)
		presets.append(loaded)


func _on_button_cancel_pressed():
	p.dismiss()


func _on_button_import_pressed():
	var indexes = list.get_selected_items()
	if len(indexes) == 0:
		return
	var first_index = indexes[0]
	var item = presets[first_index]
	p.accept([item])
