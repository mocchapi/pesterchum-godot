extends PanelContainer

onready var richtext := $VBoxContainer/richtext
onready var p = $"../../../.."
var prev := Vector2.ZERO

func _ready():
	call_deferred("_re_index")

func _re_index():
	var out := "[center]"
	for name in Globals.pesterlayer.emotes:
		var icon = Globals.pesterlayer.emotes[name]['image'].get_path()
		out +=  "[url="+name+"][img=32]"+icon+'[/img][/url] '
	out += "[/center]"
	richtext.bbcode_text = out


func _on_richtext_meta_clicked(meta):
	p.accept([meta])


func _on_richtext_meta_hover_started(meta):
	$VBoxContainer/title.text = ":"+meta+":"


func _on_close_button_pressed():
	p.dismiss()
