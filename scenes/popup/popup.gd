extends MarginContainer

export var background_dismiss := true
export var popup_on_ready := false
export var queue_free_on_close := false
export var contents:PackedScene

var input := []
var output := []

signal closed(extra_args) # Fire regardless

signal dismissed(extra_args) # fire when the user's vibe is "no"
signal accepted(extra_args) # fire when the user's vibe is "yea"

onready var _contentbox := $content

func _ready():
	if contents:
		set_content(contents.instance())
	if popup_on_ready:
		popup()
	else:
		hide()

func wait_on_close():
	# This doesnt actually seem to work due to how calling a yield from another function works
	# Maybe fixed in 4.0 with await? for now u could yield this yield ig, but easier to just yield the object directly
	
	# warning: no gaurantee this will ever hit if you dont call popup(false) right before
	return yield(self, "closed")

func popup(extra_input=[]):
	if not extra_input is Array:
		extra_input = [extra_input]
	input = extra_input
	show()
	if _contentbox.get_child(0).has_method("on_popup"):
		_contentbox.get_child(0).on_popup(extra_input)
	return []

func set_content(new_content:Node):
	if _contentbox == null:
		_contentbox = $content
	if _contentbox.get_child_count() > 0:
		_contentbox.get_child(0).queue_free()
	_contentbox.add_child(new_content)

func dismiss(extra_args:=[]):
	_close(false, extra_args)


func accept(extra_args:=[]):
	_close(true, extra_args)

func _close(state:bool, extra_args:=[]):
	hide()
	output = extra_args
	emit_signal("closed",[state]+extra_args)
	if state:
		emit_signal("accepted", extra_args)
	else:
		emit_signal("dismissed",extra_args)
	if queue_free_on_close:
		queue_free()


func _on_bg_gui_input(event):
	if background_dismiss and event is InputEventMouseButton and event.pressed == false and event.button_index == BUTTON_LEFT:
		dismiss()
