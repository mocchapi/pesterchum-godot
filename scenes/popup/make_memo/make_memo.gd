extends PanelContainer

onready var text_name = $VBoxContainer/HBoxContainer2/text_name
onready var check_secret = $VBoxContainer/check_secret
onready var check_invite = $VBoxContainer/check_invite
onready var button_create = $VBoxContainer/HBoxContainer/button_create
onready var prefix = $VBoxContainer/HBoxContainer2/prefix

func on_popup(args:Array):
	text_name.text = ''.join(args)
	_on_text_name_text_changed(text_name.text)

func _on_button_create_pressed():
	# TODO replace with usercache ConfigConversation methods
	Globals.irc.join_channel(full_name())
	Globals.main.set_chatwindow_open(true)
	$'../..'.accept()


func _on_button_cancel_pressed():
	pass # Replace with function body.
	$'../..'.dismiss()

func full_name():
	return prefix.text + text_name.text

func _on_text_name_text_changed(new_text):
	
	button_create.disabled = not Globals.validate_channel(full_name()) or len(new_text) == 0
