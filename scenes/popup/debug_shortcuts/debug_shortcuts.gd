extends PanelContainer

const scene_configviewer := preload("res://scenes/debug/save_debug/save_debug.tscn")
const scene_ircdebug := preload("res://scenes/debug/main.tscn")
const scene_packager := preload("res://scenes/debug/packager/packager.tscn")

func _ready():
	yield(get_tree(),"idle_frame")
	if Globals.is_editor_build():
		$"../..".popup()

func _on_button_close_pressed():
	$"../..".dismiss()


func _on_button_ircdebug_pressed():
	get_tree().change_scene_to(scene_ircdebug)


func _on_button_configviewer_pressed():
	get_tree().change_scene_to(scene_configviewer)


func _on_button_packager_pressed():
	get_tree().change_scene_to(scene_packager)


func _on_button_populationstats_pressed():
	get_tree().change_scene("res://scenes/debug/populationstats/populationstats.tscn")
