extends PanelContainer

onready var title := $VBoxContainer/title
onready var text := $VBoxContainer/text
onready var p := $"../.."

func _on_PanelContainer_visibility_changed():
	if len(p.input) >= 1:
		if len(p.input) == 1:
			p.input.insert(0, "")
		title.text = p.input[0]
		text.text = p.input[1]

func _on_button_ok_pressed():
	p.accept()
