extends PanelContainer

onready var p := $"../.."
onready var okbutton = $VBoxContainer/HBoxContainer/okbutton

func _on_cancelbutton_pressed():
	p.dismiss()


func _on_okbutton_pressed():
	Config.chumroll.add_user(
		Globals.usercache.get_user($VBoxContainer/chumhandle.text)
		)
	$VBoxContainer/chumhandle.text = ""
	p.accept([])


func _on_chumhandle_text_entered(new_text):
	_on_okbutton_pressed()


func on_popup(input):
	if len(input) >= 1:
		$VBoxContainer/chumhandle.text = input[0]
		_on_chumhandle_text_changed(input[0])
	$VBoxContainer/chumhandle.grab_focus()


func _on_chumhandle_text_changed(new_text):
	var OK = true
	if new_text == "":
		OK = false
	else:
		for chr in new_text:
			if not chr.to_lower() in 'abcdefghjiklmnopqrstuvwxyz1234567890':
				OK = false
				break
	okbutton.disabled = not OK
