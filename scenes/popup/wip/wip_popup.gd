extends PanelContainer

var btn_names = ["OK", "OKAY", "sure", "hurry up", ":(", "bruh", "Whatever"]

func on_popup(args=null):
	$VBoxContainer/button_ok.text = btn_names[randi() % btn_names.size()]


func _on_button_ok_pressed():
	$"../..".accept()
