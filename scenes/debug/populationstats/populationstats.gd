extends Control

onready var clients_hbox = $"%clients_hbox"
onready var lbl_total_usercount = $"%lbl_total_usercount"
onready var lbl_client_count = $"%lbl_client_count"
onready var irc_client = $"%IRCClient"
onready var btn_recheck = $"%btn_recheck"

const client_item_scene = preload("res://scenes/debug/populationstats/client.tscn")

var userlist := []

func _ready():
	pass


func _on_IRCClient_on_connected():
	irc_client.join_channel("#pesterchum")


func _on_IRCClient_on_join(location):
	if location == "#pesterchum":
		userlist.clear()
		irc_client._sendraw("WHO #pesterchum")
#		irc_client.request_channel_user_list('#pesterchum')


func rebuild(userlist):
	var lookup = {}
	for item in clients_hbox.get_children():
		clients_hbox.remove_child(item)
		item.queue_free()
	
	for user in userlist:
		var realname = user[0]
		var username = user[1]
		var client = guess_client(username, realname)
		
		if not lookup.has(client):
			lookup[client] = []
		lookup[client].append(username)
	
	lbl_total_usercount.text = str(len(userlist))
	lbl_client_count.text = str(len(lookup))
	
	for client in lookup:
		var new = client_item_scene.instance()
		new.client_name = client
		new.client_users = lookup[client]
		clients_hbox.add_child(new)

func guess_client(username,realname):
	if realname.begins_with('chumDroid'):
		return 'Chumdroid'
	elif username == realname:
		return 'Chumdroid OR godot < a8'
	elif username.to_lower().begins_with(realname.to_lower()):
		return 'Chumdroid'
	elif realname == 'pcc31':
		return 'Modern Pesterchum v2.?'
	elif realname == 'pcc30':
		return 'ANCIENT Pesterchum v3.41.2'
	elif realname == 'pco':
		return 'pesterchum.online'
	elif realname == 'pcd10':
		return 'Chumdoid?'
	return realname
#	return "Pesterchum 2.5.3"

func _on_btn_recheck_pressed():
	irc_client.start_connection()


func _on_IRCClient_on_unhandled_numeric_reply(origin, number, location, args):
	if number == "352":
		if args[4] == 'populationStats':
			return
		userlist.append([ args[-1].substr(2), args[4]])
		if args[4] == 'lisanneBottest':
			print(args[1], '->', args[4],'   :::   ',args)
#		print(args[1].substr(2),'::::',args)
		pass
	elif number == "315":
		rebuild(userlist)
		irc_client.quit()


func _on_IRCClient_on_disconnect(reason):
	btn_recheck.disabled = false


func _on_IRCClient_on_connecting():
	if btn_recheck:
		btn_recheck.disabled = true
