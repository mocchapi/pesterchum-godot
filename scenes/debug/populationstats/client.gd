extends VBoxContainer

export(String) var client_name := ""
export(Array, String) var client_users:Array = []

onready var lbl_name = $PanelContainer/VBoxContainer/lbl_name
onready var lbl_usercount = $PanelContainer/VBoxContainer/lbl_usercount

onready var item_list = $PanelContainer2/VBoxContainer/ItemList

func _ready():
	lbl_name.text = client_name
	lbl_usercount.text = str(len(client_users)) + ' Users'
	for username in client_users:
		item_list.add_item(username)
