extends LineEdit

export var fill_screen := true
export var default_path := ""

func _ready():
	yield(get_tree(),"idle_frame")
	if default_path != "":
		$FileDialog.current_dir = default_path

func _on_Button_pressed():
	$FileDialog.popup_centered(get_viewport().size*0.8)

func _on_FileDialog_file_selected(path):
	text = path
