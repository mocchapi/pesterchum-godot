extends MarginContainer


# utlitiy for packaging the various parts of a client theme into the single ClientTheme resource file

onready var text_theme_name := $"%text_theme_name"
onready var text_author := $"%text_author"
onready var text_version := $"%text_version"
onready var text_description := $"%text_description"

onready var path_ui_theme := $"%path_ui_theme"
onready var path_ui_scene := $"%path_packedscene"

onready var option_bundle_method := $"%option_bundle_method"

const pc_theme := preload("res://themes/resources/pesterchum.tres")

var _imported

func _ready():
	$"%lbl_format_version".text = str(pc_theme.format_version)

func populate(obj):
	_imported = obj
	text_theme_name.text = obj.theme_name
	text_author.text = obj.theme_author
	text_version.text = obj.theme_version
	text_description.text = obj.theme_version
	
	path_ui_scene.text = obj.ui_scene.get_path() if obj.ui_scene != null else "NULL"
	path_ui_theme.text = obj.ui_theme.get_path() if obj.ui_theme != null else "NULL"

	$"%lbl_imported_format_version".text = str(obj.format_version)

	if $"%check_apply_ui_theme".pressed and obj.ui_theme != null:
		theme = obj.ui_theme

func make_all_local():
	return option_bundle_method.get_item_id(option_bundle_method.selected) in [0,2]


func bundle_all_resources():
	return option_bundle_method.get_item_id(option_bundle_method.selected) == 2

func form_client_theme() -> ClientTheme:
	var obj = ClientTheme.new()
	
	obj.theme_name = text_theme_name.text
	obj.theme_author = text_author.text
	obj.theme_version = text_version.text
	obj.theme_description = text_description.text
	
	
	if "::" in path_ui_scene.text and _imported != null:
		obj.ui_scene = _imported.ui_scene
	else:
		obj.ui_scene = ResourceLoader.load(path_ui_scene.text,"", true)

	if "::" in path_ui_theme.text and _imported != null:
		obj.ui_theme = _imported.ui_theme
	else:
		obj.ui_theme = ResourceLoader.load(path_ui_theme.text,"", true)

	if make_all_local():
		if obj.ui_scene != null:
			obj.ui_scene = obj.ui_scene.duplicate(true)
			obj.ui_scene.resource_local_to_scene = true
		if obj.ui_theme != null:
			obj.ui_theme = obj.ui_theme.duplicate(true)
			obj.ui_theme.resource_local_to_scene = true
	
	if obj.ui_scene == null:
		obj.ui_scene = pc_theme.ui_scene
	if obj.ui_theme == null:
		obj.ui_theme = pc_theme.ui_theme
	return obj


func export_theme(obj:ClientTheme, path:String):
	var flags := 0
	if bundle_all_resources():
		print("bundle")
		flags = ResourceSaver.FLAG_BUNDLE_RESOURCES
	ResourceSaver.save(path, obj, flags)
	print("done")

func _on_button_import_pressed():
	print("> go import")
	var path :String = $VBoxContainer2/tabs_importexport/Import/text_import_path.text
	print(path)
	var obj = ResourceLoader.load(path,"",true)
	print("path: ",path)
	print("obj: ",obj)
	if obj == null:
		OS.alert("Failed to load")
	else:
		populate(obj)

func _on_button_export_pressed():
	print("go export")
	var path :String = $VBoxContainer2/tabs_importexport/Export/text_export_path.text
	var out = form_client_theme()
	print("path: ",path)
	print("obj: ",out)

	export_theme(out, path)
