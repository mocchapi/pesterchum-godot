extends Control

var quirk = preload("res://scenes/debug/quirkdebugger/terezi_collection.tres")

func _ready():
	pass

func run_through(text):
	$VBoxContainer/output.text = $VBoxContainer/quirkeditor.quirk.apply(text)

func _on_input_text_changed(new_text):
	run_through(new_text)
