extends Control

onready var irc := $IRCClient

onready var debug_output := $"%debug"
onready var chat_windows := $"%chat_windows"
onready var locationlist := $"%locationlist"
onready var msg_send_bar :=$"%message_send"
onready var known_locations = $"%known_locations"
onready var username_bar = $"%username"

var current = ""
var lookup := {}

func _ready():
	current = irc.nickname
	username_bar.text = irc.nickname
# pesterchum protocol cheatsheet
# Moods are handled in the SNEAKILY HIDDEN channel #pesterchum
# They are plain messages
# - GETMOOD handle1handle2handle3 (request others mood)
# - MOOD >number (announce mood, send when changing or detecting GETMOOD with own handle)
#
# Joining #pesterchum sends the online notif, leaving sends the offline notif
#
# Colors are handled in any location. Announce on join
#                                      (Actually, i think they only matter in DMs?)
# - COLOR r,g,b (set color)
#
# To trigger actions like X began trolling Y (send in any location):
#
# PESTERCHUM:BEGIN
# PESTERCHUM:BLOCK
# PESTERCHUM:UNBLOCK
# PESTERCHUM:IDLE
# PESTERCHUM:CEASE
# PESTERCHUM:TIME>value (To set relative time in memos)
# - PESTERCHUM:TIME>F01:11 (for future, zero padded, can be larger than 2 hour digits)
# - PESTERCHUM:TIME>P01:11 (for past, zero padded, can be larger than 2 hour digits)
# - PESTERCHUM:TIME>i (to reset)
#
# <c=r,g,b>my text</c> for colored text
# 
# IN MEMOS YOU HAVE TO SUPPLY YOUR OWN PREFIX. SENDING MESSAGES DISPLAYS THEM AS-IS WITHOUT HANDLE
# in a memo:
#    <c=r,g,b>TT: Hello!!</c>
# in a dm/pester:
#    COLOR r,g,b
#    Hello!!!



func find_list_idx(text:String) -> int:
	for i in locationlist.get_item_count():
		if locationlist.get_item_text(i) == text:
			return i
	return -1

func showtextD(text):
	text = str(text)
	if not text.ends_with("\r\n"):
		text += "\r\n"
	debug_output.append_bbcode(text)

func showtext(target:RichTextLabel,text:String):
	# TARGET IS A TEXTBOX!! NOT STRING!!
	text = str(text)
	if not text.ends_with("\r\n"):
		text += "\r\n"
	target.append_bbcode(text)

func _on_IRCClient_rawreceived(MESSAGE):
	showtextD(irc.split_message(MESSAGE))

func _on_IRCClient_rawsent(MESSAGE):
	showtextD(">>>>"+str(irc.split_message(MESSAGE)))
	


func _on_LineEdit_text_entered(new_text):
	$HBoxContainer/directacces/LineEdit.text = ""
	irc._sendraw(new_text)

func new_chat_page(location):
	var new = RichTextLabel.new()
	new.size_flags_horizontal = new.SIZE_EXPAND_FILL
	new.size_flags_vertical = new.SIZE_EXPAND_FILL
	new.selection_enabled = true
	new.scroll_following = true
	new.visible = false
	new.bbcode_enabled = true
	chat_windows.add_child(new)
	
	locationlist.add_item(location)
	locationlist.set_item_metadata(locationlist.get_item_count(), location)
	
	lookup[location] = new
	

func changevisto(location:String):
	current = location
	for item in lookup:
		lookup[item].visible = item == location

func ensure_page(location:String):
	if not lookup.has(location):
		new_chat_page(location)

func delete_page(location:String):
	if lookup.has(location):
		lookup[location].queue_free()
		var idx = find_list_idx(location)
		if idx != -1:
			locationlist.remove_item(idx)
		lookup.erase(location)

func show_message(location:String, sender:String, content:String, include_time := true):
	var sendnick = irc.split_client_identifier(sender)[0]
	if location == irc.nickname:
		location = sendnick
	
	ensure_page(location)
	
	var textbox = lookup[location]
	
	var out = ""
	if include_time:
		var t = OS.get_time()
		out += "["+ str(t['hour']).pad_zeros(2) + ":" + str(t['minute']).pad_zeros(2) + "] "
	out +=  sendnick+': '+ content
	showtext(textbox, out)


func _on_IRCClient_on_message_received(location, sender, content):
	show_message(location, sender, content)


func _on_locationlist_item_activated(index):
	changevisto(locationlist.get_item_text(index))


func _on_message_send_text_entered(new_text):
	msg_send_bar.text = ""
	if new_text.begins_with('/'):
		irc._sendraw(new_text.substr(1))
	else:
		irc.send_message(current, new_text)


func _on_IRCClient_on_message_sent(location, content):
	show_message(location, irc.nickname, content)


func _on_IRCClient_on_join(location):
	ensure_page(location)


func _on_IRCClient_fallback(MESSAGE):
	showtextD("[color=red]NOPARSE"+"[/color]")


func _on_IRCClient_on_numeric_reply(origin, number, location, args):
	show_message(location, origin, "["+str(number).pad_zeros(3)+"] " + " ".join(args))


func _on_IRCClient_on_user_join(user, location):
	ensure_page(location)
	showtext(lookup[location], "[center] -- "+irc.split_client_identifier(user)[0]+" joined -- [/center]")


func _on_IRCClient_on_user_part(user, location, reason):
	printerr("USER PART!!!")
	ensure_page(location)
	var out = "[center] -- "+irc.split_client_identifier(user)[0]
	out += " left "
	out += (" ("+reason+")") if reason != "" else ""
	out += " -- [/center]"
	showtext(lookup[location], out)




func _on_IRCClient_on_channel_list_updated(channel_list):
	known_locations.clear()
	for item in channel_list:
		known_locations.add_item(item[0])


func _on_change_nickname_pressed():
	irc.set_nickname(username_bar.text)


func _on_refresh_known_locations_pressed():
	irc.request_channel_list()


func _on_connect_pressed():
	irc.nickname = username_bar.text
	irc.start_connection()
