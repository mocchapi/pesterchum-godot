extends Control

var b = preload("res://scenes/debug/save_debug/viewer_item.tscn")
var prev := Vector2.ZERO

func _ready():
	print("welcome to config viewer")
	beans()

func _process(delta):
	var new = rect_size
	if prev != new:
		print("changed: ",new)
		prev= new

func _on_hostname_changed(value):
	print("YOOO",value)

func _onchange(prop, val):
	print("HIYA::",prop, val)

func beans():
	var inst =b.instance()
	inst.obj = Config.c
	inst.title = "root config"
	inst.me = b
	$ScrollContainer/VBoxContainer.add_child(inst)
	var s = Config.settings.connection
	print(s.connect("value_changed", self, "_onchange"))
	print(s.connect("hostname_value_changed", self, "_on_hostname_changed"))
	print(Config.settings.connection.hostname)
