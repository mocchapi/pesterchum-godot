extends PanelContainer

var obj:_baseconfig
var me:PackedScene
var description:String
var title:String

func _ready():
	$VBoxContainer/title.text = "   "+title
	$VBoxContainer/description.text = description
	
	var b = $VBoxContainer/MarginContainer/bepis
	for item in obj.get_options().values():
		var val = obj.get(item['name'])
		var inst
		if val is _baseconfig:
			inst = me.instance()
			inst.title = item['name']
			inst.description = "?: "+item['documentation']
			inst.obj = val
			inst.me = me
		else:
			var instb = PanelContainer.new()
			inst = Label.new()
			inst.text = "  "+item['name']+" = "+str(val)+"\n?: "+item['documentation']
			inst.autowrap = true
			instb.add_child(inst)
			inst = instb
		print("setup ",title,'/',item['name'])
		b.add_child(inst)
