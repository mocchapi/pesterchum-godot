extends Node

class_name _PesterchumLayer

# Provides functions & signals for interacting with pesterchum stuff
# Like some kinda layer,,, between pchum & irc,,,
#
# Most parsing functions have been moved to Parsetools
# The prefered method of getting messages in a specific channel / from a specific user
# is to grab the ConfigCovnersation from Usercache with get_conversation, & connecting to the received(), sent(), or new_message() signals
#


signal focus_conversation(conversation)
signal user_mood_changed(new_mood, old_mood, user)
signal user_color_changed(new_color, old_color, user)

export(String, DIR) var emote_directory:String = "res://assets/texture/emotes/"
export(String, DIR) var mood_directory:String = "res://assets/texture/moods/"



const pesterchum_special := {
	BEGIN = "-- {sender} {sendersmall} began pestering {receiver} {receiversmall} at {time} --",
	CEASE = "-- {sender} {sendersmall} ceased pestering {receiver} {receiversmall} at {time} --",
	IDLE = "-- {sender} {sendersmall} is now an idle chum! --",
}


const mood_names := [
	"chummy",
	"rancorous",
	"offline",
	"pleasant",
	"distraught",
	"pranky",
	"smooth",
	"ecstatic",
	"relaxed",
	"discontent",
	"devious",
	"sleek",
	"detestful",
	"mirthful",
	"manipulative",
	"vigorous",
	"perky",
	"acceptant",
	"protective",
	"mystified",
	"amazed",
	"insolent",
	"bemused",
	]

var emotes := {} # emote streamimage and path to file, loaded dynamically
const emote_aliases := { # emotes that trigger the same image as its value
	woeful = "bemused",
	smooth = "cool",
}
var moods := {} # mood streamimage and path to file, loaded dynamically




func _enter_tree():
	Globals.pesterlayer = self

func _ready():
	_load_moods()
	_load_emotes()
	Config.current_profile.connect("random_encounters_value_changed",self,"_on_random_encounters_value_changed")

func _load_moods(directory:=mood_directory):
	if not directory.ends_with('/'):
		directory += '/'
	var dir = Directory.new()
	if dir.open(directory) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if file_name.ends_with('.import'):
				file_name = file_name.replace('.import','')
				if not dir.current_is_dir() and not file_name.split('.')[-1] in ['gif']:
					var key = file_name.split('.')[0]
					if key in mood_names:
						moods[key] = {'path':directory + file_name, 'image':load(directory + file_name)}
			file_name = dir.get_next()

func _load_emotes(directory:=emote_directory):
	# Currently godot doesnt import gifs so i'll have to think of something
	# maybe an automated gif -> spritesheet converter? would that work with bbcode?
	# custom importer?
	if not directory.ends_with('/'):
		directory += '/'
	var dir = Directory.new()
	if dir.open(directory) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if file_name.ends_with('.import'):
				file_name = file_name.replace('.import','')
				if not dir.current_is_dir() and not file_name.split('.')[-1] in ['gif']:
					emotes[file_name.split('.')[0].to_lower()] = {'path':directory + file_name, 'image':load(directory + file_name)}
			file_name = dir.get_next()
	
	for item in emote_aliases:
		emotes[item] = emotes[emote_aliases[item]]

#func dispatch_notice(location:String, name:String):
#	if location in Config.robots:
#		return
#	if name.begins_with('PESTERCHUM:'):
#		name = name.replace('PESTERCHUM:',"")
#	if name in pesterchum_special:
#		send_message(location, 'PESTERCHUM:'+name, false, false)

#func dispatch_timeframe(location:String, timeframe=null):
#	print("(PesterLayer) dispatch_timeframe: ","DISPATCH TIMEFRAME TO ",location)
#	var memo = Globals.usercache.get_memo(location)
#	if timeframe != null:
#		memo.set_user_timeframe("",timeframe)
#	Globals.irc.send_message(location, memo.get_user_dispatch(""))

func request_mood(users, force_if_metadata:bool=true)->bool:
	# users can be singular user or array of users
	if typeof(users) == TYPE_STRING:
		users = [users]
	if Config.settings.server.is_legacy_mode():
		print("(PesterLayer) request_mood: request legacy mood for ",users)
		send_message(Config.c.settings.server.legacy_mood_channel,"GETMOOD "+' '.join(users), false)
		return true
	elif Config.settings.server.is_normal_mode():
		if force_if_metadata:
			if Globals.irc.cap_requests_results.get('draft/metadata', false):
				print("(PesterLayer) request_mood: request metadata mood for ",users)
				for user in users:
					Globals.irc._sendraw("METADATA "+user+" GET mood")
					return true
			else:
				printerr("(PesterLayer) request_mood: could not request moods through metadata, because server doesnt support it!")
				return false
		else:
			printerr("(PesterLayer) request_mood: legacy mode is disabled, mood request ignored")
			return false
	return false


func send_message(location:String, content:String, add_color:=true, quirkify:=false):
	if quirkify:
		content = Config.current_profile.quirks.apply(content)
	if location[0] == "#" and add_color:
		# Its a memo, add handle
		content = Config.current_profile.get_initials()+': '+content 
		content = "<c=#"+Config.current_profile.color.to_html(false)+">"+content+'</c>'
	Globals.irc.send_message(location, content)

func dispatch_legacy_color(user:ConfigUser):
	if user.is_robot:
		return
	print("(pesterlayer) dispatch_legacy_color: Notifying ",user.chumhandle," of color")
	Globals.usercache.get_conversation(user.chumhandle).sendraw(
		Globals.parsetools.form_legacy_color(Config.current_profile.color)
	)

func dispatch_legacy_mood():
	if not Config.settings.server.should_obey_legacy_moods():
		printerr("(pesterlayer) dispatch_legacy_mood: obey_legacy_moods disabled")
	var idx:int = mood_names.find(Config.current_profile.mood)
	Globals.irc.send_message(Config.settings.server.legacy_mood_channel,"MOOD >"+str(idx))

func set_mood(mood):
	# TODO: evaluate if this is an appropriate place to handle this
	# Yeah i think so
	var idx:int
	if mood is String:
		idx = mood_names.find(mood)
		if idx == -1:
			printerr("(PesterLayer) set_mood: no such mood '",mood,"'")
			return
	else:
		idx = mood
		mood = mood_names[idx]
	Config.current_profile._set("mood",mood)
	emit_signal("mood_changed",mood)
	
	if Config.settings.server.is_normal_mode() and Globals.irc.cap_requests_results.get('draft/metadata',false):
		print("(PesterLayer) set_mood: broadcasting changed mood through metadata: ",mood, " (",idx,")")
		Globals.irc._sendraw("METADATA * SET mood :"+str(idx))
	elif Globals.irc != null and Config.settings.server.is_legacy_mode():
		print("(PesterLayer) set_mood: broadcasting changed mood through legacy: ",mood, " (",idx,")")
		dispatch_legacy_mood()

func set_color(color:Color):
	print("(Pesterlayer) set_color: broadcasting changed color: #",color.to_html(false))
	Config.current_profile._set("color",color)
	if Config.settings.server.is_normal_mode():
		print("(Pesterlayer) set_color: using metadata")
		Globals.irc._sendraw("METADATA * SET color :#"+color.to_html(false))

	elif Config.settings.server.is_legacy_mode():
		for conv in Globals.usercache.get_open_conversations():
			if conv is PrivmsgConversation:
				conv.sendraw(
					Globals.parsetools.form_legacy_color(Config.current_profile.color)
					)


# Leaving this here for now in case i missed somethingin the new systems :P
# This was the old message parsing function, now replaced by
# the ingest_message func here & the parse_message functions on ConfigConversation & its subclasses

#func old_message_to_bbcode(MESSAGE:String, sender:="", location:=""):
#	# TODO: holy shit. this needs to be not absolutely terrible
#	# Parses the pesterchum formats to displayable bbcode
#	assert(sender != "")
#	var is_me = sender == Config.current_profile.chumhandle
#	if MESSAGE.begins_with("COLOR >"):
#		if is_me:
#			return
#		var color = MESSAGE.split(">")[-1]
#		Globals.usercache.get_user(sender).color = parse_color(color)
#		return
#	var memo
#	if location[0] == '#':
#		memo = Globals.usercache.get_memo(location)
#		if MESSAGE.begins_with("PESTERCHUM:TIME"):
#			var value = MESSAGE.split('>')[1]
#			var display_special = true 
#			if value == 'i':
#				memo.set_user_timeframe(sender,0)
#			else:
#				var future = value[0] == 'F'
#				value = value.substr(1)
#				var hour_minute = value.split(':')
#				var new = abs(int(hour_minute[0]))*60 + abs(int(hour_minute[1]))
#				if not future:
#					new = -abs(new)
#				memo.set_user_timeframe(sender,new)
#			if display_special:
#				return _colorwrap(memo.get_user_special_begin(sender))
#			return
#	if MESSAGE.begins_with('PESTERCHUM:'):
#		var _sender = sender
#		var _receiver = Config.current_profile.chumhandle
#		var _sender_color
#		var _receiver_color
#		if is_me:
#			_sender = Config.current_profile.chumhandle
#			_sender_color = Config.current_profile.color.to_html(false)
#			_receiver = location
#			_receiver_color = Globals.usercache.get_user(_receiver).color.to_html(false)
#		else:
#			_sender_color = Globals.usercache.get_user(_receiver).color.to_html(false)
#			_receiver_color = Config.current_profile.color.to_html(false)
#		var formating = {
#			'receiver':_receiver,
#			'sender': _sender,
#			'sendersmall':'[color=#'+_sender_color+']['+format_sender(_sender)+'][/color]',
#			'receiversmall':'[color=#'+_receiver_color+']['+format_sender(_receiver)+'][/color]',
#			'time':get_timestamp(false)
#			}
#		var key = MESSAGE.split(':')[-1]
#		if key in pesterchum_special:
#			return _colorwrap(pesterchum_special[key].format(formating))
#
#	if "<c=" in MESSAGE:
#		var segments = MESSAGE.split('<c=')
#		var out = []
#		for item in segments:
#			item = item.split('>',true,1)
#			if len(item) < 2:
#				print("(PesterLayer) message_to_bbcode: ","skipping color item ",item)
#				continue
#			var color = item[0]
#			var remaining_text = item[1]
#			out.append("[color=#"+parse_color(color).to_html(false)+"]"+remaining_text.replace('</c>','[/color]'))
#		MESSAGE = ''.join(out)
#
#	# TODO: make this less shit. maybe.
#	for item in emotes:
#		MESSAGE = MESSAGE.replacen(':'+item+':', '[img]'+emotes[item]['image'].get_path()+'[/img]')
#
#
#	var clr:= Color.black
#	if is_me:
#		clr = Config.current_profile.color
#	else:
#		clr = Globals.usercache.get_user(sender).color
#
#	if location[0] == '#':
#
#		var split := MESSAGE.find(':')
#		var A = MESSAGE.substr(0, split)
#		A = A.substr(0, len(A)-2)
#		var B = MESSAGE.substr(split, -1)
#		MESSAGE = A + memo.get_user_shorthandle(sender) + B
#		if Config.settings.interface.show_timestamps_in_memos:
#			MESSAGE = memo.get_user_timestamp(sender) + " " + MESSAGE
#		return MESSAGE
#	else:
#		MESSAGE = format_sender(sender if sender != '' else Config.current_profile.chumhandle)+": "+MESSAGE
#	MESSAGE = '[color=#'+clr.to_html(false)+']'+MESSAGE+'[/color]'
#	if Config.settings.interface.show_timestamps_in_pesters:
#		MESSAGE = get_timestamp()+" "+MESSAGE
#	return _colorwrap(MESSAGE)

func get_timestamp(add_brackets:=true, add_time:=0.0, timestamp=null):
	var t
	if timestamp == null:
		t = Time.get_time_dict_from_unix_time(Time.get_unix_time_from_system() + add_time + Time.get_time_zone_from_system()['bias']*60)
	else:
		t = timestamp
	var out =str(t['hour']).pad_zeros(2) + ":" + str(t['minute']).pad_zeros(2)
	if add_brackets:
		out =  "["+ out + "]"
	return out


var has_connected:bool = false

func _on_first_connected():
	# Triggered only on the very first connect, not reconnects
	has_connected = true
	
	for channel in Config.current_profile.autojoin_memos:
		Globals.usercache.get_conversation(channel).join()

func _on_IRCClient_on_connected():
	if not has_connected:
		_on_first_connected()

	if Config.settings.server.should_obey_legacy_moods():
		Globals.irc.join_channel(Config.settings.server.legacy_mood_channel)
	
	
	print('(PesterLayer) _on_IRCClient_on_connected: ','Requesting moods for chums')
	_fetch_chumroll_moods()
	
	if Config.current_profile.nickserv_password != "":
		Globals.usercache.get_conversation(
			Config.settings.server.nickserv_handle
			).sendraw(
				"IDENTIFY "+Config.current_profile.nickserv_password
				)
	
	if Globals.irc.cap_requests_results.get('draft/metadata', false) and Config.settings.server.is_normal_mode():
		Globals.irc._sendraw("METADATA * SUB mood")
		Globals.irc._sendraw("METADATA * SUB color")
	
	for conversation in Globals.usercache.get_open_conversations():
		if conversation is MemoConversation and conversation.is_participating:
			conversation._put_reconnect()
			conversation.join()
	
	set_color(Config.current_profile.color)
	set_mood(Config.current_profile.mood)
	
	notify_RE_state()
#	Globals.usercache.get_conversation()

func notify_RE_state():
	var state = '+' if Config.current_profile.random_encounters else '-'
	print("(PesterLayer) notify_RE_state: telling RE: ",state,' (',Config.current_profile.random_encounters,')')
	Globals.irc.send_notice(Config.settings.server.randomencounter_handle, state)

func request_RE_user():
	Globals.irc.send_notice(Config.settings.server.randomencounter_handle, '!')

func _fetch_chumroll_moods():
	for user in Config.chumroll:
		user._set("mood","offline")
		user.request_mood(true)


func _on_IRCClient_on_notice_received(location, sender, content):
	var nick = Globals.irc.split_client_identifier(sender)[0]
	if nick in Config.c.robots:
		if nick == Config.settings.server.randomencounter_handle:
			if content.begins_with('!='):
				var encounter_name = content.substr(2)
				print("(PesterLayer) _on_IRCClient_on_notice_received: RE has given us: ",encounter_name)
				if encounter_name in Config.robots or encounter_name == Config.current_profile.chumhandle:
					print("              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  Thats me/a robot!! asking again")
					request_RE_user()
					return
				else:
					var conv = Globals.usercache.get_conversation(encounter_name)
					conv.open()
					conv.request_focus()
				return
			else:
				var meaning = ""
				match content:
					'-=k':
						meaning = 'off'
					'+=k':
						meaning = 'on'
					'~=k':
						meaning = 'afk'
					_:
						meaning = 'idk'

				print("(PesterLayer) _on_IRCClient_on_notice_received: RE says we are: ",content,' which means: ',meaning)

		_on_IRCClient_on_message_received(location, sender, content)
	else:
		printerr("(PesterLayer) _on_IRCClient_on_notice_received: notice from ",nick,". not in robots, so silently dropped. (", Config.c.robots,')')


func is_blocked(sender:ConfigUser):
	if sender.is_robot or Config.chumroll.contains_user(sender) or sender == Config.current_profile:
		return false
	elif Config.blocklist.contains_user(sender):
		return true
	return false

func idx_to_mood(idx:int):
	if idx in range(mood_names.size()):
		return mood_names[idx]
	else:
		return null

func parse_legacy_mood(user:ConfigUser, content:String):
	var mood_idx = content.replace('MOOD >','')
	mood_idx = int(mood_idx)
	if mood_idx in range(mood_names.size()):
		var mood = mood_names[mood_idx]
		user._set("mood", mood)
	else:
		printerr("(pesterlayer) parse_legacy_mood: ",user.chumhandle,' emitted mood ',mood_idx,' but this is outside the range of known moods.')

func parse_legacy_getmood(content:String):
	var handles = content.split(' ')
	handles.remove(0)
	if Config.current_profile.chumhandle in handles:
		dispatch_legacy_mood()

func _on_IRCClient_on_message_received(location:String, sender_full:String, content:String):
	var sender = Globals.irc.split_client_identifier(sender_full)[0]
	var sender_user = Globals.usercache.get_user(sender)
	ingest_message(location, sender_user, content)

func ingest_message(location:String, sender:ConfigUser, content:String):
	# TODO:
	# finally, theres some userlist widgets that dont work anymore
	# because i removed the list_path export. because that bit me in the ass too often
	# known: chumroll & profile switcher
	
	var message_type = Globals.parsetools.detect_message_type(location, content)	

	if message_type == "chanserv_memonote":
		location = content.split(' ')[0]
		location = location.substr(1, len(location)-2)

	var conversation:ConfigConversation = Globals.usercache.get_conversation(location)
	
	
	print("(PesterLayer) ingest_message: msg: `",content,'`')
	print('              ^^^^^^^^^^^^^^  in ',location,' by ',sender.chumhandle)
	print("              ^^^^^^^^^^^^^^  msg type: ",message_type)
	
	match message_type:
		'color':
			var color = Globals.parsetools.parse_color(content.replace('COLOR >',''))
			print("(PesterLayer) ingest_message: set ",sender,' color to ',color)
			sender._set("color", color)
			
		'getmood':
			if sender != Config.current_profile:
				parse_legacy_getmood(content)
		'mood':
			parse_legacy_mood(sender, content)
		
		_:

			conversation.parse_message(sender, content, message_type)
	
	return
	

func _on_random_encounters_value_changed(new_value, old_value):
	notify_RE_state()

	# This will cause all users to become cached on first message
	# including #pesterchum MOOD > and stuff
	# But its probably not a big deal
	# its even less of a deal if legacy mode is disabled
	# New code up there ^ ^ ^
	# (Actually. the _on_receive is called down below)
	# old code here v v v

#	if location == Config.settings.server.legacy_mood_channel and Config.settings.server.obey_legacy_moods:
#		if content.begins_with("MOOD >") and Globals.usercache.has_user(sender):
#			var mood_idx = content.replace('MOOD >','')
#			mood_idx = int(mood_idx)
#			if mood_idx in range(mood_names.size()):
#				var mood = mood_names[mood_idx]
#				if user.mood != mood:
#					emit_signal("user_mood_changed",usr)
#				var u =user._set("mood", mood)
#			else:
#				printerr("(PesterLayer) _on_irc_on_message_received: mood index ",mood_idx," is out of bounds! (",content,")")
#		elif content.begins_with('GETMOOD '):
#			var handles = content.split(' ')
#			if Config.current_profile.chumhandle in handles:
#				dispatch_legacy_mood()
#		return
#	var out = "{0} {1}: {2}".format([get_timestamp(), sender, content])
#
#	if not sender in Config.robots:
##		out = old_message_to_bbcode(content, sender, location)
#		if out == null:
#			return
#
#	out += "\n"
#
##	conversation._on_receive(sender_user, out)
#
#	emit_signal("on_processed_message", location, sender, out)
#
#	if Globals.validate_channel(location):
#		emit_signal("on_processed_message_memo", location, sender, out)
#	else:
#		emit_signal("on_processed_message_dm", location, sender, out)

func _on_IRCClient_on_message_sent(location, content):
#	var is_legacy_mood_channel = Config.settings.server.obey_legacy_moods and location == Config.settings.server.legacy_mood_channel
#	if is_legacy_mood_channel or content == "PESTERCHUM:CEASE":
#		# Intercept opening a new tab for either of these
#		# #pesterchum because its the invisible metadata channel
#		# PESTERCHUM:CEASE because it would open a DM right after closing it
#		# location is blocked
#		printerr("(PesterLayer) _on_irc_on_message_sent: supressed sent msg ",content,' to ',location," from getting emitted to logs, because:")
#		if is_legacy_mood_channel:
#			printerr("	destination is the moods channel")
#		if content == "PESTERCHUM:CEASE":
#			printerr("	content is a CEASE notice")
#		return
##	var out = old_message_to_bbcode(content, Config.current_profile.chumhandle, location)
##	if out == null:
##		return
#	var out = content
#	out += "\n"
	
	ingest_message(location, Config.current_profile, content)
#	var message_type = Globals.parsetools.detect_message_type(location, content)
#
#	Globals.usercache.get_conversation(location).parse_message(content, message_type)
#	emit_signal("on_processed_sent", location, out)

func _on_IRCClient_on_unhandled_numeric_reply(origin, number, location, args):
	pass



