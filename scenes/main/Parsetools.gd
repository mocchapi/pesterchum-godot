extends Node
class_name _ParseTools

var RE_color := RegEx.new()
var RE_url := RegEx.new()
var RE_highlights := {
	"url": RE_url,
	"memo": RegEx.new(),
	"imgurl": RegEx.new(),
	"handle": RegEx.new(),
}


enum MSGTYPE {
	normal,
	color,
	mood,
	cease,
	begin,
	idle,
	blocked,
	me,
	timeframe,
}

func _ready():

	# Obtained from our dear friends at https://github.com/Dpeta/pesterchum-alt-servers/blob/main/parsetools.py	
	RE_url.compile("(?i)(?:^|(?<=\\s))(?:(?:https?|ftp)://|magnet:)[^\\s]+")
	RE_color.compile("(?i)<c=(.*?)>")
	RE_highlights['memo'].compile("(\\s|^)(#[A-Za-z0-9_\\.]+)")
	RE_highlights['imgurl'].compile("(?i)<img src=['\"](\\S+)['\"]\\s*/>")
	RE_highlights['handle'].compile("(\\s|^)(@[A-Za-z0-9_]+)")

func split_memo_initials(message:String):
	# Returns a memo without the initials
	# IE "<c=32,50,0>TT: bazinga :D</c>"
	# becomes ["<c=32,50,0>TT", "bazinga :D</c>"]
	# note that the ": " is lost
	# also note that you also have to strip the color prefix if it exists and just want the initials
	# You can do this easily by splitting on > and taking the last index 
	var split = message.split(':')
	if ' ' in split[0]:
		return ['', message]
	else:
		var out = [ split[0] ]
		split.remove(0)
		out.append( ':'.join(split).substr(1) )
		return out

func detect_message_type(location_name:String, message:String):
	# Determines what kinda message something is
	# possible values:
	# message, pchum_begin, pchum_cease, pchum_block, pchum_blocked, pchum_unblock,
	# pchum_idle, me, timeframe, color, getmood, mood, chanserv_memonote
	if location_name == Config.c.settings.server.chanserv_handle and message.begins_with('[#'):
		return 'chanserv_memonote'
	if message.begins_with('PESTERCHUM:'):
		match message:
			'PESTERCHUM:BEGIN':
				return 'pchum_begin'
			'PESTERCHUM:CEASE':
				return 'pchum_cease'
			'PESTERCHUM:BLOCK':
				return 'pchum_block'
			'PESTERCHUM:BLOCKED':
				return 'pchum_blocked'
			'PESTERCHUM:UNBLOCK':
				return 'pchum_unblock'
			'PESTERCHUM:IDLE':
				return 'pchum_idle'
		if message.begins_with('PESTERCHUM:ME'):
			return 'me'
		elif message.begins_with("PESTERCHUM:TIME>") and location_name.begins_with('#'):
			return 'timeframe'
	
	if message.begins_with('GETMOOD ') \
				and location_name == Config.c.settings.server.legacy_mood_channel\
				and Config.c.settings.server.obey_legacy_moods:
		return 'getmood'
		
	elif message.begins_with('/me') \
				or ( \
					Globals.validate_channel(location_name) \
					and ':' in message \
					and strip_initials(message).begins_with('/me') \
					):
		return 'me'

	elif message.begins_with('COLOR >'):
		return 'color'

	elif message.begins_with('MOOD >') \
				and location_name == Config.c.settings.server.legacy_mood_channel:
		return 'mood'

	else:
		return 'message'

func color_to_rgb(clr:Color):
	return ','.join([int(clr.r*255), int(clr.g*255), int(clr.b*255)])

func strip_initials(memo_content:String):
	# Removed the `?TT: ` part of a memo message
	return split_memo_initials(memo_content)[1]

func parse_color(part:String)->Color:
#	print("(Parsetools) parse_color: ",part)
	if part.count(',') >= 2:
#		print("             ^^^^^^^^^^^  This is an RGB color")
		var rgb = part.split(',')
#		print("             ^^^^^^^^^^^  ",rgb,' -> ', float(rgb[0])/255,',', float(rgb[1])/255,',', float(rgb[2])/255)
		return Color(float(rgb[0])/255, float(rgb[1])/255, float(rgb[2])/255)
	elif part[0] == '#' or len(part) == 6:
#		print("             ^^^^^^^^^^^  This is a HEX color")
		return Color(part)
#	printerr("(Parsetools) parse_color: failed parsing '",part,"'")
	return Color.black



func clean_user_message(user_message:String)->String:
	# Safely escapes messages with [square brackets] to not get parsed as BBcode
	return user_message.replace('[', '[​')
	# This may look like it does nothing, BUT that second string has
	# a U+200B ZERO WIDTH SPACE at the end which does not render
	# but does make any bbcode invalid



func format_full_reltime(hours_minutes:Array):
	var hours:int = hours_minutes[0]
	var minutes:int = hours_minutes[1]
	
	var hours_padded:String = str(hours).pad_zeros(2)
	var minutes_padded:String = str(minutes).pad_zeros(2)
	
	if hours != 0:
		return "{0}:{1} HOURS".format([hours_padded, minutes_padded])
	elif minutes != 0:
		return "{0} MINUTES".format([minutes_padded])
	else:
		return ""


func timestamp_to_H_M(timestamp:int)->Array:
	timestamp = abs(timestamp)
	var hours:int = floor(float(timestamp)/60)
	var minutes:int = timestamp - hours*60
	return [hours, minutes]



func translate_user_message(user_message:String)->String:
	# Translates a user message to include the proper BBCode tags to make it work on our end
	# Expects a clean string!
	user_message = insert_colors(user_message)
	user_message = insert_emotes(user_message)
	user_message = insert_highlights(user_message)
	return user_message

func insert_colors(user_message:String)->String:
	# Make colors good
	user_message = user_message.replace('</c>', '[/color]')
	
	var color_matches = RE_color.search_all(user_message)
	color_matches.invert()
	for instance in color_matches:
		var new = user_message
		new.erase(instance.get_start(), len(instance.get_string()))
		user_message = new.insert(instance.get_start(), '[color=#' + parse_color(instance.get_string(1)).to_html(false) + ']')
	return user_message

func insert_highlights(user_message:String):
	# Turn some aspects in to clickable elements
	for highlight_name in RE_highlights:
		var regex:RegEx = RE_highlights[highlight_name]
		var matches = regex.search_all(user_message)
		matches.invert()
		for instance in matches:
			var as_url = '[color=blue][url='+highlight_name+':'+instance.get_string()+']'+instance.get_string()+'[/url][/color]'
			var new = user_message
			new.erase(instance.get_start(), len(instance.get_string()))
			user_message = new.insert(instance.get_start(), as_url)
	return user_message




func insert_emotes(user_message:String):
	# Insert emotes
	for emote_name in Globals.pesterlayer.emotes:
		var emote_img = '[img]' + Globals.pesterlayer.emotes[emote_name]['image'].get_path() + '[/img]'
		user_message = user_message.replace(':'+emote_name+':', emote_img)
	return user_message

func color_wrap(message:String, color:=Color("#646464")):
	return "[color="+color.to_html(false)+"]"+message+"[/color]"

func form_legacy_color(color:Color)->String:
	return "COLOR >"+color_to_rgb(color)


func is_valid_chumhandle(handle:String):
	if len(handle) < 2:
		printerr("(Parsetools) is_valid_chumhandle: handle too short")
		return false

	if handle[0] in '0123456789!#':
		printerr("(Parsetools) is_valid_chumhandle: handle[0] is numeric")
		return false

	if handle[0] == handle[0].to_upper():
		printerr("(Parsetools) is_valid_chumhandle: handle[0] is capital")
		return false
	

	var capitals := 0
	for chr in handle:
		if chr in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
			capitals += 1

	if capitals == 0:
		printerr("(Parsetools) is_valid_chumhandle: no capitals")
		return false
	if capitals > 1:
		printerr("(Parsetools) is_valid_chumhandle: more than one capital")
		return false
	return true
