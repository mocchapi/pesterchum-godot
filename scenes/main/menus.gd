extends MarginContainer

func get_menu(cat:String, name:String):
	var ident = cat.to_lower()+'/'+name.to_lower()
	for item in get_children():
		if item.identifier == ident:
			return item

func show_menu(menu_category, menu_name):
	var item = get_menu(menu_category, menu_name)
	var ok:bool = item != null and item.enabled
	if ok:
		item.show()
	return ok


func handle_or_show(menu_category, menu_name):
	# Exceptions for the menu system
	# If none of these cases match, itll try to open a child of `/pesterchum_main/approot/menus`
	# with corrosponding menu_category and menu_name
	print("(menus.gd) handle_or_show: "," check ",menu_category,'/',menu_name)
	match menu_category.to_lower() + '/' + menu_name.to_lower(): 
		'help/chanserv':
			if Config.settings.server.chanserv_handle == "":
				return
			var conv = Globals.usercache.get_conversation(Config.settings.server.chanserv_handle)
			conv.open()
			conv.request_focus()

		'help/nickserv':
			if Config.settings.server.nickserv_handle == "":
				return
			var conv = Globals.usercache.get_conversation(Config.settings.server.nickserv_handle)
			conv.open()
			conv.request_focus()

		'help/calsprite':
			if Config.settings.server.calsprite_handle == "":
				return
			var conv = Globals.usercache.get_conversation(Config.settings.server.calsprite_handle)
			conv.open()
			conv.request_focus()
		'client/reload':
			Config.save()
			Globals.hard_reload()
		'profile/preferences':
			var m = get_menu(menu_category, menu_name)
			m.menu.use_current_profile = true
			m.show()
		'client/pester':
			pass
		'client/help':
			pass
		'client/random encounter':
			Globals.pesterlayer.request_RE_user()
		_:
			print("    Not a special case, summoning normal menu")
			if not show_menu(menu_category, menu_name):
				print("notimplnotice: ",menu_category,"/",menu_name)
				Globals.main.notimplnotif()
			else:
				print("    all OK")
