extends MarginContainer

func getpop(name:String):
	return get_node_or_null(name)

func showpop(name:String):
	var p = getpop(name)
	if p:
		p.popup()
		return p
