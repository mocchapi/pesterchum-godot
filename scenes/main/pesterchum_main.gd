extends Node

class_name _pesterchum_main

var ready := false

signal chatwindow_changed(state_main, state_chat)


export(Resource) var theme setget load_theme

onready var approot := $approot
onready var themeslot := $approot/backupscroll
onready var themenode := $approot/backupscroll/app

onready var menus_ := $approot/menus
onready var popups := $approot/popups


const addchum := preload("res://scenes/popup/add_chum/add_chum.tscn")
const notifclass := preload("res://scenes/popup/popup.tscn")

var theme_resource:Theme

var chatwindow_open:= false setget set_chatwindow_open


func _ready():
	$"%version_number".text = VERSIONS.application + ' // config v' + str(VERSIONS.config_format) + ' // theme v' + str(VERSIONS.theme_format)
	ProjectSettings.set_setting("display/window/dpi/allow_hidpi", Config.settings.interface.use_hiDPI)

	if Globals.running_on_phone():
		# Make the app fit better to differing mobile screens
		get_tree().set_screen_stretch( SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_EXPAND, Vector2(ProjectSettings.get_setting("display/window/size/width"), ProjectSettings.get_setting("display/window/size/height")))
	
	_do_transparent()
	OS.window_borderless = false
	ready = true
	
	Config.connect("loading_failed", self, "_on_config_loading_failed")
	$approot/popups/login_popup.connect("accepted", self, "load_config_theme", [], CONNECT_ONESHOT)

	if Config.c.settings.flags.check_for_updates_on_start:
		var err = $update_request.request(VERSIONS.update_check_url)
		if err != OK:
			simpleerror("Could not start communicating with update server\nerror code: "+str(err),"Error checking for updates")

func _input(event):
	if event.is_action_released("debug", true):
		Globals.popups.showpop("debug_shortcuts_popup")

func _on_Main_tree_exiting():
	if Config.check_if_changed():
		Config.save()

onready var fullheight = ProjectSettings.get_setting("display/window/size/height")
var last := OS.get_virtual_keyboard_height()
func _process(delta):
	var new = OS.get_virtual_keyboard_height()
	if new != last:
		var target = 0
		if new > 0:
			var focus = approot.get_focus_owner()
			if focus != null:
				var ratio = (approot.rect_size.y / get_tree().get_root().size.y)
				var diff = focus.rect_global_position.y - (new*ratio)
				if diff < new*ratio:
					target = 0
				else:
					target = -new*ratio - (Config.c.settings.flags.virtual_keyboard_offset)
		match Config.c.settings.flags.virtual_keyboard_mode:
			Globals.VKMODE.PUSH_UP:
				approot.rect_size.y = target
				approot.rect_global_position.y = 0
			Globals.VKMODE.NONE:
				approot.rect_size.y = fullheight
				approot.rect_global_position.y = 0
			Globals.VKMODE.RESIZE, _:
				approot.rect_global_position.y = 0
				approot.rect_size.y = fullheight+target
		last = new

func toggle_chatwindow_open():
	set_chatwindow_open(not chatwindow_open)


func set_chatwindow_open(state:bool):
	# Politely informs the theme to open or close the chatwindow
	# This usually only has an effect in SINGLEWINDOW display mode
	# And can vary in functionality between themes.
	chatwindow_open = state
	var state_main := false
	var state_chat := false
	match Config.settings.flags.display_mode if not Globals.running_on_phone() else Globals.DISPLAYMODES.SINGLEWINDOW:
		Globals.DISPLAYMODES.MULTIWINDOW:
			state_main = true
			state_chat = true
		Globals.DISPLAYMODES.SINGLEWINDOW, _:
			state_main = not state
			state_chat = state
	
	get_tree().call_group("chat_screen", "set_visible", state_chat)
	get_tree().call_group("main_screen", "set_visible", state_main)
	
	emit_signal("chatwindow_changed", state_main, state_chat)

func _do_transparent():
	get_tree().get_root().set_transparent_background(!Globals.running_on_phone())

func resolve_key(key:String, node:Node):
	var items = key.split('/')
	var next = node
	for item in items:
		if item.begins_with('.'):
			continue
		next = node.find_node(item)
		if next == null:
			return null
	return next

func switch_profile(profile):
	pass

func load_theme(new_theme:ClientTheme):
	# 
	# Load a theme packedscene
	# 
	# Themes
	# And finally: this whole system kind of depends on the honor system. Please inspect how the pesterchum theme works and follow its example until theres a guide
	#
	# If you are only editing the theme resource, you can simply duplicate the pesterchum theme and modify that
	#
	# Theres also a set of node groups you can add nodes to to make developing and adapting to different platforms easier:
	# - hide_on_phones: hides the node when the detected platform is android or ios on load
	#
	# - show_on_multiwindow: Node gets shown if Config.display_mode == DISPLAYMODES.MULTIWINDOW
	# - show_on_singlewindow: Node gets shown if Config.display_mode == DISPLAYMODES.SINGLEWINDOW
	#
	# - hide_on_multiwindow: Node gets hidden if Config.display_mode == DISPLAYMODES.MULTIWINDOW
	# - hide_on_singlewindow: Node gets hidden if Config.display_mode == DISPLAYMODES.SINGLEWINDOW
	#
	# - show_on_editor: Node gets shown when running an editor instance (useful for debug tools)
	# - hide_on_release: Node gets hidden when not running on an editor instance (useful for debug tools)
	#    ^ Note that this triggers both on debug and release mode builds. It only checks if the app is launched from the godot editor
	#
	# - chat_screen: Hidden when main screen is open, shown when main screen is closed
	# - main_screen: Hidden when chat screen is open, shown when chat screen is closed
	#    ^ Note that in multiwindow mode, both of these are always visible at the same time

	theme = new_theme
	if is_instance_valid(themenode) and themenode != null:
		themeslot.remove_child(themenode)
		themenode.queue_free()
	if ready:
		var inst = new_theme.ui_scene.instance()
		themenode = inst
		themenode.connect("ready", self, "gearup", [themenode], CONNECT_ONESHOT)
		themeslot.add_child(inst)
		if new_theme.ui_theme != null:
			approot.theme = new_theme.ui_theme
		else:
			approot.theme = Globals.themes['Pesterchum'].ui_theme
	# The second half of the load process is in the "gearup" function

func gearup(node:Node):
	"""Initialises some stuff that only exists when the full scene is loaded
	Like hiding GUI elements that should only appear on mobile"""
	print("(_pesterchum_main) gearup: ","go")
	
	var tree = get_tree()
	if Globals.running_on_phone():
		tree.call_group("hide_on_phones", "hide")
	if Config.settings.flags.display_mode == Globals.DISPLAYMODES.MULTIWINDOW:
		if OS.window_size.x < 350:
			OS.window_size.x = 800
		tree.call_group("show_on_multiwindow", "show")
		tree.call_group("hide_on_multiwindow", "hide")
	else:
		tree.call_group("hide_on_singlewindow", "hide")
		tree.call_group("show_on_singlewindow", "show")
	
	if Globals.is_editor_build():
		tree.call_group("show_on_editor", "show")
		tree.call_group("hide_on_release", "hide")
	
	set_chatwindow_open(chatwindow_open)


func load_config_theme(state):
	"""Loads the theme associated with the current profile"""
	var to_load = Globals.themes.get(Config.current_profile.theme, Globals.themes["Pesterchum"])
	print("(_pesterchum_main) load_config_theme: ","Goign to load theme ",to_load)
	load_theme(to_load)


func _on_IRCClient_on_quit(reason):
	_on_IRCClient_on_disconnect(reason)

func _on_IRCClient_on_disconnect(reason):
	print("(_pesterchum_main) _on_IRCClient_on_disconnect: ","Disconnected! Reason: ",reason)
	$approot/popups/connecting_popup.dismiss()
	$approot/popups/login_popup.dismiss()
	if not popups.getpop("reconnect_popup").visible:
		popups.getpop("reconnect_popup").popup(reason)


func _on_IRCClient_on_connecting():
	var pop = $approot/popups/connecting_popup
	pop.popup()


func _on_IRCClient_on_connected():
	$approot/popups/login_popup.dismiss()
	$approot/popups/connecting_popup.dismiss()



func notimplnotif():
	popups.getpop("wip_popup").popup()

func simplenotif(text:="", title:="Notice"):
	var pop = popups.getpop("simplemsg_popup")
	pop.popup( [title, text])
	return pop

func simpleerror(text:="", title:="Error"):
	return simplenotif(text, title)
	

func _on_IRCClient_on_user_channellist_updated(user, channel_list):
	pass # Replace with function body.

func _on_config_loading_failed(message):
	printerr("(_pesterchum_main) _on_config_loading_failed: ",message)
	simpleerror(message, "Error loading configuration")


func _on_update_request_request_completed(result, response_code, headers, body):
	if result == OK and response_code < 300:
		var search_for = 'const application:String = '
		var text = body.get_string_from_utf8()
		var version_pos = text.find(search_for)
		if version_pos != -1:
			version_pos += len(search_for)
			var version_text = text.substr(version_pos).split('\n')[0].strip_edges()
			version_text = version_text.substr(1, len(version_text)-2)
			if version_text != VERSIONS.application:
				simplenotif("Version "+version_text+' is now available\nYou can get it at:\n  mocchapi.itch.io/pesterchum-godot\nor\n  gitlab.com/mocchapi/pesterchum-godot/-/releases', 'New update!')
#			simplenotif(version_text)
		else:
			simpleerror("The file at "+VERSIONS.update_check_url+' does not contain a version number','Invalid update file')
	else:
		simpleerror("Error while communicating with update server\nError code: "+str(result)+'\nHTTP error code: '+str(response_code),"Error checking for updates")
