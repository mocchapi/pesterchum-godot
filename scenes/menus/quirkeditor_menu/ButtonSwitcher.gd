extends HBoxContainer

onready var btn_selector := $btn_selector
onready var btn_replacer := $btn_replacer
signal state_changed(new_state)
var state := true


func _update_state():
	btn_selector.set_pressed_no_signal(state)
	btn_replacer.set_pressed_no_signal(not state)
	if state:
		btn_replacer.get_node("TextureRect").modulate.a = 0.2
		btn_selector.get_node("TextureRect").modulate.a = 1.0
	else:
		btn_selector.get_node("TextureRect").modulate.a = 0.2
		btn_replacer.get_node("TextureRect").modulate.a = 1.0
	emit_signal("state_changed", state)


func _on_btn_selector_toggled(button_pressed):
	state = true
	_update_state()


func _ready():
	_update_state()


func _on_btn_replacer_toggled(button_pressed):
	state = false
	_update_state()
