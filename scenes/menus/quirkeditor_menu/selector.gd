extends VBoxContainer

export var list_path := "/root/Globals:quirk_selectors"
export var variable_name := "selector"
onready var parent := $"../../.."
onready var settings := $settings

var variable setget set_variable,get_variable
var list :Array

onready var option_type := $option_type

func get_variable():
	return parent.quirk.get(variable_name)

func set_variable(new_value):
	return parent.quirk._set(variable_name, new_value)

func _ready():
	list = get_node(list_path).get(list_path.split(':')[-1])
	for item in list:
		option_type.add_item(str(item.name)+" "+variable_name)
	option_type.connect("item_selected", self, "_on_option_type_item_selected")
	yield(get_tree(), "idle_frame")
	re_init()

func re_init():
	var x := 0
	for item in list:
		if get_variable() is item:
			option_type.select(x)
			reset_settings_page()
			break
		x += 1

func reset_settings_page():
	settings.config_obj = get_variable()
	settings.re_init()

func _on_option_type_item_selected(new_index:int):
	if get_variable() is list[new_index]:
		return
	set_variable(list[new_index].new())
	reset_settings_page()
