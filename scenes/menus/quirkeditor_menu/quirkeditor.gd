extends VBoxContainer

onready var tabcontainer := $"%content"
onready var buttonswitcher := $"%ButtonSwitcher"

onready var selector := $"%selector"
onready var replacer := $"%replacer"

onready var slider_randomness := $"%slider_randomness"
onready var label_random_percentage := $"%label_random_percentage"

var quirk:Resource = Quirk.new() setget set_quirk

func _ready():
	if quirk == null:
		set_quirk(Quirk.new())
	$contentscroller/content/replacer/settings.key_hints = {
		"case_mode": {'type':Globals.HINTS.enum_as_int, 'arg':QuirkReplacerCase.MODES.keys()},
		"expression_code": {'type':Globals.HINTS.codeblock}
	}

func set_quirk(new_quirk):
	quirk = new_quirk
	selector.re_init()
	replacer.re_init()



func _on_ButtonSwitcher_state_changed(new_state):
	if tabcontainer == null:
		yield(get_tree(), "idle_frame")
	tabcontainer.current_tab = int(not new_state)


func _on_quirkeditor_visibility_changed():
	$ButtonSwitcher/btn_selector.pressed = (true)


func _on_slider_randomness_drag_ended(value_changed):
	quirk.randomness = clamp(slider_randomness.value, 0.0, 1.0)


func _on_slider_randomness_value_changed(value):
	label_random_percentage.text = str(stepify(100*value, 0.1)).pad_decimals(1) + "%"
