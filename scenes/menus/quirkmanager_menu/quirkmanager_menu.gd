extends VBoxContainer

export var use_current_profile_collection = true setget set_use_current_profile_collection
export(Resource) var collection = QuirkCollection.new()

onready var list_quirks: ItemList = $"%list_quirks"
var menu_quirkeditor

var col

var prev_selected := 0

func set_use_current_profile_collection(new_val: bool):
	# NOTE: it is now required that `collection` be set BEFORE changing this
	# Otherwise the update will run right away, locking in a previous `collection` into `col`
	# eg:
	#	<quirkmanager>.use_current_profile_collection = true
	#	<quirkmanager>.collection = new_collection
	# Will not work. these lines must be swapped in order.
	#
	# TODO?: re-evaluate if using a getter is the right move after all.
	use_current_profile_collection = new_val
	update_collection()


func update_collection():
	var new_col = Config.c.current_profile.quirks if use_current_profile_collection else collection
	col = new_col


func _ready():
	update_collection()
	re_init()
	yield(get_tree(), "idle_frame")
	menu_quirkeditor = Globals.main.menus_.get_menu("quirks", "editor")
	menu_quirkeditor.connect("visibility_changed", self, '_on_quirkeditor_visibility_changed')

func break_connections():
	for item in get_incoming_connections():
		if item['method_name'] in ['_on_col_quirk_added','_on_col_quirk_removed','_on_col_quirk_moved']:
			if item['source'] == col:
				continue
			item['source'].disconnect(item['signal_name'], self, item['method_name'])

func re_init():
	var indexes = list_quirks.get_selected_items()
	if len(indexes) != 0:
		prev_selected = indexes[0]
	list_quirks.clear()
	break_connections()
	col.connect('quirk_added', self, '_on_col_quirk_added')
	col.connect('quirk_removed', self, '_on_col_quirk_removed')
	col.connect('quirk_moved', self, '_on_col_quirk_moved')
	for quirk in col:
		list_quirks.add_item(quirk.printable())
	if list_quirks.get_item_count() != 0:
		list_quirks.select(clamp(prev_selected, 0, list_quirks.get_item_count()))


func _on_col_quirk_added(quirk, index):
	re_init()

func _on_col_quirk_removed(quirk, index):
	re_init()

func _on_col_quirk_moved(quirk, index):
	re_init()


func _on_button_new_pressed():
	var quirk = Quirk.new()
	col.add_quirk(quirk)
	summon_edit_menu(quirk)
	


func _on_button_delete_pressed():
	var idx = list_quirks.get_selected_items()
	if len(idx) == 0:
		return
	idx = idx[0]
	col.remove_index(idx)


func _on_button_duplicate_pressed():
	var idx = list_quirks.get_selected_items()
	if len(idx) == 0:
		return
	idx = idx[0]
	
	var quirk = col.get_quirk(idx)
	var dupe = Quirk.new(quirk)
	col.add_quirk(dupe, idx)


func _on_quirkeditor_visibility_changed():
	if not menu_quirkeditor.visible:
		re_init()

func _on_button_edit_pressed():
	var idx = list_quirks.get_selected_items()
	if len(idx) == 0:
		return
	idx = idx[0]
	summon_edit_menu(col.get_quirk(idx))

func summon_edit_menu(quirk):
	menu_quirkeditor.menu.quirk = quirk
	menu_quirkeditor.show()


func _on_list_quirks_resized():
	list_quirks.fixed_column_width = list_quirks.rect_size.x


func _on_button_debug_export_pressed():
	ResourceSaver.save('user://custom/quirk_presets/debug.tres', col)
	OS.execute('xdg-open',[ ProjectSettings.globalize_path('user://custom/quirk_presets/')])


func _on_button_move_up_pressed():
	var idx = list_quirks.get_selected_items()
	if len(idx) == 0:
		return
	idx = idx[0]
	col.move_quirk(col.get_quirk(idx), idx-1)
	re_init()
	list_quirks.select(idx-1)


func _on_button_move_down_pressed():
	var idx = list_quirks.get_selected_items()
	if len(idx) == 0:
		return
	idx = idx[0]
	col.move_quirk(col.get_quirk(idx), idx+1)
	re_init()
	list_quirks.select(idx+1)


func _on_button_presets_pressed():
	var pop = Globals.main.popups.getpop("quirk_preset_picker")
	pop.connect("accepted",self,"_on_quirk_preset_import")
	pop.show()

func _on_quirk_preset_import(quirk_collection):
	quirk_collection = quirk_collection[0]
	if not quirk_collection is QuirkCollection:
		printerr("(quirkmanager_menu) _on_quirk_preset_import: imported resource is not a QuirkCollection")
	for quirk in quirk_collection:
		col.add_quirk(Quirk.new(quirk))
