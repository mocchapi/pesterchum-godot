extends VBoxContainer

onready var userlist := $PanelContainer/userlist

func _ready():
	Globals.irc.connect("on_channel_user_list_updated", self, "_on_irc_on_channel_user_list_updated")
	_on_userlist_menu_content_visibility_changed()

func _on_irc_on_channel_user_list_updated(channel, users):
	if channel == Config.settings.server.legacy_mood_channel:
		userlist.clear()
		var idx := -1
		for item in users:
			idx += 1
			if item[0] in ['~','+','%','&','@']:
				item = item.substr(1,-1)
			if item == Config.current_profile.chumhandle:
				continue
			
			var icon = null
			var chumroll_handles = Config.chumroll.get_handles()
			if item in Config.robots:
				icon = preload("res://assets/texture/icons/robot_small.png")
			elif item in Globals.canon_handles:
				icon = Globals.icons['canon']
			elif item in chumroll_handles:
				icon = Globals.icons['friend']
			userlist.add_item(item, icon)

		userlist.sort_items_by_text()


func _on_userlist_menu_content_visibility_changed():
	Globals.irc.request_channel_user_list(Config.settings.server.legacy_mood_channel)


func _on_button_addchum_pressed():
	var select = userlist.get_selected_items()
	if len(select) > 0:
		select = select[0]
		var handle = userlist.get_item_text(select)
		if not handle in Config.chumroll:
			Globals.main.popups.getpop("addchum_popup").popup([handle])
		else:
			Globals.main.simplenotif(handle+" is already your chum!")


func _on_button_pesterchum_pressed():
	var select = userlist.get_selected_items()
	if len(select) > 0:
		var conv = Globals.usercache.get_conversation( userlist.get_item_text(select[0]) )
		conv.open()
		conv.request_focus()
		$"../../../..".hide()
