extends MarginContainer


func _ready():
	var is_known = Config.settings.connection.hostname == "irc.pesterchum.xyz"
	$unknown_host.visible = !is_known
	$pesterchum_xyz.visible = is_known
