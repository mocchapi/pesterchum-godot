extends VBoxContainer

onready var list_profiles := $"%list_profiles"

func _ready():
	list_profiles.watch_list(Config.c.profiles)

func _on_button_delete_pressed():
	# todo: make confirmation popup
	var profile = list_profiles.get_selected_user()
	if profile != null and profile != Config.current_profile:
		Config.profiles.remove_user(profile)
	else:
		# todo: notify user you cant delete current profile
		pass


func _on_button_switch_pressed():
	var profile = list_profiles.get_selected_user()
	if profile != null and profile != Config.current_profile:
		Config.switch_current_profile(profile)
	pass # Replace with function body.


func _on_button_duplicate_pressed():
	var profile = list_profiles.get_selected_user()
	if profile != null:
		var duplicate = profile.duplicate(true)
		duplicate.chumhandle += "2"
		print("(profilemanager_menu.gd) _on_button_duplicate_pressed: ","duplicate is: ",duplicate)
		Config.profiles.add_user(duplicate)


func _on_button_edit_pressed():
	var profile = list_profiles.get_selected_user()
	if profile != null:
		var menu = Globals.main.menus_.get_menu('profile','preferences')
		menu.menu.use_current_profile = false
		menu.menu.profile = profile
		menu.show()

func _on_button_new_pressed():
	var new = ConfigProfile.new()
	Config.profiles.add_user(new)
