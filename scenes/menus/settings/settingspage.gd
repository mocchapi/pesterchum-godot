extends VBoxContainer

# Generates user-facing settings screens that automatically update the config
# simply set `obj` to the item inheriting from _baseconfig (BASE_CONFIG.gd)
# then call re_init()
#
# You can also get the generated item from form_ui without using this
#
# Can infer most basic types, but not Dictionaries or Arrays.
# For special behavior you can use `key_hints` to define what keys of the resource should be handled in what way
# The value for the key should be a variation of the following:
# { "type": Globals.HINTS.enum_as_string, 'arg':null }
# Where 'type' must be a HINTS enum and 'arg' will be used by the specific handler, so varies.
# Check Globals.HINTS for a list of all possible methods. Remember to use the enum and not the integer value in case


var config_obj:_baseconfig

signal set_variable(variable_name, new_value)

onready var settingspage := $ScrollContainer/settingspage

export var exclude_keys: PoolStringArray = []
export var exclude_keys_starting_with_underscores: bool = true
export var key_hints := {}
# { "type": HINTS.enum_as_string, 'arg':null }



static func format_name(text:String):
	return text.replace('_',' ').capitalize()

static func form_ui(obj, var_name, target_connector, key_hints:={}):
	var option = obj.get_options()[var_name]
	var value = obj.get(option['name'])
	var out := VBoxContainer.new()
	var sub := HBoxContainer.new()
	var real_out = PanelContainer.new()
	real_out.theme_type_variation = "menu_settings_panel"

	out.add_child(sub)

	var description = Label.new()
	description.text = option['documentation']
	description.autowrap = true
	description.name ="description"

	real_out.name = var_name

	var label := Label.new()
	label.text = format_name(option['name'])
	label.autowrap = true
	label.size_flags_horizontal = Label.SIZE_EXPAND_FILL
	label.size_flags_stretch_ratio = 0.5
	
	var add_below_label := false
	
	real_out.mouse_filter = Control.MOUSE_FILTER_IGNORE
	out.mouse_filter = Control.MOUSE_FILTER_IGNORE
	sub.mouse_filter = Control.MOUSE_FILTER_IGNORE
	label.mouse_filter = Control.MOUSE_FILTER_IGNORE
	description.mouse_filter = Control.MOUSE_FILTER_IGNORE
	
	
	var hint = null
	var arg = null
	var new 
	
	if key_hints.has(var_name):
		hint = key_hints[var_name].get('type', Globals.HINTS.ignore)
		arg = key_hints[var_name].get('arg')


		match hint:
			Globals.HINTS.codeblock:
				add_below_label = true
				new = TextEdit.new()
				new.text = value
				new.syntax_highlighting = true
				new.show_line_numbers = true
				new.highlight_current_line = true
				new.draw_tabs = true
				new.size_flags_horizontal = new.SIZE_EXPAND_FILL
				new.size_flags_vertical = new.SIZE_EXPAND_FILL
				new.rect_min_size.y = 300
				new.connect("text_changed", target_connector, "value_changed_text", [option['name'], new])
			Globals.HINTS.enum_as_int:
				new = OptionButton.new()
				for item in arg:
					new.add_item(item)
				new.selected = value
				new.connect("item_selected", target_connector, "value_changed", [option['name']])
			Globals.HINTS.enum_as_string:
				new = OptionButton.new()
				for item in arg:
					new.add_item(item)
				new.selected = arg.find(value)
				new.connect("item_selected", target_connector, "value_changed_index_to_string", [option['name'], arg])
			Globals.HINTS.none,_:
				pass
	else:
		match typeof(value):
			TYPE_INT, TYPE_REAL:
				new = SpinBox.new()
				new.rounded = true
				new.allow_greater = true
				new.allow_lesser = true
				new.value = value
				new.connect("value_changed", target_connector, "value_changed", [option['name']])

			TYPE_BOOL:
				new = CheckButton.new()
				new.pressed = value
				new.flat = true
				new.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
				new.connect("toggled", target_connector, "value_changed", [option['name']])

			TYPE_COLOR:
				new = WidgetScaledcolorpickerbutton.new()
				new.text = "   "
				new.color = value
				new.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
				new.connect("color_changed", target_connector, "value_changed", [option['name']])

			TYPE_STRING:
				new = LineEdit.new()
				new.size_flags_horizontal = LineEdit.SIZE_EXPAND_FILL
				new.text = value
				new.connect("text_changed", target_connector, "value_changed", [option['name']])

			TYPE_ARRAY,_:
				# Unimplemented types go here
				printerr("(settingspage.gd) form_ui: ","cannot deal with type ",typeof(value)," of key ",var_name)
				return
	
	new.name = "input"
	
	
	sub.add_child(label)
	
	if not add_below_label:
		sub.add_child(new)
	else:
		out.add_child(new)

	out.add_child(description)
	real_out.add_child(out)
	

	return real_out

func make_correct_ui(option):
	return form_ui(config_obj, option, self, key_hints)

func _ready():
	if config_obj != null:
		re_init()

func re_init():
	for item in settingspage.get_children():
		item.queue_free()
		settingspage.remove_child(item)

	for item in config_obj.get_options():
		if item.begins_with('_') and exclude_keys_starting_with_underscores or item in exclude_keys:
			continue
		settingspage.add_child(make_correct_ui(item))

func value_changed(new_value, var_name):
	emit_signal("set_variable", var_name, new_value)
	config_obj._set(var_name, new_value)

func value_changed_index_to_string(new_value, var_name, array):
	value_changed(array[new_value], var_name)

func value_changed_text(var_name, object):
	value_changed(object.text, var_name)
