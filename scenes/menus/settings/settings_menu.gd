extends VBoxContainer

var page := preload("res://scenes/menus/settings/settingspage.tscn")
onready var tabs := $better_tabs

var key_hints := {
	"display_mode": {'type':Globals.HINTS.enum_as_int, 'arg':Globals.DISPLAYMODES.keys()},
	"force_desktop": {'type':Globals.HINTS.enum_as_int, 'arg':Globals.OVERRIDE.keys()},
	"virtual_keyboard_mode": {'type':Globals.HINTS.enum_as_int, 'arg':Globals.VKMODE.keys()},
	"server_mode": {'type':Globals.HINTS.enum_as_int, 'arg':Globals.SERVERMODES.keys()},
}

func _ready():
	for item in Config.settings.options.values():
		init_page(item['name'], item['documentation'], Config.settings.get(item['name']))
	tabs.focus_tab(Config.settings.options.keys()[0])

func init_page(title:String, description:String, obj:_baseconfig):
	var inst = page.instance()
	inst.config_obj = obj
	inst.key_hints = key_hints
	inst.get_node("description").text = description
	tabs.add_tab(inst, false, title.capitalize())
