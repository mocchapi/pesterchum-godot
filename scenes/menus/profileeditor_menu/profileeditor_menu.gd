extends VBoxContainer

onready var preferences_vbox := $"%Preferences"

onready var line_memo_name = $"%line_memo_name"
onready var btn_add_memo = $"%btn_add_memo"
onready var btn_remove_memo = $"%btn_remove_memo"
onready var list_autojoin = $"%list_autojoin"

export var use_current_profile := true
export(Resource) var profile

func _ready():
	preferences_vbox.key_hints = {
		"mood": {'type':Globals.HINTS.enum_as_string, 'arg':Globals.pesterlayer.mood_names},
		"theme": {'type':Globals.HINTS.enum_as_string, 'arg':Globals.themes.keys()}, # // TODO: implement this (switch theme when value changes) when themes are added
	}

func rebuild():
	var p = profile if not use_current_profile else Config.current_profile
	preferences_vbox.config_obj = p
	preferences_vbox.re_init()
	var pw = preferences_vbox.settingspage.get_node("nickserv_password")
	var pw_input = pw.get_child(0).get_child(0).get_child(1) # super hacky :s
	# For some unknown reason, find_node("input") always returns null. even tho that makes no goddamn sense
	pw_input.secret = true
	$"%quirkmanager_menu".collection = p.quirks
	$"%quirkmanager_menu".use_current_profile_collection = use_current_profile
	$"%quirkmanager_menu".re_init()
	
	list_autojoin.clear()
	line_memo_name.clear()
	btn_add_memo.disabled = true
	btn_remove_memo.disabled = true
	for item in p.autojoin_memos:
		list_autojoin.add_item(item)

func _on_better_tabs_visibility_changed():
	if visible:
		rebuild()


func _on_line_memo_name_text_entered(memo_name):
	if not memo_name.begins_with('#'):
		memo_name = '#'+memo_name
	var p = profile if not use_current_profile else Config.current_profile	
	if not memo_name in p.autojoin_memos:
		p.autojoin_memos.append(memo_name)
		list_autojoin.add_item(memo_name)
	line_memo_name.clear()


func _on_btn_add_memo_pressed():
	_on_line_memo_name_text_entered(line_memo_name.text)


func _on_line_memo_name_text_changed(new_text):
	btn_add_memo.disabled = new_text == ""


func _on_list_autojoin_item_selected(index):
	btn_remove_memo.disabled = false


func _on_list_autojoin_nothing_selected():
	btn_remove_memo.disabled = true


func _on_btn_remove_memo_pressed():
	var select = list_autojoin.get_selected_items()
	btn_remove_memo.disabled = len(select) > 0
	if len(select) > 0:
		var p = profile if not use_current_profile else Config.current_profile
		select = select[0]
		p.autojoin_memos.remove(select)
		list_autojoin.remove_item(select)
