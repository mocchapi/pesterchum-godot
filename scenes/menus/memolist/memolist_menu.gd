extends VBoxContainer

onready var pop = Globals.popups.getpop("make_memo_popup")
var p

func _ready():
	Globals.irc.connect("on_channel_list_updated", self, "_on_irc_on_channel_list_updated")
	pop.connect("accepted", self, "on_popup_success")

func _sort_on_users(a,b):
	var compare = b[0].nocasecmp_to(a[0])
	return (a[1]*100 + compare) > (b[1]*100 - compare)

func _on_irc_on_channel_list_updated(channels:Array):
	var list = $PanelContainer/memolist
	channels.sort_custom(self, "_sort_on_users")
	list.clear()
	for item in channels:
#		print(item)
		if (item[0] == Config.settings.server.legacy_mood_channel) and Config.settings.server.should_obey_legacy_moods():
			continue
		item[0][0] = ""
		list.add_item('('+str(item[1])+') '+item[0])
		list.set_item_metadata(list.get_item_count()-1, item)

func on_popup_success(whatever):
#	print(p)
	p.hide()

func _on_memolist_menu_content_visibility_changed():
	Globals.irc.request_channel_list()


func _on_button_join_memo_pressed():
	var memolist := $PanelContainer/memolist
	var select = memolist.get_selected_items()
	if len(select) > 0:
		select = select[0]
		var memo_name = memolist.get_item_metadata(select)[0]
		memo_name = '#' + memo_name
		Globals.irc.join_channel(memo_name)
		Globals.irc.request_channel_user_list(memo_name)
		$"../../../..".hide()


func _on_button_make_memo_pressed():
	pop.popup()
