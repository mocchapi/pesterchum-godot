extends PanelContainer

export var title := "menu title"
export var identifier := "client/"
export var content : PackedScene
export var back_button_enabled := true
export var hide_on_ready := true
export var enabled := true

var menu

func _on_button_back_pressed():
	if back_button_enabled:
		hide()

func _ready():
	if hide_on_ready:
		hide()
	$verts/toppanel/topbar/button_back.visible = back_button_enabled
	$verts/toppanel/topbar/menutitle.text = title
	if content:
		set_content(content)
	else:
		printerr("(basemenu.gd) _ready: ","no content set for menu ",title)

func set_content(scene:PackedScene):
	var new = scene.instance()
	$verts/scroller/content.add_child(new)
	menu = new
	content = scene
	menu.set('p', self)
