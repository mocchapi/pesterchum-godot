extends VBoxContainer


func _ready():
	$pc_panel/pc_details/version.text = "Version "+VERSIONS.application + ' (config '+str(VERSIONS.config_format)+')'

	var theme:ClientTheme 
	if Config.c.current_profile.theme in Globals.themes:
		theme = Globals.themes[Config.c.current_profile.theme]
	else:
		printerr("No such theme: ",Config.current_profile.theme)
		theme = Globals.themes['Pesterchum']
	$theme_details/name_by_author/name.text = theme.theme_name
	$theme_details/name_by_author/author.text = theme.theme_author
	$theme_details/version.text = "Version "+theme.theme_version+ " ("+ str(theme.format_version)+")"
	$theme_details/source.bbcode_text = '[url]' + theme.theme_source + '[/url]'
	$theme_details/supports_singlewindow.pressed = theme.supports_singlewindow
	$theme_details/supports_multiwindow.pressed = theme.supports_multiwindow



func _on_source_meta_clicked(meta):
	var url = Globals.themes[Config.current_profile.theme].theme_source
	OS.shell_open(url)
