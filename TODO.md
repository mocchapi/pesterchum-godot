### Quick todo list so i dont forget stuff
In no way a comprehensive list, these are just specific things i need to do and if i dont write them down i'll forget about them.



//todo:
- [x] Config saving does not trigger at all on web
- [x] sending /me should skip applying quirks
- [ ] users QUITing does not remove them from open memo Userlists
	- This should already be handled. maybe its something else causing a buildup of users that are not in the memo
- [ ] IRCv3 SASL login

- [ ] Gif emotes
- [ ] set metadata color & mood on launch
	- possibly already done
- [ ] emotes popup goes behind virtual keyboard
- [ ] chattabs3 tabs wrap the last letter onto a newline in android

- [x] Check updates against the git Versions.gd
- [ ] Show "X changed their mood to Y" in pesters
- [ ] Fix tab colors in chattabs3
- [ ] Make bettertabs highlight the selected tab
- [~] text is red. why
- [x] users not removed from list of memo participants when leaving
- [x] replace chattabs_2 code that handles "open-ness" & other conversation stuff with the new ConfigConversation resource & usercache additions
	- [x] Replace memo resource with a subclass of ConfigConversation
- [x] Redo pesterlayer's `message_to_bbcode`
- [x] fix the channel user list to not combine with the chumroll. oops.
- [ ] Move as many pieces of a theme's content to bespoke widgets. Goal is no-code drag & drop themeing aswell as decreased pesterchum_main responsibility
	- [x] Mood buttons
	- [x] Add chum
	- [x] change handle bar
	- [x] chat tabs
	- [x] menu bar items
	- [x] userlist
	- [x] close/mimimize buttons
	- [x] misc buttons
	- some stuff i forgor, add them later
- [x] check wtf is up with mood 23 (maybe instead do `idx in range(mood_names.size()+1)`?) <- seems to be a QT-PC client error, actually
- [ ] Fix config and quirk collections bundling scripts for no reason
	- somehow during saving or duplicating or something, they stop being Quirk/QuirkCollections, forcing them to be saved as their own resource with bundled script
- [~] Make memowindow and chatwindow handle chat logic themselves, allowing for drag & drop for theme makers.  

^ This is already handled in chattabs_2  
- [x] Replace the lookups system in Gearup with either %nodenames or node groups
- [~] Properly expose numeric reply errors to the user (Requires more reply definitions in the IRC handlers)
- [x] Automatic login with nickserv
- [x] Obey GETMOODs
- [x] Dont do any processing on outgoing messages in #pesterchum
- [ ] Make offline chums greyed out in lists
	- is this necessary?
- [~] System for silently sending a message without opening a chatwindow on response
- [x] Dispatch timeframe on memo join (PESTERCHUM:TIME>i)
- [x] Dispatch timeframe on other user joining memo (PESTERCHUM:TIME>i)
- [~] Dispatch ALL active timeframes on other user joining memo  
	- Seemed very spammy, and the main client doesnt seem to do it either
- [x] Only show the "FUTURE|PAST x responded to memo" when the user says something after changing timeframe, instead of when the timeframe is sent
- [ ] Rename all theme variations in general UX to make sense for themes
	- ongoing effort
	- [ ] Settings pages (profile preferences/client options/etc)
	- [ ] Menus
	- [ ] Popups
- [x] Pick a less ugly font / mess with the rendering
	- Changing the base viewport scale on mobile could help with being able to increase the font size without running into minimum size issues
	- Switch back to IBM courier with Courier Prime as fallback only
- [ ] Properly implement the "create memo" popup
	- [x] pop up when button pressed
	- [x] create memo
	- [ ] modify perms to allow hidden
	- [ ] modify perms to allow invite-only
- [x] Expose the server settings on the login screen / revamp the login screen
- [x] Stop caching every user when they declare a mood in #pesterchum; only cache those that are directly interacting
- [ ] Hook up the rest of the settings menu:
	- [x] Connect port & check cetitificate to the irc node
	- [~] Implement the TCP fallback (Bad idea)
	- [x] Disable pesterchum behavior (IRC mode)
	- [ ] Hide host messages
	- [x] make flags optionbuttons corrosponding to the enum keys
	- [ ] fix bbcode_enabled to actually do something
	- [x] Allow enabling websockets for non-web clients
	- [x] Allow disabling chumroll background colors
	- [x] Allow disabling chumroll text colors
- [x] Add quirk management
	- Screen with all quirks for this profile
	- The quirks are ordered by which get run first
	- Each entry can have a randomness value between 1.0 and 0.0 corrosponding to a % of outgoing messages it gets run on
- [x] Implement quirks:
	- [x] Make quirk editing UI
	- [x] Make quirkcollection editing UI
	- 1 Quirk comes in two parts:
		- Selectors: Selects a substring from the input. Returns an array of indices of the string, I.E. [[0,3,"text"]]
			- [ ] Add a dice roll for every selector item, seperate from the quirk-wide random (maybe called "consistency"?)
			- [x] Add whole sentence selector; selects entire sentence
			- [~] Add prefix selector; selects a nonexistant character at the start of the line (think one index before 0) (superseded by prefix replacer)
			- [~] Add suffix selector; selects a nonexistant character at the end of the line (think one index after -1) (superseded by suffix replacer)
			- [x] Add simple selector; selects by a literal string. option for case sensitivity
			- [~] Add simple list selector; selects by multiple literals. option for case sensitivity
			- [x] Add regex selector; selects by a regex match string
			- [ ] Add "every Nth" character(s) selector
			- [ ] Add "last/first" character(s) selector
			- [x] Add between selector; selects text between two characters
			- [ ] MAYBE: Add punctuation selector
			- [ ] MAYBE: Add smiley selector
			- [ ] MAYBE: Add code selector; selects by a custom script (Would be complicated to present user-side due to having to slice manually)
		- Replacers: Receives the bit of text to be meddled and changes it.
			- [x] Add suffix replacer; adds a suffix to the message
			- [x] Add prefix replacer; adds a prefix to the message
			- [x] Add simple replacer; replaces input with a specific string literal  
				^ Allow for case-matching, IE selected is all caps == replaced is all caps
			- [x] add case replacer; replaces case of selected text to upper, lower, proper, or inverted (replaces uppercase replacer)
			- [ ] Add simple list replacer; replaces input with that of a random/sequential index in a list of items
			- [x] Add repeat replacer; repeats selected text X amount of times
			- [x] Add "simplified code"/expression replacer; uses the Expression object w/ user code to replace
				- [ ] Write a wiki page for how to use this
				- [x] Add an environment object that provides fake variable support
				- [x] Hack in newline support
	- This is now the QuirkCollection -> ~~A profile can have a "stack" of quirks: sorted by first to be run to last, the next quirks runs on the output of the current quirk~~
	- Some implementations thoughts on code replace quirks:
		- ~~Import text as Script and attach to a node (could pull down entire process on error)~~
		- ~~Use a LUA plugin to run the code sandboxed (would be a lot of work)~~
		- Run text using Expression ~~(would limit functionality greatly to one-liners)~~
		- ~~Wait for godot 4.0 :')~~
- [x] Fix emoji selector to not go offscreen
- [ ] Fix the virtual keyboard not resizing high enough on some devices
	- Probably means that it has too little room to scale
		- Either detect this & switch entirely to PUSH_UP
		- OR detect this & push the remaining part up
	- actually, i think the code for this is just conked.   
	- [x] for now, ive just added a flag override to increase/decrease the target height
- [ ] Fix the window returning to slightly smaller than full resolution after dismissing virtual keyboard 
- [~] Handle notices better
	- [~] What the fuck. theyre sent to yourself.what.
	- [~] They dont even fuckin show up anymore  
^^^ Could not replicate
- [x] Add /me
- [ ] Add quirk presets:
	- [ ] Figure out why quirks keep getting their scripts bundled
		- at some point the quirk objects become a custom object instead of an instane
	- [ ] save current quirk list as preset
		- currently only in debug mode
	- HS
		- [ ] sprites
		- [ ] cherub
			- [ ] calliope
			- [ ] caliborn
		- [ ] trolls
			- [x] Vriska (wip)
			- [x] Terezi
			- [x] karkat
			- [x] kanaya
			- [x] nepeta
			- [x] aradia
			- [ ] sollux (1/3 done)
			- [x] tavros (maybe add smileys)
			- [x] equius
			- [x] gamzee
			- [x] feferi
			- [x] eridan
	- [ ] Hiveswap
		- Man i gotta play this before i dare look at the wiki
- [x] Add profile management menu
	- Apply when settings is
	- [x] Allow adding new profile
	- [x] Allow duplicating profile
	- [x] Allow deleting profile
	- [x] Allow switching to profile
	- per profile settings:
		- [x] String: Chumhandle
		- [x] Color: Color
		- [x] String-dropdown: Mood
		- [x] String-dropdown: theme
			- Make this a seperate menu maybe?
		- [x] Bool: Allow random encounters (not yet hooked up)
		- [x] Bool: Receive messages from chums only
		- [x] List: memos to autojoin (Seperate menu)
		- [x] List: quirks (Seperate menu)
- [x] Implement chums_only mode
- [ ] Make selected tabs lighter or highlit in some way
- [x] Implement random encounters (doc: https://github.com/Dpeta/randomEncounter#protocol)
- [ ] Add attribution and license info in app
- [x] Allow providing custom tab, tab holder, & chat screens to bettertabs
- [x] Define theme system. Current ideas:
	- Themes are made in godot editor
	- [ ] should theme be able to override mood images?
	- Theme has a Theme resource and a PackedScene resource
		- [x] Theme resource applied to app root so popup and menu match
		- [x] PackedScene instanced on load
			- Should be as drag & drop as possible

- [x] Refactor chat system so closing a chat doesnt open it back up
	- fixed for now, but i still think a central system to deal with the "openness" of chats would be much more helpful than leaving it all to chat_tabs
- [x] Simplify/refactor memo resource



- Themes:
	- [ ] Finalize pesterchum theme
		- [ ] Make the settings pages less awful to look at
	- [ ] Add trollian theme
		- [ ] Fix buttons & other small still-default theme items
		- [ ] Make the settings page look less awful
	- [ ] Add pestercord theme (pesterchum x discord)
	- [ ] Add godotengine theme (godot x pesterchum)
	- [ ] Add the enamel/7.0 theme
	- [ ] Add the BettyBother theme
	- [ ] Add the Serious Business theme
	- [ ] Add hiveswap themes if they exist
		- will have to play the games first :3c
	- [ ] MAYBE: add walkaound theme. Point & click walkaround game where characters are uses you're talking to.
	- [ ] MAYBE: add pictochat theme
	- [~] MAYBE: Add modernapp theme (pesterchum as a modern chat app, a la whatsapp/telegram/whatever. with BetterTabcontainer maybe?) (hoenstly this one kinda sucks)

- [x] Set up a plugin + theme repository / backend to facilitate installing plugins and themes from inside the app (Must work on web, should be fine when CORS is set)
	- Could be a git repository?
	- simple laravel setup?
		- would be easier to control, esp with CORS stuff
		- Would requier a lot more work (database, frontends, profile management)

- [ ] Integrate theme repository
	- Not a pressing issue, no themes are being made yet

- [ ] Rework popup and menu to use the child instead of a packedscene

- [~] Add base64/whatever text input & output dialog to share some resouces in text form (for easier custom assets distribution on web and phones)
	- Ideas:
		- Quirk presets
		- MAYBE themes (would probably be excessively large due to packed assets)
		- MAYBE custom quirk selectors/replacers (could be a security concern, especially if its copy-pasted in an unreadable format like base64)
		- allow distributing through QR codes?
		- allow distributing through URLs?
	- I dont think this will be necessary now that we have the theme repository, the other assets are unlikely to get shared

- [x] Move DMs to their own resource & hook up signals and fucntions up to that instead of handling logic in chattabs_2
	- allows more elegant handling of what DM chats are open
	- Central location to open and close DMs
		- like cache
	- saveable
	- more robust
	- doesnt (have to) wipe all conversations on theme change / other reload

- [ ] Handle changing settings more gracefully without requiring a restart
	- Theme switching could be done on the fly by freeing the current theme
		- This is now technically fully possible without dropping conversations (but this mechanism is not implemented)
	- Some theme settings do not require a reconnect at all
	- [ ] For now, make it more obvious that a user needs to restart

- [~] Make custom color picker, default color picker is seemingly impossible to make smaller
	- Just force scaled the popup instead 

- [ ] Give mood buttons a checkmark when they're selected

- [ ] Add settings/profile reset button
	- for now, android users can "clear data" and desktop users can run with '--reset-config'



- [ ] Implement invites
	- Having a general way to embed custom GUI in between text would be helpful.  
	Could either find a way to sync a floating Control to the scroll position OR split the richtext into two at the point where the GUI should go

- [x] Make links clickable
- [x] Make #channel_names clickable
- [x] Make @mentions clickable

- [ ] Add message notif sounds
- [ ] Add x unread messages indicator 
	- Unread messages are semi-tracked rn, chattabs3 needs to facilitate resetting more
	- if chat tabs starts tracking the selected tab, it can also quite easily implement the selected tab highlight
- [ ] Add custom chum groups (chumroll/trollsum/etc) like normal pchum
- [x] Ignore quirks when talking to robots
	- make this a setting, maybe? <- no. why would i do that. why would you quirk to the bot. what.
- [x] Implement metadata
- [x] deal with "handle is already taken" irc error
- [ ] deal with "handle changed by admin" case
	- Possibly done, requires some testing

- [ ] Allow chumhandlebar to show a popup to change mood
- [x] Implement blocking users
- [x] Implement UNblocking users
- [x] Implement removing chums

- [~] MAYBE BUT PROBABLY NOT: Implement C2C file transfer  
No.  
this is way too big a security & privacy concern  

- [ ] Add a way to override font size

- [ ] MAYBE: Dissalow changing nicknames on the fly

- [x] In memowindow: move the userlist to the side if in desktop mode