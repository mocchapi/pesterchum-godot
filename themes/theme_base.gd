extends Resource
class_name ClientTheme,"res://assets/texture/icons/theme_icon.png"
# Base resource for all themes

# actual stuff
export var ui_theme:Theme # This gets applied to popups and menus
export var ui_scene:PackedScene # This is the actual loaded GUI
export var supports_singlewindow := true # Scales to a usable version for singlewindow (mostly phones/compact)
export var supports_multiwindow := true # Expands to a usable version for multiwindow (mostly desktop/large)
export var theme_name := "Pesterchum"
# metadata
export var theme_author := "Your Name"
export var theme_version := "1.0"
export var theme_source := "https://example.com/source_code" setget set_theme_source# Link to source code of this theme. Must be availible for all bundled themes
export(String, MULTILINE) var theme_description := ""


# Do not change these
const is_client_theme := true # Easier to check this than test if the class is ClienTheme for imported resource shenanigans reasons
export var format_version:int = VERSIONS.theme_format # For testing if the client is newer or older than the version the theme expects to be run on

func set_theme_source(new:String):
	if not new.begins_with('http://') and not new.begins_with('https://'):
		new = 'https://' + new
	theme_source = new
