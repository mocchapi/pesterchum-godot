extends VBoxContainer



func _on_chat_tabs_tab_pressed(tab_name):
	$"%chat_content_panel".visible = true
	$"%timelines_panel".visible = false


func _on_button_back_to_lines_pressed():
	$"%chat_content_panel".visible = false
	$"%timelines_panel".visible = true
