extends MarginContainer

signal pressed()
signal closed()

export var icon :Texture setget set_icon
export var text := "" setget set_text
export var show_close_button := true setget set_show_close_button
export var color:Color setget set_color

func _ready():
	var height = rand_range(0.2, 5.0)
	$"%buttonspot".size_flags_stretch_ratio = height
	size_flags_horizontal = SIZE_FILL if Globals.running_on_phone() else SIZE_EXPAND_FILL
	set_color(color)
	$VBoxContainer/blackbar/HBoxContainer.visible = rand_range(0,1) > 0.75

func set_color(newcolor:Color):
	var contrast = Globals.color_contrasts_dark(color)
	var text_color = newcolor
	if !contrast:
		newcolor = newcolor.lightened(0.25)
	elif contrast:
		text_color.darkened(0.25)
	color = newcolor
	$"%tab_text".add_color_override("font_color", color)
	$"%arrow".modulate = newcolor
	$"%ball".modulate = newcolor

func set_icon(newicon:Texture):
	pass
	$"%icon".texture = newicon

func set_text(new_text:String):
	if Globals.usercache.has_user(new_text):
		new_text = Globals.usercache.get_user(new_text).initials
	$"%tab_text".text = new_text

func set_show_close_button(value:bool):
	$MarginContainer/close_button.visible = value


func _on_tab_text_container_mouse_entered():
	$"%tab_text".clip_text = false


func _on_tab_text_container_mouse_exited():
	$"%tab_text".clip_text = !Globals.running_on_phone()


func _on_tab_text_container_gui_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and not event.pressed:
		emit_signal("pressed")


func _on_close_button_pressed():
	emit_signal("closed")


func _on_ball_pressed():
	emit_signal("pressed")
